(function($){
            
        window.onload = function() {

            // console.log("Tell me smt");
            // console.log('window - onload'); // 4th

        // Global Variable

        var form;

        var name2 = document.getElementById("name2");
        var email2 = document.getElementById("email2");
        var phone2 = document.getElementById("phone2");
        var dob2 = document.getElementById("dob2");
        var address2 = document.getElementById("address2");
        var country2 = document.getElementById("country2");
        var city2 = document.getElementById("city2");
        var shoesArray = [];
        var i; //for loop
        var cleanbox; //clean checkbox
        var repainttype; //repaint child checkbox
        var note2 = document.getElementById("note2");
        var userSubmitted = false;
        var delivery;
        var order_type;
        var server_user_id;

        // Form Part 1 - User Info
        var name = document.getElementById("name");
        var email = document.getElementById("email");
        var phone = document.getElementById("phone");
        var dob = document.getElementById("dob");
        var address = document.getElementById("address");
        var note = document.getElementById("note");
        // var country = document.getElementById("country");
        var city = document.getElementById("city");
        var regbox = document.getElementById("check-1");
        
        

        // Inject Event to Function passvalue
        name.onkeyup =  
        email.onkeyup =
        phone.onkeyup = 
        dob.onkeyup =
        name.onkeyup = 
        address.onkeyup = 
        note.onkeyup =
        city.onkeyup =  function() {passvalue()};

        // Date Picker pick Birthday


        $('#dob').daterangepicker({
            locale: {
                format: "DD/MM/YYYY"
            },
            singleDatePicker: true,
            // autoApply: true,
            autoUpdateInput: false,
            showDropdowns: true,
            minYear: 1970,
            maxYear: 2010},
            function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
            $('#dob').data('daterangepicker').setStartDate(start.format('DD/MM/YYYY'));
            // // console.log(start);
            // var date =  new firebase.firestore.Timestamp.fromDate(start._d);
            dob2.textContent = convertDate(start._d);

            console.log(dob2);
        });


        // Form Part 2 - Service Detail
        
        $(".repaint").toggle(); 

        $("#repaint_shoes").change(() => {
            $(".repaint").fadeToggle();

        })

        $("#delshoes").hide();


        $(".selectgiay").change(function(){
            console.log($(this).val())
            let outer_div = $(this).closest("div.form-group");
            outer_div.find("#price_clean").text("Vệ Sinh " + $(this).val());
        })



        // Form Part 3 - Confirmation and Shipping


        // Chỗ viết Function
        function passvalue(){
            //Get the input element
            
            //Get the value

            console.log("PASSING DATA TO PART 3");
            
            name2.textContent = name.value;
            email2.textContent = email.value;
            phone2.textContent = phone.value;
            dob2.textContent = dob.value;
            address2.textContent = address.value;
            note2.textContent = note.value;
            // country2.textContent = country.value;
            city2.textContent = city.value;
            regbox2 = regbox; 
        }

        function convertDate(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
        }
        
        // function testUser(){
        //     console.log(name.value)
        //     if (regbox.checked == true){
        //         console.log(email.value);
        //     }
        // }

        function testShoes(){
            console.log()
        }

        function addNewUser(){
            // console.log(bluesicht_DB);
            var stats_ref = bluesicht_DB.collection('users').doc('statistics');
            var new_user_ref = bluesicht_DB.collection('users').doc();
            var new_order_ref = bluesicht_DB.collection('orders').doc();
            var new_order_id = new_order_ref.id;
            var shoes_ref = bluesicht_DB.collection('shoes');
            let id = 0;
            bluesicht_DB.runTransaction(function(transaction) {
                // This code may get re-run multiple times if there are conflicts.
                return transaction.get(stats_ref).then(function(doc_id) {
                    if (!doc_id.exists) {
                        throw "Document does not exist!";
                    }
                    var currentID = doc_id.data().userCountID;
                    id = currentID;
                    var newID = doc_id.data().userCountID + 1;

                    transaction.set(new_user_ref, 
                        {
                            customid: currentID,
                            name: name.value || 'none',
                            dob: dob.value || 'none',
                            email: email.value || 'none',
                            address: address.value || 'none',
                            city: city.value || 'none',
                            user_type: order_type || 'none',
                            phone: phone.value || 'none',
                            timestamp: firebase.firestore.FieldValue.serverTimestamp()
                        }); 

                    transaction.update(stats_ref, { userCountID: newID });


                    transaction.set(new_order_ref,
                        {
                            customid: currentID,
                            total: calculateTempPrice(),
                            delivery: delivery,
                            address: address.value || 'none',
                            city: city.value || 'none',
                            note: note.value || 'none',
                            timestamp: firebase.firestore.FieldValue.serverTimestamp()
                        })

                    
                    
                    




                });
            }).then(function() {
                console.log("Transaction successfully committed!");

                    var shoesBatch = bluesicht_DB.batch();
                    shoesArray.forEach(function(shoe){
                        console.log("uploading shoes");
                        let current_doc_ref = shoes_ref.doc();
                        shoesBatch.set(current_doc_ref,
                            {
                                order_id: new_order_id,
                                customid: id,
                                type: shoe.type,
                                clean: shoe.clean,
                                repaint: shoe.repaint,
                                desc: shoe.desc || "none",
                                timestamp: firebase.firestore.FieldValue.serverTimestamp()
                            })
                        
                    })
                    shoesBatch.commit().then(function () {

                        Swal.close()

                        Swal.fire(
                            'Đăng kí thành công!',
                            'Chúng tôi sẽ liên lạc với bạn sớm nhất để hoàn tất đơn hàng!',
                            'success'
                          ).then(function () {
                               document.location.reload()
                        });
                        
                        
                    });




            }).catch(function(error) {
                console.log("Transaction failed: ", error);
            });
           
            
        }
        function addOrder(id){
            
        }
        //if regbox.checked == true
        function processOrder(){ // part 1

            
            
            addNewUser();
            // var addNewUser = firebase.functions().httpsCallable('writeUserCallable');		
            // addNewUser({
            //     name:name.value,
            //     email:email.value,
            //     phone:phone.value,
            //     dob:dob.value,
            //     user_type: order_type,
            //     address:address.value,
            //     city:city.value,
            //     timestamp: firebase.firestore.Timestamp
            // }).then(function(result){
            //     server_user_id = result;
            //     console.log(server_user_id)
            //     // addServiceToFirestoreForGuest(server_user_id);
            // }).catch(function(error){
            //     console.log("Adding user error: ",error);
            // });
            userSubmitted = true;
        }

        function addServiceToFirestoreForGuest(user_id){ // part 3            
            // getTimestamp();
            getDelivery();
            i = 0;
            var addNewTransaction = firebase.functions().httpsCallable('writeServiceCallable');
            // for (i;i < shoesCounter; i++){
                addNewTransaction({                
                    email:email.value,
                    phone:phone.value,
                    user_id: user_id,
                    // shoe_type:shoesArray[i].type,
                    // clean:shoesArray[i].clean,                
                    // repaint:shoesArray[i].repaint,
                    delivery:delivery,
                    order_type: order_type,
                    timestamp: firebase.firestore.Timestamp
                }).catch(function(error){
                    console.log(`Adding service ${i} error: `,error);
                });
        // }}
        }
        function addServiceToArray(){ //part 2
            shoesArray = [];
            i = 0;            
            var shoes = document.getElementsByClassName("selectgiay");
            var shoesdetail = document.getElementsByClassName("detailgiay");
            var clean = document.getElementsByClassName("cleangiay");
            var repaint = document.getElementsByClassName("repaintgiay");
            var repaintde = document.getElementsByClassName("repaintde");
            var repaintthan = document.getElementsByClassName("repaintthan");
            var repaintfull = document.getElementsByClassName("repaintfull");            
            // console.log(clean[0].checked);
            // console.log(repaint[0].checked);
            for (i; i < shoesCounter; i++){
                cleanbox = false;
                repainttype = 'none';
                shoe_type = 1;
                if (clean[i].checked == false && repaint[i].checked == false)
                {
                    console.log("Not chosen")
                    // continue;
                }
                if (clean[i].checked == true)
                {
                    cleanbox = true;
                }
                if (repaint[i].checked == true)
                {
                    if (repaintde[i].checked == true){
                        repainttype = "de";
                    }
                    else if (repaintthan[i].checked == true){
                        repainttype = "than";
                    }                      
                    else if (repaintfull[i].checked == true){
                        repainttype = "full";
                    }
                    else {
                        repainttype = "none";
                        console.log("Not selected")
                        // continue;
                    }
                }
                
                shoe_type = $(".selectgiay").eq(i).children("option:selected").attr("type");
                let desc = shoesdetail[i].value
                
                shoesArray.push({
                    "type": shoe_type,
                    "clean":cleanbox,
                    "repaint":repainttype,
                    "desc": desc
                })
                }
                
                console.log(shoesArray);
            }
        

        function getTimestamp(){
            var asiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Bangkok"});
            time = new Date(asiaTime).toLocaleString();
        }
        function checkInputInfo(){
            let name = $('#name').val();
            let email = $('#email').val();
            let phone = $('#phone').val();
            let address = $('#address').val();
            
            if (!name || !email || !phone || !address)
                {
                    Swal.fire(
                    'Phát hiện lỗi!',
                    'Vui lòng điền đầy đủ những thông tin yêu cầu vào form!',
                    'error'
                  )
                    setTimeout(
                        function() 
                        {
                            $('#step2back').trigger('click');
                            $('#step2back').trigger('click');
                        }, 500);    
                }
            else
                {

                }
        }
        function calculateTempPrice(){

            let total_price = 0;
            shoesArray.forEach(function(shoe){
                console.log("checking shoe");
                if (shoe.clean)
                {    
                    switch (shoe.type) {
                        case "1":
                        total_price = total_price + 119;
                        break;
                        case "2":
                        total_price = total_price + 169;
                        break;
                        case "3":
                        total_price = total_price + 69;
                        break;
                        case "4":
                        total_price = total_price + 99;
                        break;
                    }
                }
                switch (shoe.repaint) {
                    case 'none':
                    break;
                    case 'de':
                    total_price = total_price + 299;
                    break;
                    case 'than':
                    total_price = total_price + 399;
                    break;
                    case 'full':
                    total_price = total_price + 599;
                    break;
                }

                    
              });
              console.log(total_price);
            $("#total_bill").text("Total : " + total_price + ".000 VND" );
            return total_price;
        }

        function getDelivery(){            
            console.log("geting delivery status")
            var ship = $('#ship-01').is(':checked');
            var pickup = $('#ship-02').is(':checked');
            if (ship == false && pickup == false){
                Swal.fire(
                    'Error!',
                    'Vui lòng chọn phương thức giao hàng!',
                    'error'
                  )             
                  return false;
            }
            if (ship){
                delivery = "Shipping"
                return true;
            }
            if (pickup){
                delivery = "Pickup"
                return true;
            }
        }

        $('#step1next').add('#tab2-btn').on('click',function(){
            console.log("checking")
            checkInputInfo();
        })
        $('#step2next').add('#tab3-btn').on('click', function() {
            shoesArray = [];
            checkInputInfo();
            addServiceToArray();
            calculateTempPrice();
            console.log("Array added")
        });

        $('#step3next').on('click', function() {
            console.log("Finishing")
            if (regbox.checked == true)
            {
                order_type = "member"
                console.log("Member")
            }
            else
            {
                order_type = "guest"
                console.log("Guest")
            }

            if (getDelivery())
                Swal.fire({
                    type: 'info',
                    title: 'Loading...',
                    text: 'Sending your order information to the moon and back to us!',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    onBeforeOpen: () => {

                        Swal.showLoading();
                    }
                })
                processOrder();
            console.log("Guest form uploading...")
        });
    }
    
    //define counter
    var shoesIncrement = 1;
    var shoesCounter = 1;
    
    $(function(){
    // $(document).ready(function(){
        $("#addshoes").on('click',function(){
            // $("#delshoes").show();
            //define shoes' clone    
            var shoesclone = $('.tabgiay:first').clone(false);
    

            var cleanShoes = shoesclone.find("input.custom-control-input").each(function() {
               $(this).prop({
                "name":$(this).attr('name')+shoesIncrement,              
                "id":$(this).attr('id')+shoesIncrement,
                "checked":false
               }) 
            });
            var labelShoes = shoesclone.find("label.custom-control-descfeedback").each(function() {
                $(this).prop({
                 "for":$(this).attr('for')+shoesIncrement
                }) 
             });
            var divShoes = shoesclone.find(".repaint").each(function() {
                $(this).prop({
                 "class":$(this).attr('class')+shoesIncrement
                }) 
             });       

            
            let newclass = ".repaint" + shoesIncrement;
            
            
            shoesclone.appendTo(".tabgiayall")
            .on('click', 'button.remove', remove);            


            $('.remove:not(:first)').show();
            shoesclone.find("#price_clean").text("Vệ Sinh Sneaker - 119.000 VND");
            // console.log(shoesclone.find("#shoes_detail"))
            shoesclone.find("#shoes_detail").val("");
            
            shoesclone.find(".selectgiay").change(function(){
                console.log($(this).val())
                let outer_div = $(this).closest("div.form-group");
                outer_div.find("#price_clean").text("Vệ Sinh " + $(this).val());
            })

            $(newclass).hide(); 
            
            $("#repaint_shoes" + shoesIncrement).change(() => {
                console.log("changing");
                
                $(newclass).fadeToggle();

            })
            
            shoesIncrement++;
            shoesCounter++;
            document.getElementById("shoesNum").innerHTML = `Số đôi giày của bạn: ${shoesCounter}`;
            return false;
        });

        function remove() {        
            $(this).parents(".tabgiay").remove();
            shoesCounter--;
            document.getElementById("shoesNum").innerHTML = `Số đôi giày của bạn: ${shoesCounter}`;
            if ($(".tabgiay").length == 1) {
                $('#delshoes').hide();
            } else {
                $('.remove:not(:first)').show();
            }
        }        
        
        // service.addEventListener('change', changeVal);


        function changeVal(){
            $('.tabgiayall').find('.tabgiay').each(function(){                
                var shoes = document.getElementsByClassName("selectgiay");
                var test = shoes.options[shoes.selectedIndex].text;                
                console.log(shoes);
            });
        }

        
        
    });
// })

})(jQuery);


        