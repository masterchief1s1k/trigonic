(function (windows)
{
    // // define images
    // var images = [
    //     "assets/road-1.svg",
    //     "assets/road-2.svg",
    //     "assets/road-3.svg",
    //     "assets/road-4.svg",
    //     "assets/road-5.svg"
    // ];

    // // TweenMax can tween any property of any object. We use this object to cycle through the array
    // var obj = {curImg: 0};



    // Shoe

    // create tween
    var tween_1 = TweenMax.fromTo( "#shoe", 1, { top :"-100%", rotation: 0, ease: Linear.easeNone}, {top:"50%",rotation: 720, ease: Linear.easeNone});
    // var tween = TweenMax.to(obj, 0.6,
    //     {
    //         curImg: images.length - 1,	// animate propery curImg to number of images
    //         roundProps: "curImg",				// only integers so it can be used as an array index
    //         repeat: -1,									// repeat 3 times
    //         immediateRender: true,			// load first image automatically
    //         ease: Linear.easeNone,			// show every image the same ammount of time
    //         onUpdate: function () {
    //             $("#myimg").attr("src", images[obj.curImg]); // set the image source
    //         }
    //     }
    // );

    // init controller
    var controller = new ScrollMagic.Controller();
    window.MainController = controller;

    // build scene
    var scene_shoe = new ScrollMagic.Scene({triggerElement: "#trigger_6", duration: "0"})
                    .setTween(tween_1)
                    .setPin("#shoe")
                     // add indicators (requires plugin)
                    .addTo(controller);

    // Table

    // create tween
    // var tween_2 = TweenMax.fromTo( "#table", 1, { left :"-100vh", ease: Linear.easeNone}, {left:"0vh", ease: Linear.easeNone});
    var tween_2 = TweenMax.to( "#table", 1, {left:"50%", ease: Linear.ease});
    // tween_2.delay(0.5);
   
    // build scene
    var table_shoe = new ScrollMagic.Scene({triggerElement: "#trigger_6", duration: "0"})
                    .setTween(tween_2)
                    .setPin("#table")
                     // add indicators (requires plugin)
                    .addTo(controller);

    // Lighting

    var tween_fade_lighting = TweenMax.to( "#light", 1, {autoAlpha:1, ease: Linear.easeOut})
    // create tween
    var tween_3 = TweenMax.to( "#light", 1, {rotation: -55, ease: Linear.easeOut, repeat: -1, yoyo: true});
    // tween_2.delay(0.5);
    var lighting_lamp_fade = new ScrollMagic.Scene({triggerElement: "#trigger_7", duration: "0"})
    .setTween(tween_fade_lighting)
     // add indicators (requires plugin)
    .addTo(controller);

    // build scene
    TweenMax.set("#light",{autoAlpha:0});
    var lighting_lamp = new ScrollMagic.Scene({triggerElement: "#trigger_7", duration: "0"})
                    .setTween(tween_3)
                    .setPin("#light")
                     // add indicators (requires plugin)
                    .addTo(controller);

    
    // Lamp

    var tween_fade_lamp = TweenMax.to( "#lamp", 1, {autoAlpha:1, ease: Linear.easeOut})
    
    // build scene

    TweenMax.set("#lamp",{autoAlpha:0});
    var lamp_fade = new ScrollMagic.Scene({triggerElement: "#trigger_7", duration: "0"})
    .setTween(tween_fade_lamp)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // Lamp

    var tween_fade_lamp = TweenMax.to( "#lamp", 1, {autoAlpha:0, left:"-30%", ease: Linear.easeOut})
    

    var lamp_fade = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "0"})
    .setTween(tween_fade_lamp)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // Lamp

    var tween_fade_lamp = TweenMax.to( "#light", 1, {autoAlpha:0, left:"-30%", ease: Linear.easeOut})
    

    var lamp_fade = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "0"})
    .setTween(tween_fade_lamp)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    





    // Khach run in
    var tween_bouncein_khach = TweenMax.to("#khachhang", 1 , {right: 0, ease: Back.easeOut})
    TweenMax.set("#khachhang", {right: '110%'})

    var bouncein_khach = new ScrollMagic.Scene({triggerElement: "#trigger", duration: "10%"})
    .setTween(tween_bouncein_khach)
    
    .addTo(controller);

     // Khach run out
     var tween_bouncein_khach = TweenMax.to("#khachhang", 1 , {right: '-80%', ease: Back.easeOut})
     
 
     var bouncein_khach = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "10%"})
     .setTween(tween_bouncein_khach)
     
     .addTo(controller);
 

    // Running dust
    var tween_bouncein_dust = TweenMax.to("#khachhang_dust", 1 , {right: 0, ease: Back.easeOut})
    TweenMax.set("#khachhang_dust", {right: '110%'})
    var tween_animate_dust = TweenMax.to( "#khachhang_dust", 0.2, { scale:1.1, ease: Linear.easeOut, repeat: -1, yoyo: true});
    var tween_dust_out = TweenMax.to("#khachhang_dust", 0.5 , {autoAlpha: 0, ease: Back.easeOut})

    var bouncein_dust = new ScrollMagic.Scene({triggerElement: "#trigger", duration: "10%"})
    .setTween(tween_bouncein_dust)
    
    .addTo(controller);

    var animate_dust = new ScrollMagic.Scene({triggerElement: "#trigger", duration: "10%"})
    .setTween(tween_animate_dust)
    
    .addTo(controller);

    var dust_out = new ScrollMagic.Scene({triggerElement: "#trigger_3", duration: 0})
    .setTween(tween_dust_out)
    
    .addTo(controller);

    // Bluesichter run in
    var tween_bouncein_bs = TweenMax.to("#bluesichter", 1 , {right: 0, ease: Back.easeOut})
    TweenMax.set("#bluesichter", {right: '-20%'})

    var bouncein_khach = new ScrollMagic.Scene({triggerElement: "#trigger", duration: "5%"})
    .setTween(tween_bouncein_bs)
    
    .addTo(controller);


    // Bluesichter run out
    var tween_bouncein_bs = TweenMax.to("#bluesichter", 1 , {right: '-20%', ease: Back.easeOut})
   

    var bouncein_khach = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "5%"})
    .setTween(tween_bouncein_bs)
    
    .addTo(controller);

    // Khach change to khach 2
    var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_confused.png", ease: Linear.easeOut})

    var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_3", duration: "0"})
    .setTween(tween_khach_2)
    
    .addTo(controller);

    // Khach 2 change to khach 3
    var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_thinking.png", ease: Linear.easeOut})

    var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_10", duration: "0"})
    .setTween(tween_khach_2)
    
    .addTo(controller);


    // Khach 3 change to khach 4
    var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_busy.png", ease: Linear.easeOut})

    var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_14", duration: "0"})
    .setTween(tween_khach_2)
    
    .addTo(controller);

    // Khach 4 change to khach 5
    var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_hold.png", ease: Linear.easeOut})

    var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_18", duration: "0"})
    .setTween(tween_khach_2)
    
    .addTo(controller);

    // Khach 5 run in
    var tween_bouncein_khach = TweenMax.to("#khachhang", 1 , {right: "-15%", ease: Back.easeOut})
   

    var bouncein_khach = new ScrollMagic.Scene({triggerElement: "#trigger_19", duration: "10%"})
    .setTween(tween_bouncein_khach)
    
    .addTo(controller);

     // Khach 5 run out
     var tween_bouncein_khach = TweenMax.to("#khachhang", 1 , {right: "0%", ease: Back.easeOut})
   

     var bouncein_khach = new ScrollMagic.Scene({triggerElement: "#trigger_20", duration: "5%"})
     .setTween(tween_bouncein_khach)
     
     .addTo(controller);

      // Khach 5 change to khach 6
    var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_busy.png", ease: Linear.easeOut})

    var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_20", duration: "0"})
    .setTween(tween_khach_2)
    
    .addTo(controller);


      // Khach 6 change to khach 7
      var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_thinking.png", ease: Linear.easeOut})

      var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_21", duration: "0"})
      .setTween(tween_khach_2)
      
      .addTo(controller);

      // Khach 7 change to khach 8
      var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_busy.png", ease: Linear.easeOut})

      var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_29", duration: "0"})
      .setTween(tween_khach_2)
      
      .addTo(controller);


      // Khach 8 change to khach 9
      var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_thinking.png", ease: Linear.easeOut})

      var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_32", duration: "0"})
      .setTween(tween_khach_2)
      
      .addTo(controller);

      
      // Khach 9 change to khach 10
      var tween_khach_2 = TweenMax.to("#khachhang", 0.001 , {src: "assets/khachhang_happy.png", ease: Linear.easeOut})

      var khach_2 = new ScrollMagic.Scene({triggerElement: "#trigger_37", duration: "0"})
      .setTween(tween_khach_2)
      
      .addTo(controller);

      
  

  



    
    // Bs change to bs 2
    var tween_bs_2 = TweenMax.to("#bluesichter", 0.001 , {src: "assets/bluesichter_2.png", ease: Linear.easeOut})

    var bs_2 = new ScrollMagic.Scene({triggerElement: "#trigger_8", duration: "0"})
    .setTween(tween_bs_2)
    
    .addTo(controller);

    // Bs 2 change to bs 3
    var tween_bs_2 = TweenMax.to("#bluesichter", 0.001 , {src: "assets/bluesichter_3.png", ease: Linear.easeOut})

    var bs_2 = new ScrollMagic.Scene({triggerElement: "#trigger_14", duration: "0"})
    .setTween(tween_bs_2)
    
    .addTo(controller);

    // Bs 3 change to bs 4
    var tween_bs_2 = TweenMax.to("#bluesichter", 0.001 , {src: "assets/bluesichter_4.png", ease: Linear.easeOut})

    var bs_2 = new ScrollMagic.Scene({triggerElement: "#trigger_18", duration: "0"})
    .setTween(tween_bs_2)
    
    .addTo(controller);

    // Bs 4 change to bs 5
    var tween_bs_2 = TweenMax.to("#bluesichter", 0.001 , {src: "assets/bluesichter_5.png", ease: Linear.easeOut})

    var bs_2 = new ScrollMagic.Scene({triggerElement: "#trigger_20", duration: "0"})
    .setTween(tween_bs_2)
    
    .addTo(controller);


    // Thoai Bluesicht

    // Fadein thoai 2

    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_8", duration: "10%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);

    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_9", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);



    // thoai 1 change to thoai 2
    var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_1", 0.001 , {src: "assets/thoai_bs_2.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_14", duration: "0"})
    .setTween(tween_change_thoai_bs_1)
    
    .addTo(controller);


    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_15", duration: "5%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);

    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_16", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // thoai 1 change to thoai 2
    var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_1", 0.001 , {src: "assets/thoai_bs_4.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_16b", duration: "0"})
    .setTween(tween_change_thoai_bs_1)
    
    .addTo(controller);



    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_18", duration: "5%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_20", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);




    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_16", duration: "5%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_17", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // thoai 1 change to thoai 2
    var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_2", 0.001 , {src: "assets/thoai_bs_5.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_19", duration: "0"})
    .setTween(tween_change_thoai_bs_1)
    
    .addTo(controller);




    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_20", duration: "5%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_21", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // thoai 5 change to thoai 6
    var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_2", 0.001 , {src: "assets/thoai_bs_6.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_22", duration: "0"})
    .setTween(tween_change_thoai_bs_1)
    
    .addTo(controller);



    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_24", duration: "5%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_25", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


     // thoai 6 change to thoai 7
     var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_2", 0.001 , {src: "assets/thoai_bs_7.png", ease: Linear.easeOut})

     var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_30", duration: "0"})
     .setTween(tween_change_thoai_bs_1)
     
     .addTo(controller);
 
 
 
     var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:1, ease: Linear.easeOut})
 
     var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_31", duration: "10%"})
     .setTween(tween_fade_thoai_2)
     // .setPin("#lamp")
      // add indicators (requires plugin)
     .addTo(controller);
 
 
     var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:0, ease: Linear.easeOut})
 
     var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_32", duration: "5%"})
     .setTween(tween_out_thoai_2)
     // .setPin("#lamp")
      // add indicators (requires plugin)
     .addTo(controller);




    // thoai 7 change to thoai 8
    var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_1", 0.001 , {src: "assets/thoai_bs_8.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_32", duration: "0"})
    .setTween(tween_change_thoai_bs_1)
    
    .addTo(controller);



    var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:1, ease: Linear.easeOut})

    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_33", duration: "5%"})
    .setTween(tween_fade_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_34", duration: "5%"})
    .setTween(tween_out_thoai_2)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);



     // thoai 8 change to thoai 9
     var tween_change_thoai_bs_1 = TweenMax.to("#thoai_bs_2", 0.001 , {src: "assets/thoai_bs_9.png", ease: Linear.easeOut})

     var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_34", duration: "0"})
     .setTween(tween_change_thoai_bs_1)
     
     .addTo(controller);
 
 
 
     var tween_fade_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:1, ease: Linear.easeOut})
 
     var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_35", duration: "10%"})
     .setTween(tween_fade_thoai_2)
     // .setPin("#lamp")
      // add indicators (requires plugin)
     .addTo(controller);
 
 
     var tween_out_thoai_2 = TweenMax.to( "#thoai_bs_2", 1, {autoAlpha:0, ease: Linear.easeOut})
 
     var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_36", duration: "5%"})
     .setTween(tween_out_thoai_2)
     // .setPin("#lamp")
      // add indicators (requires plugin)
     .addTo(controller);




    





    // Thoai Khach Hang


    // Fadein thoai 1

    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:1, ease: Linear.easeOut})

    TweenMax.set("#thoai_khachhang_1",{autoAlpha:0});
    TweenMax.set("#thoai_khachhang_2",{autoAlpha:0});
    TweenMax.set("#thoai_bs_1",{autoAlpha:0});
    TweenMax.set("#thoai_bs_2",{autoAlpha:0});
    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_3", duration: "10%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);

    var tween_out_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_7", duration: "5%"})
    .setTween(tween_out_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);

    

    

    // thoai 1 change to thoai 2
    var tween_thoai_2 = TweenMax.to("#thoai_khachhang_1", 0.001 , {src: "assets/thoai_khachhang_2.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_9", duration: "0"})
    .setTween(tween_thoai_2)
    
    .addTo(controller);

    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:1, ease: Linear.easeOut})


    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_10", duration: "5%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_3 = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai_3 = new ScrollMagic.Scene({triggerElement: "#trigger_11", duration: "5%"})
    .setTween(tween_out_thoai_3)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_2", 1, {autoAlpha:1, ease: Linear.easeOut})


    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_12", duration: "5%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_3 = TweenMax.to( "#thoai_khachhang_2", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai_3 = new ScrollMagic.Scene({triggerElement: "#trigger_13", duration: "5%"})
    .setTween(tween_out_thoai_3)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // thoai 3 change to thoai 4
    var tween_thoai_2 = TweenMax.to("#thoai_khachhang_1", 0.001 , {src: "assets/thoai_khachhang_4.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_13", duration: "0"})
    .setTween(tween_thoai_2)
    
    .addTo(controller);


    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:1, ease: Linear.easeOut})


    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_14", duration: "5%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_3 = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai_3 = new ScrollMagic.Scene({triggerElement: "#trigger_15", duration: "5%"})
    .setTween(tween_out_thoai_3)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // thoai 4 change to thoai 5
    var tween_thoai_2 = TweenMax.to("#thoai_khachhang_1", 0.001 , {src: "assets/thoai_khachhang_5.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_16", duration: "0"})
    .setTween(tween_thoai_2)
    
    .addTo(controller);


    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:1, ease: Linear.easeOut})


    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_17", duration: "5%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_3 = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai_3 = new ScrollMagic.Scene({triggerElement: "#trigger_19", duration: "5%"})
    .setTween(tween_out_thoai_3)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);



    // thoai 5 change to thoai 6
    var tween_thoai_2 = TweenMax.to("#thoai_khachhang_1", 0.001 , {src: "assets/thoai_khachhang_6.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_27", duration: "0"})
    .setTween(tween_thoai_2)
    
    .addTo(controller);


    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:1, ease: Linear.easeOut})


    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_29", duration: "5%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_3 = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai_3 = new ScrollMagic.Scene({triggerElement: "#trigger_30", duration: "5%"})
    .setTween(tween_out_thoai_3)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);



    // thoai 6 change to thoai 7
    var tween_thoai_2 = TweenMax.to("#thoai_khachhang_1", 0.001 , {src: "assets/thoai_khachhang_7.png", ease: Linear.easeOut})

    var thoai_2 = new ScrollMagic.Scene({triggerElement: "#trigger_36", duration: "0"})
    .setTween(tween_thoai_2)
    
    .addTo(controller);


    var tween_fade_thoai = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:1, ease: Linear.easeOut})


    var fade_thoai = new ScrollMagic.Scene({triggerElement: "#trigger_37", duration: "5%"})
    .setTween(tween_fade_thoai)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out_thoai_3 = TweenMax.to( "#thoai_khachhang_1", 1, {autoAlpha:0, ease: Linear.easeOut})

    var out_thoai_3 = new ScrollMagic.Scene({triggerElement: "#trigger_38", duration: "5%"})
    .setTween(tween_out_thoai_3)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);




    // Everything out






    



})( window );