
Royal_Preloader.config({
    mode           : 'logo',
    logo           : 'img/logo_white.png',
    logo_size      : [100, 100],
    showProgress   : true,
    showPercentage : true,
    setTimeout : 1,
    text_colour: '#f8f8f8',
    background:  '#212121'
});	
