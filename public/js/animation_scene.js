(function (windows)
{

    controller = window.MainController;


    

    // Doctor on Machine
    

    var tween_fade_doctor_machine = TweenMax.to( "#doctor_machine", 1, {autoAlpha:1, left:"10%", ease: Linear.easeOut})
    
    // build scene

    TweenMax.set("#doctor_machine",{autoAlpha:0, left:"-10%"});
    var doctor_machine_fade = new ScrollMagic.Scene({triggerElement: "#trigger_20", duration: "0"})
    .setTween(tween_fade_doctor_machine)
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_fade_doctor_machine = TweenMax.to( "#doctor_machine", 1, {autoAlpha:0, left:"-20%", ease: Linear.easeOut})
    
    // build scene

    var doctor_machine_fade = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "0"})
    .setTween(tween_fade_doctor_machine)
     // add indicators (requires plugin)
    .addTo(controller);

    // Doctor on Machine animating

    var images_doctor_machine = ["assets/doctor/doctor_machine_1.png",
                                "assets/doctor/doctor_machine_2.png",
                                "assets/doctor/doctor_machine_3.png"];

    var obj_doctor_machine = {curImg: 0};

    var tween_doctor_machine_animation = TweenMax.to(obj_doctor_machine, 0.5,
        {
            curImg: images_doctor_machine.length - 1,	// animate propery curImg to number of images
            roundProps: "curImg",				// only integers so it can be used as an array index
            repeat: 20,									// repeat 3 times
            immediateRender: true,			// load first image automatically
            ease: Linear.easeNone,			// show every image the same ammount of time
            onUpdate: function () {
                $("#doctor_machine").attr("src", images_doctor_machine[obj_doctor_machine.curImg]); // set the image source
            }
        })

    var doctor_machine = new ScrollMagic.Scene({triggerElement: "#trigger_28", duration: "200%"})
    .setTween(tween_doctor_machine_animation)
     // add indicators (requires plugin)
    .addTo(controller);


    // Phone


    var phone_slidein = TweenMax.to( "#phone", 1, { left:"50%", ease: Linear.easeOut})
    
    // build scene

    TweenMax.set("#phone",{ left:"-40%"});
    TweenMax.set("#phone2",{autoAlpha:0});
    var doctor_machine_fade = new ScrollMagic.Scene({triggerElement: "#trigger_20a", duration: "0"})
    .setTween(phone_slidein)
     // add indicators (requires plugin)
    .addTo(controller);



    var phone_slideout = TweenMax.to( "#phone2", 1, {autoAlpha:0, left:"-50%", ease: Linear.easeOut})
    

    var doctor_machine_fade = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "0"})
    .setTween(phone_slideout)
     // add indicators (requires plugin)
    .addTo(controller);


    var phone_change_tween = TweenMax.to( "#phone", 0.5, {autoAlpha:0, ease: Linear.easeOut})
    
    // build scene

    var phone_change = new ScrollMagic.Scene({triggerElement: "#trigger_23", duration: "0"})
    .setTween(phone_change_tween)
     // add indicators (requires plugin)
    .addTo(controller);


    var phone_change_tween = TweenMax.to( "#phone2", 0.5, {autoAlpha:1, ease: Linear.easeOut})
    
    // build scene

    var phone_change = new ScrollMagic.Scene({triggerElement: "#trigger_23a", duration: "0"})
    .setTween(phone_change_tween)
     // add indicators (requires plugin)
    .addTo(controller);



    


    // Card


    var card_slidein = TweenMax.to( "#card", 1, {autoAlpha:1, left:"80%", ease: Bounce.easeOut})
    
    // build scene

    TweenMax.set("#card",{autoAlpha:0, left:"110%"});
    var card_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_21", duration: "10%"})
    .setTween(card_slidein)
     // add indicators (requires plugin)
    .addTo(controller);


    var card_read = TweenMax.to( "#card", 1, {autoAlpha:1, left:"50%", ease: Linear.easeOut})
    
    // build scene

    var card_read_tween = new ScrollMagic.Scene({triggerElement: "#trigger_22", duration: "20%"})
    .setTween(card_read)
     // add indicators (requires plugin)
    .addTo(controller);


    var card_out = TweenMax.to( "#card", 1, {autoAlpha:1, left:"-50%", ease: Linear.easeOut})
    
    // build scene

    var card_out_tween = new ScrollMagic.Scene({triggerElement: "#trigger_24a", duration: "20%"})
    .setTween(card_out)
     // add indicators (requires plugin)
    .addTo(controller);


    
    // Clean


    var clean_slidefromtop = TweenMax.to( "#clean_machine", 1, {autoAlpha:1, top:"40%", ease: Bounce.easeOut})
    
    // build scene

    TweenMax.set("#clean_machine",{autoAlpha:0, top:"20%"});
    var clean_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_26", duration: "10%"})
    .setTween(clean_slidefromtop)
     // add indicators (requires plugin)
    .addTo(controller);


    var clean_slidefromtop = TweenMax.to( "#clean_laser", 1, {autoAlpha:1, top:"40%", ease: Bounce.easeOut})
    
    // build scene

    TweenMax.set("#clean_laser",{autoAlpha:0, top:"20%"});
    var clean_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_26", duration: "10%"})
    .setTween(clean_slidefromtop)
     // add indicators (requires plugin)
    .addTo(controller);




    var clean_slidefromtop = TweenMax.to( "#clean_machine", 1, {autoAlpha:0, top:"-60%", ease: Linear.easeOut})
    
    // build scene

    var clean_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "100%"})
    .setTween(clean_slidefromtop)
     // add indicators (requires plugin)
    .addTo(controller);


    var clean_slidefromtop = TweenMax.to( "#clean_laser", 1, {autoAlpha:0, top:"-60%", ease: Linear.easeOut})
    
    // build scene

    var clean_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "100%"})
    .setTween(clean_slidefromtop)
     // add indicators (requires plugin)
    .addTo(controller);


    // Fadeout Tween

    var tween_fade_floor = TweenMax.to( "#floor", 1, {autoAlpha:0, top:"-60%", ease: Linear.easeOut})

    var nhiptim_fade = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "100%"})
    .setTween(tween_fade_floor)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);



    var clean_slidefromtop = TweenMax.to( "#clean_laser", 1, {rotation:10, ease: Linear.easeOut})
    
    // build scene
    
    var clean_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_26", duration: "10%"})
    .setTween(clean_slidefromtop)
     // add indicators (requires plugin)
    .addTo(controller);




    var clean_slidefromtop = TweenMax.to( "#table", 1, {autoAlpha:0, top:"-50%", ease: Linear.easeOut})
    
    // build scene

    var clean_slidein_tween = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "100%"})
    .setTween(clean_slidefromtop)
     // add indicators (requires plugin)
    .addTo(controller);





    var images_clean_machine = ["assets/clean/cleana.png",
                                "assets/clean/cleanb.png",
                                "assets/clean/clean 1.png",
                                "assets/clean/clean 2.png",
                                "assets/clean/clean 3.png",
                                "assets/clean/clean 4.png",
                                "assets/clean/clean 5.png",
                                "assets/clean/clean 6.png",
                                "assets/clean/clean 7.png",
                                "assets/clean/cleana.png",
                                "assets/clean/cleanb.png"];

    var obj_clean_machine = {curImg: 0};

    var tween_clean_machine_animation = TweenMax.to(obj_clean_machine, 0.5,
        {
            curImg: images_clean_machine.length - 1,	// animate propery curImg to number of images
            roundProps: "curImg",				// only integers so it can be used as an array index
            repeat: 5,									// repeat 3 times
            immediateRender: true,			// load first image automatically
            ease: Linear.easeNone,			// show every image the same ammount of time
            onUpdate: function () {
                $("#clean_machine").attr("src", images_clean_machine[obj_clean_machine.curImg]); // set the image source
            }
        })

    var clean_machine = new ScrollMagic.Scene({triggerElement: "#trigger_clean", duration: "140%"})
    .setTween(tween_clean_machine_animation)
     // add indicators (requires plugin)
    .addTo(controller);

    

    var images_soap_machine = ["assets/soap/0.png",
                                "assets/soap/1.png",
                                "assets/soap/2.png",
                                "assets/soap/3.png",
                                "assets/soap/4.png",
                                "assets/soap/5.png",
                                "assets/soap/6.png",
                                "assets/soap/7.png",
                                "assets/soap/8.png",
                                "assets/soap/9.png",
                                "assets/soap/10.png",
                                "assets/soap/11.png",
                                "assets/soap/12.png",
                                "assets/soap/13.png",
                            ];

    var obj_soap_machine = {curImg: 0};

    TweenMax.set("#soap_machine",{autoAlpha:0});


    TweenMax.set("#shoe_after",{autoAlpha:0});

    var tween_fadein_soap = TweenMax.to( "#soap_machine", 0.5, { autoAlpha: 1});
    
    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_clean", duration: "0"})
    .setTween(tween_fadein_soap)
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_fadein_soap = TweenMax.to( "#soap_machine", 0.5, { autoAlpha: 0});
    
    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_38", duration: "0"})
    .setTween(tween_fadein_soap)
     // add indicators (requires plugin)
    .addTo(controller);


    

    var tween_animate_soap = TweenMax.to( "#soap_machine", 0.2, { scale:1.1, ease: Linear.easeOut, repeat: -1, yoyo: true});
    
    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_clean", duration: "140%"})
    .setTween(tween_animate_soap)
     // add indicators (requires plugin)
    .addTo(controller);



    
    var tween_soap_machine_animation = TweenMax.to(obj_soap_machine, 0.5,
        {
            curImg: images_soap_machine.length - 1,	// animate propery curImg to number of images
            roundProps: "curImg",				// only integers so it can be used as an array index
            repeat: 5,									// repeat 3 times
            immediateRender: true,			// load first image automatically
            ease: Linear.easeNone,			// show every image the same ammount of time
            onUpdate: function () {
                $("#soap_machine").attr("src", images_soap_machine[obj_soap_machine.curImg]); // set the image source
            }
        })

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_clean", duration: "100%"})
    .setTween(tween_soap_machine_animation)
     // add indicators (requires plugin)
    .addTo(controller);





    // Shoe Clean

    
    var tween_shoe_clean = TweenMax.to("#shoe", 0.5,{autoAlpha: 0, ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_shoeclean", duration: "0"})
    .setTween(tween_shoe_clean)
     // add indicators (requires plugin)
    .addTo(controller);

    var tween_shoe_clean = TweenMax.to("#shoe_after", 0.5,{autoAlpha: 1, ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_shoeclean", duration: "0"})
    .setTween(tween_shoe_clean)
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_shoe_rotating = TweenMax.to("#shoe_after", 1,{rotation: 1080, ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_39", duration: "75%"})
    .setTween(tween_shoe_rotating)
     // add indicators (requires plugin)
    .addTo(controller);

    
    
    var tween_bag_slideup = TweenMax.to("#bag", 1,{autoAlpha:1, top: '50%', ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_40", duration: "45%"})
    .setTween(tween_bag_slideup)
     // add indicators (requires plugin)
    .addTo(controller);

    
    var tween_khachhang_holdingbag = TweenMax.to("#khachhang_holdingbag", 1,{autoAlpha:1, top: '50%', ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_40", duration: "45%"})
    .setTween(tween_khachhang_holdingbag)
     // add indicators (requires plugin)
    .addTo(controller);



    
    var tween_khachhang_holdingbag = TweenMax.to("#khachhang_holdingbag", 1,{ left: '50%', ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_end", duration: "20%"})
    .setTween(tween_khachhang_holdingbag)
     // add indicators (requires plugin)
    .addTo(controller);

        
    var tween_out = TweenMax.to("#khachhang_holdingbag", 1,{autoAlpha:0, left: '120%', ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_showformbutton", duration: "10%"})
    .setTween(tween_out)
     // add indicators (requires plugin)
    .addTo(controller);

        
    var tween_out = TweenMax.to("#bag", 1,{autoAlpha:0, left: '120%', ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_showformbutton", duration: "10%"})
    .setTween(tween_out)
     // add indicators (requires plugin)
    .addTo(controller);


    var tween_out = TweenMax.to("#shoe_after", 1,{autoAlpha:0, left: '120%', ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_showformbutton", duration: "10%"})
    .setTween(tween_out)
     // add indicators (requires plugin)
    .addTo(controller);



    TweenMax.set("#callToActionText",{autoAlpha:0});
    TweenMax.set("#callToActionBtn",{autoAlpha:0});


    var tween_in = TweenMax.to("#callToActionText", 1,{autoAlpha:1, ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_showformbutton", duration: "10%"})
    .setTween(tween_in)
     // add indicators (requires plugin)
    .addTo(controller);



    var tween_in = TweenMax.to("#callToActionBtn", 1,{autoAlpha:1, ease: Linear.easeOut})

    var soap_machine = new ScrollMagic.Scene({triggerElement: "#trigger_showformbutton", duration: "10%"})
    .setTween(tween_in)
     // add indicators (requires plugin)
    .addTo(controller);





    TweenMax.set("#khachhang_holdingbag",{autoAlpha:0, top:"100%",left:"-10%"});
    TweenMax.set("#bag",{autoAlpha:0, top:"100%"});
    
    









    



    var tween_3 = TweenMax.to( "#clean_laser", 1, {rotation: -10, ease: Linear.easeOut, repeat: -1, yoyo: true});

    var clean_laser_tween = new ScrollMagic.Scene({triggerElement: "#trigger_27", duration: "130%"})
    .setTween(tween_3)
     // add indicators (requires plugin)
    .addTo(controller);
    


    // var card_read = TweenMax.to( "#card", 1, {autoAlpha:1, left:"50%", ease: Linear.easeOut})
    
    // // build scene

    // var card_read_tween = new ScrollMagic.Scene({triggerElement: "#trigger_22", duration: "20%"})
    // .setTween(card_read)
    //  // add indicators (requires plugin)
    // .addTo(controller);


    // var card_out = TweenMax.to( "#card", 1, {autoAlpha:1, left:"-50%", ease: Linear.easeOut})
    
    // // build scene

    // var card_out_tween = new ScrollMagic.Scene({triggerElement: "#trigger_24a", duration: "20%"})
    // .setTween(card_out)
    //  // add indicators (requires plugin)
    // .addTo(controller);


    

    // Máy đo nhịp tim
    var images_nhiptim = [];
    let img_name_temp = "assets/flatline/ekg";
    let i = 0;
    for (i = 1; i < 55; i++) { 
        let img_name = img_name_temp + i + ".png";
        images_nhiptim.push(img_name);
      }
   
    var obj = {curImg: 0};

    // create tween
    var tween_nhiptim = TweenMax.to(obj, 0.5,
        {
            curImg: images_nhiptim.length - 1,	// animate propery curImg to number of images
            roundProps: "curImg",				// only integers so it can be used as an array index
            repeat: 1,									// repeat 3 times
            immediateRender: true,			// load first image automatically
            ease: Linear.easeNone,			// show every image the same ammount of time
            onUpdate: function () {
                $("#nhiptim").attr("src", images_nhiptim[obj.curImg]); // set the image source
            }
        }
    );

    // Fadein Tween

    var tween_fade_nhiptim = TweenMax.to( "#nhiptim", 1, {autoAlpha:1, left:"15%", ease: Linear.easeOut})

    TweenMax.set("#nhiptim",{autoAlpha:0, left:"-100%"});
    var nhiptim_fade = new ScrollMagic.Scene({triggerElement: "#trigger_19", duration: "0"})
    .setTween(tween_fade_nhiptim)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    // Fadeout Tween

    var tween_fade_nhiptim = TweenMax.to( "#nhiptim", 1, {autoAlpha:0, left:"-100%", ease: Linear.easeOut})

    var nhiptim_fade = new ScrollMagic.Scene({triggerElement: "#trigger_endscene1", duration: "0"})
    .setTween(tween_fade_nhiptim)
    // .setPin("#lamp")
     // add indicators (requires plugin)
    .addTo(controller);


    



    // build scene
    var nhiptim = new ScrollMagic.Scene({triggerElement: "#trigger_28", duration: "140%"})
                    .setTween(tween_nhiptim)
                     // add indicators (requires plugin)
                    .addTo(controller);

})( window );