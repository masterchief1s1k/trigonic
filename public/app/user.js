var providerFB = new firebase.auth.GoogleAuthProvider();
var providerGG = new firebase.auth.FacebookAuthProvider();
var current_user;
var current_user_doc;
var form = {
    name: null,
    email: null,
    DoB: null,
    phone: null,
    personalID: null,
    fb: null,
    photoURL: null,
    photoFile: null
};
var email_regex=/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;


Royal_Preloader.config({
    mode           : 'logo',
    logo           : '../images/logo.png',
    logo_size      : [100, 100],
    showProgress   : true,
    showPercentage : true,
    setTimeout : 1,
    text_colour: '#f8f8f8',
    background:  '#212121'
});	

$('#DoB').daterangepicker({
    locale: {
        format: "DD/MM/YYYY"
    },
    singleDatePicker: true,
    // autoApply: true,
    autoUpdateInput: false,
    showDropdowns: true,
    minYear: 1975,
    maxYear: 2010},
    function(start, end, label) {
    // console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
    $('#DoB').data('daterangepicker').setStartDate(start.format('DD/MM/YYYY'));
    // console.log(start);
    var date =  new firebase.firestore.Timestamp.fromDate(start._d);
    form.DoB = date;
    // console.log(date.toDate());
});


    // if (user) {
    //     console.log(user);
    //     $("#user-nav").append("<a href=\"user/index.html\"\> " + user.displayName + " </a>");
    //     $("#user-nav").append("<img src=" + user.photoURL + " style=\" align-self:center; padding-left: 20px; \" class=\"img-circle\" />");
        
    //     // $("#user-nav").append("<a href=\"user/index.html\"\><i class=\"fa fa-user\" style=\"font-size: 20px;\"></i>" + user.displayName + " </a> ");
    // } else {
    //     $("#user-nav").append("<a href=\"login.html\"\>Login/Register</a> ");
    // }

 // Catch Login Status
 firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        console.log(user);
        current_user = user;
        
        $("#user-nav").append("<a href=\"user/index.html\"\> " + user.displayName + " </a>");
        $("#user-nav").append("<img src=" + user.photoURL + " style=\" align-self:center; padding-left: 0px; \" class=\"rounded-circle\" />");
        
         var user_ref = trigonic_DB.collection("users").doc(user.uid);

         user_ref.get().then(function(doc) {
            if (doc.exists) {
                current_user_doc = doc.data();
                console.log(current_user_doc);
                if (current_user_doc.name != "none")
                {
                    $("#full_name").val(current_user_doc.name);
                    form.name = current_user_doc.name;
                    $("#name_display").text(current_user_doc.name);
                    // $("#full_name").prop('disabled', true);
                }
                else 
                {
                    $("#full_name").toggleClass("focused");
                    $("#full_name").keyup(function(){
                        if ($("#full_name").val() != "")
                        {
                            $("#full_name").removeClass("focused");
                        }
                        else
                        {
                            $("#full_name").addClass("focused");
                        }
                    })
                }

                if (current_user_doc.email != "none")
                {
                    $("#email").val(current_user_doc.email);
                    form.email = current_user_doc.email;
                    $("#email").prop('disabled', true);
                    $("#email_display").text(current_user_doc.email);
                }
                else
                {
                    $("#email").toggleClass("focused");
                    $("#email").keyup(function(){
                        if ($("#email").val() != "")
                        {
                            $("#email").removeClass("focused");
                        }
                        else
                        {
                            $("#email").addClass("focused");
                        }
                    })
                }

                

                if (current_user_doc.photoURL != "none")
                {
                    $("#imagePreview").css('background-image', 'url('+current_user_doc.photoURL +')');
                }   

                if (current_user_doc.fb != "none")
                {
                    let url = 'https://graph.facebook.com/me?fields=name&access_token=' + current_user_doc.fb;
                    fetch(url)
                        .then((resp) => resp.json())
                        .then(function(data){
                            // user_ref.update({
                            //     fb:current_user_doc.fb,
                            // }).then(function(){
                                $("#fb-btn").html('<i class="la la-facebook-official"></i>' + "Verified - " + data.name);
                                $("#fb-btn").prop('disabled', true);
                                form.fb = current_user_doc.fb;
                            // })
                        })
                        .catch(function(error) {
                            console.log(error);

                            $("#fb-btn").html('<i class="la la-facebook-official"></i>' + "Not Verified");
                            form.fb = null;
                            user_ref.update({
                                fb:'none'
                            }).then(unlinkFacebook)
                        });  

                        
                    
                }
                else
                {
                    $("#fb-btn").toggleClass("focused");
                    unlinkFacebook();
                }

                if (current_user_doc.DoB != "none")
                {
                    // let url = 'https://graph.facebook.com/me?fields=birthday&access_token=' + current_user_doc.fb;
                    // fetch(url)
                    //     .then((resp) => resp.json())
                    //     .then(function(data){
                    //             $("#DoB").html('<i class="la la-facebook-official"></i>' + "Verified - " + data.birthday);
                    //             $("#DoB").prop('disabled', true);
                    //             form.fb = current_user_doc.fb;
                            
                    //     })
                    //     .catch(function(error) {
                    //         console.log(error);

                    //         $("#fb-btn").html('<i class="la la-facebook-official"></i>' + "Not Verified");
                    //         form.fb = null;
                    //         user_ref.update({
                    //             fb:'none'
                    //         }).then(unlinkFacebook)
                    //     });  
                    var birthday =  current_user_doc.DoB.toDate();
                    console.log(birthday);
                    let birthday_string = getFormattedDate(birthday)
                    $('#DoB').data('daterangepicker').setStartDate(birthday);
                    $('#DoB').data('daterangepicker').setEndDate(birthday);
                    $("#DoB").val(birthday_string);
                    form.DoB = current_user_doc.DoB;
                    // $("#DoB").prop('disabled', true);
                }
                else
                {
                    $("#DoB").toggleClass("focused");
                    $("#DoB").val("");
                    $("#DoB").keyup(function(){
                        if ($("#DoB").val() != "")
                        {
                            $("#DoB").removeClass("focused");
                        }
                        else
                        {
                            $("#DoB").addClass("focused");
                        }
                    })
                }

                if (current_user_doc.phone != "none")
                {
                    $("#phone-btn").html("<i class='la la-phone'></i>" +  "Verified - " + current_user_doc.phone);
                    $("#phone-btn").prop('disabled', true);
                    form.phone = current_user_doc.phone;
                }
                else
                {
                    $("#phone-btn").toggleClass("focused");
                    
                }

                if (current_user_doc.personalID != "none" )
                {
                    if (current_user_doc.pending)
                    {
                        $("#personalID-btn").html("<i class='la la-credit-card'></i> Pending for Validation");
                        $("#personalID-btn").prop('disabled', true);
                        form.personalID = current_user_doc.personalID;
                    }
                    else
                    {
                        $("#personalID-btn").html("<i class='la la-credit-card'></i> Verified");
                        $("#personalID-btn").prop('disabled', true);
                        form.personalID = current_user_doc.personalID;
                    }
                }
                else
                {
                    $("#personalID-btn").html("<i class='la la-credit-card'></i> Not Verified");
                    $("#personalID-btn").toggleClass("focused");
                }
                if (current_user_doc.add_street != "none")
                {
                    $("#add_street").val(current_user_doc.add_street)
                }
                if (current_user_doc.add_ward != "none")
                {
                    $("#add_ward").val(current_user_doc.add_ward)
                }
                if (current_user_doc.add_district != "none")
                {
                    $("#add_district").val(current_user_doc.add_district)
                }
                if (current_user_doc.add_city != "none")
                {
                    $("#add_city").val(current_user_doc.add_city)
                }
                

                

                

                
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });


        
    } else {
         $("#user-nav").empty();
         $("#user-nav").append("<a href=\"../login\"\>Login/Register</a> ");
        //  alert("Please login kappa :v");
         window.location = '/../index.html';
    }
    });


// Nav Mobile

window.onload = function () {

    // Phone button

    firebase.auth().getRedirectResult().then(function (result) {
        
    
            var token = result.credential.accessToken;
    
            var user_ref = trigonic_DB.collection("users").doc(current_user.uid);
    
            let url = 'https://graph.facebook.com/me?fields=name&access_token=' + token;
                fetch(url)
                    .then((resp) => resp.json())
                    .then(function(data){
                        // console.log(data);
                        user_ref.update({
                            fb:token,
                        }).then(function(){
                        swal({
                            title: 'Successful!',
                            text: 'Your facebook account with the name: ' + data.name + ' is linked to your Trigonic account. ',
                            type: 'success',
                        
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                        })
                        })
                    })
                    .catch(function(error) {
                    // If there is any error you will catch them here
                    });  
    
        
        // The signed-in user info.
    
    }).catch(function (error) {
        console.log(error.code);
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
    });

    $("#submit_btn").click(submitForm);

    $("#phone-btn").click(function () {
                        
        if (form.phone != null)
        {
                
              
              swal({
                title: 'Unlink phone number',
                text: 'This account is already linked to the phone number ' + form.phone + '. Do you want to unlink?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, unlink phone number!'
              }).then((result) => {
                if (result.value) {
                  unlinkPhone();
                  
                }
              })
        }
        else
        {
            $("#phone-modal").modal('show');
        }
        
    });

    
    // Facebook Button

    $("#fb-btn").click(function () {
                        
        if (form.fb != null)
        {
            let url = 'https://graph.facebook.com/me?fields=name&access_token=' + form.fb;
              fetch(url)
                .then((resp) => resp.json())
                .then(function(data){
                    // console.log(data);
                    swal({
                        title: 'Facebook account linked!',
                        text: 'This account is already linked to the facebook account with the name: ' + data.name + '. ',
                        type: 'error',
                       
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok'
                      })
                })
                .catch(function(error) {
                // If there is any error you will catch them here
                });  

              
        }
        else
        {
            linkFacebook();
            
            // Login and link account with facebook
            
        }
        
    });


    $("#remove-class").removeClass("preload");
    $('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI
    
    
    $('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)
    
    $(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\"></a>");
    
    //Adds menu-mobile class (for mobile toggle menu) before the normal menu
    //Mobile menu is hidden if width is more then 1199px, but normal menu is displayed
    //Normal menu is hidden if width is below 1199px, and jquery adds mobile menu
    
    $(".menu > ul > li").hover(function (e) {
        if ($(window).width() > 1183) {
            $(this).children("ul").stop(true, false).fadeToggle(300);
            e.preventDefault();
        }
    });
    //If width is more than 943px dropdowns are displayed on hover
    
    $(".menu > ul > li").click(function () {
        if ($(window).width() <= 1183) {
            $(this).children("ul").fadeToggle(300);
        }
    });
    //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)
    
    $(".menu-mobile").click(function (e) {
        $(".menu > ul").toggleClass('show-on-mobile');
        e.preventDefault();
    });
    //when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)
   
}



    // Phone modals and Login

        // Phone number modal login
        var telInput = $("#phone"),
            errorMsg = $("#error-msg"),
            validMsg = $("#valid-msg");

        var tab_mobile = $(".tab-mobile");
        
        tab_mobile.eq(0).show();

        // telInput.intlTelInput({
        //     allowDropdown: false,
        //     initialCountry: "vn",
        //     utilsScript: "../js/utils.js"

        // });

        var input = document.querySelector("#phone");
        var iti = window.intlTelInput(input,{
            allowDropdown: false,
            initialCountry: "vn",
            utilsScript: "../js/utils.js"

        });


        // Event Binding 

         document.getElementById('phone-number').addEventListener('keyup', updateSignInButtonUI);
        document.getElementById('phone-number').addEventListener('change', updateSignInButtonUI);
        document.getElementById('verification-code').addEventListener('keyup', updateVerifyCodeButtonUI);
        document.getElementById('verification-code').addEventListener('change', updateVerifyCodeButtonUI);
        document.getElementById('verify-code-button').addEventListener('click', onVerifyCodeSubmit);
        

        // Init State
        $(document).ready(function () {

           

            $("#phone-modal-open").click(function () {
                $("#phone-modal").modal('show');
            });

            $("#phone-modal-close").click(function () {
                $("#phone-modal").modal('hide');
            });

            $("#phone-modal").on('shown.bs.modal', function () {
                $(document).keydown(function (e) {
                    if (e.keyCode == 9) {
                        e.preventDefault();
                    }
                });
            });

            var reset = function () {
                telInput.removeClass("error");
                errorMsg.addClass("hide");
                validMsg.addClass("hide");
            };
            // on blur: validate
            telInput.keyup(function () {
                updateSignInButtonUI();
                reset();
                if ($.trim(telInput.val())) {
                    if (iti.isValidNumber()) {
                        validMsg.removeClass("hide");
                        
                        // $('#login-mobile').prop('disabled', false);
                    }
                    else {
                        // $('#login-mobile').prop('disabled', true);
                        telInput.addClass("error");
                        errorMsg.removeClass("hide");
                    }
                }
            });

            $("#try-again").click(function(){
                tab_mobile.eq(1).hide();
                tab_mobile.eq(0).show();
        
        
                reset();
                telInput.val("");
        
                
        
            })

            // on keyup / change flag: reset
            // telInput.on("keyup change", reset);

            // Phone captcha

            window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('login-mobile', {
                'size': 'invisible',
                'callback': function (response) {
                    // reCAPTCHA solved, allow signInWithPhoneNumber.
                    onSignInSubmit();
                }
            });

            recaptchaVerifier.render().then(function (widgetId) {
                window.recaptchaWidgetId = widgetId;
                updateSignInButtonUI();

            });

        });


        


        // Function to handle Phone sign in

        function onSignInSubmit() {
            if (isPhoneNumberValid()) {
                window.signingIn = true;
                updateSignInButtonUI();
                var phoneNumber = getPhoneNumberFromUserInput();
                var appVerifier = window.recaptchaVerifier;
                firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                    .then(function (confirmationResult) {
                        // SMS sent. Prompt user to type the code from the message, then sign the
                        // user in with confirmationResult.confirm(code).
                        window.confirmationResult = confirmationResult;
                        window.signingIn = false;
                        // tab_mobile.eq(0).hide();
                        // tab_mobile.eq(1).show();
                        $("#phone-number").text(getPhoneNumberFromUserInput());
                        updateSignInButtonUI();
                        updateVerifyCodeButtonUI();
                        updateSignInFormUI();
                        updateVerificationCodeFormUI();
                    }).catch(function (error) {
                        // Error; SMS not sent
                        console.error('Error during signInWithPhoneNumber', error);
                        window.alert('Error during signInWithPhoneNumber:\n\n'
                            + error.code + '\n\n' + error.message);
                        window.signingIn = false;
                        updateSignInFormUI();
                        updateSignInButtonUI();
                    });
            }
        }
        /**
         * Function called when clicking the "Verify Code" button.
         */
        function onVerifyCodeSubmit(e) {
            e.preventDefault();
            if (!!getCodeFromUserInput()) {
                window.verifyingCode = true;
                updateVerifyCodeButtonUI();
                var code = getCodeFromUserInput();
                var current_credential = firebase.auth().currentUser;
                var credential = firebase.auth.PhoneAuthProvider.credential(confirmationResult.verificationId, code);
                current_credential.linkAndRetrieveDataWithCredential(credential).then(function(usercred) {
                    var user = usercred.user;
                    window.verifyingCode = false;
                    window.confirmationResult = null;
                    trigonic_DB.collection("users").doc(user.uid).update({
                        phone: user.phoneNumber
                    }).then(function(){
                        form.phone = user.phoneNumber;
                        $("#phone-btn").val("Verified");
                        $("#fb-btn").toggleClass("focused");

                        updateVerificationCodeFormUI();
                    })
                    
                    console.log("Account linking success", user);
                  }, function(error) {
                    console.log("Account linking error", error);
                    console.error('Error while checking the verification code', error);
                    window.alert('Error while checking the verification code:\n\n'
                        + error.code + '\n\n' + error.message);
                    window.verifyingCode = false;
                    updateSignInButtonUI();
                    updateVerifyCodeButtonUI();
                  });
                // confirmationResult.confirm(code).then(function (result) {
                //     // User signed in successfully.
                //     var user = result.user;
                //     window.verifyingCode = false;
                //     window.confirmationResult = null;
                //     updateVerificationCodeFormUI();
                //     writeUserData(user.uid,user.displayName,user.DoB, user.email, user.photoURL, user.gender, user.fbLink, user.phoneNumber);
                // }).catch(function (error) {
                //     // User couldn't sign in (bad verification code?)
                //     console.error('Error while checking the verification code', error);
                //     window.alert('Error while checking the verification code:\n\n'
                //         + error.code + '\n\n' + error.message);
                //     window.verifyingCode = false;
                //     updateSignInButtonUI();
                //     updateVerifyCodeButtonUI();
                // });
            }
        }
        /**
         * Cancels the verification code input.
         */
        function cancelVerification(e) {
            e.preventDefault();
            window.confirmationResult = null;
            updateVerificationCodeFormUI();
            updateSignInFormUI();
        }

        /**
     * Reads the verification code from the user input.
     */
        function getCodeFromUserInput() {
            return document.getElementById('verification-code').value;
        }

        /**
     * Reads the phone number from the user input.
     */
        function getPhoneNumberFromUserInput() {

            var intlNumber = iti.getNumber();
            return intlNumber;
        }

        function isPhoneNumberValid() {
            var isValid = iti.isValidNumber();
            return isValid;
        }

        /**
         * Re-initializes the ReCaptacha widget.
         */
        function resetReCaptcha() {
            if (typeof grecaptcha !== 'undefined'
                && typeof window.recaptchaWidgetId !== 'undefined') {
                grecaptcha.reset(window.recaptchaWidgetId);
            }
        }


        function updateSignInButtonUI() {
            document.getElementById('login-mobile').disabled =
                !isPhoneNumberValid()
                || !!window.signingIn;
        }

                /**
        * Updates the Verify-code button state depending on form values state.
        */
        function updateVerifyCodeButtonUI() {
            document.getElementById('verify-code-button').disabled =
                !!window.verifyingCode
                || !getCodeFromUserInput();
        }

                /**
        * Updates the state of the Sign-in form.
        */
        function updateSignInFormUI() {
            if (firebase.auth().currentUser || window.confirmationResult) {
            // document.getElementById('sign-in-form').style.display = 'none';
            tab_mobile.eq(0).hide();
            } else {
            resetReCaptcha();
            tab_mobile.eq(0).show();
            // document.getElementById('sign-in-form').style.display = 'block';
            }
        }
        /**
        * Updates the state of the Verify code form.
        */
        function updateVerificationCodeFormUI() {
            if (form.phone == null && window.confirmationResult) {

            tab_mobile.eq(1).show();
            // document.getElementById('verification-code-form').style.display = 'block';
            } else {

            tab_mobile.eq(1).hide();
            // if (firebase.auth().currentUser)
            // {
                $("#phone-modal").modal('hide');
                swal({
                    title: 'Successful!',
                    text: 'Phone Verification and linking is completed',
                    type: 'success'
                  });                  
                console.log(firebase.auth().currentUser);
                console.log("VERIFY COMPLETED");
                $("#phone-btn").html("<i class='la la-phone'></i>" +  "Verified - " + current_user_doc.phone);
                $("#phone-btn").prop('disabled', true);
                form.phone = current_user_doc.phone;
               
                
            // }
            // document.getElementById('verification-code-form').style.display = 'none';
            }
        }

        // End phone modal and login

        // Personal ID Verification

        var tab_pid = $(".tab-pid");

         // Init State
         $(document).ready(function () {
            tab_pid.eq(0).show();
            tab_pid.eq(1).hide();

            // Config stream of camera

             
            var width = 320;    // We will scale the photo width to this
            var height = 0;     // This will be computed based on the input stream

            // |streaming| indicates whether or not we're currently streaming
            // video from the camera. Obviously, we start at false.

            var streaming = false;

            // The various HTML elements we need to configure or control. These
            // will be set by the startup() function.

            var video = null;
            var canvas = null;
            var photo = null;
            var startbutton = null;
            var photo_file = null;
            var taken = false;
            var storageRef = firebase.storage().ref();

            var user_pid_ref = null;

            function startup() {
            
            video = document.getElementById('video');
            canvas = document.getElementById('canvas');
            photo = document.getElementById('photo');
            startbutton = document.getElementById('startbutton');
            verifybutton = document.getElementById('verify-pid-button');

            verifybutton.disabled = true;

            verifybutton.addEventListener('click', uploadPhoto);


            startbutton.addEventListener('click', function(ev){
            takepicture();
            ev.preventDefault();
            }, false);

            clearphoto();
            }

            // Fill the photo with an indication that none has been
            // captured.

            function clearphoto() {
            var context = canvas.getContext('2d');
            context.fillStyle = "#AAA";
            context.fillRect(0, 0, canvas.width, canvas.height);

            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);
            }

            // Capture a photo by fetching the current contents of the video
            // and drawing it into a canvas, then converting that to a PNG
            // format data URL. By drawing it on an offscreen canvas and then
            // drawing that to the screen, we can change its size and/or apply
            // other changes before drawing it.

            function uploadPhoto() {

                if (taken)
                {
                user_pid_ref  =  storageRef.child("users/" + current_user.uid + "/personalID.png");   
                Swal({
                    title: 'Uploading',
                    // confirmButtonText: 'Show my public IP',
                    text:
                      'Your Personal ID is being uploaded to our Cloud Server ' +
                      'Please wait a moment... ',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    onOpen: () => {
                        Swal.showLoading(),
                        clearphoto();
                                    }
                    
                  }); 
                  $("#personalID-modal").modal('hide');

                   user_pid_ref.put(photo_file)
                      .then(function(snapshot){
                          console.log("uploaded " + snapshot);
                          var user_ref = trigonic_DB.collection("users").doc(current_user.uid);
                          user_ref.update({
                            personalID:user_pid_ref.fullPath,
                            pending: true
                        }).then(function(){
                            Swal({
                                title: 'Personal ID Uploaded',
                                text: 'You look Great!',
                                type: 'success'
                              })
                        })

                            
                          
                          
                      }).catch((error) => {
                          Swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Upload failed: ' + error
                          }
                              
                          )});
                  
                }
            }

            function takepicture() {
            var context = canvas.getContext('2d');
            if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.translate(width, 0);
            context.scale(-1, 1);
            context.drawImage(video, 0, 0, width, height);

            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);
            
            canvas.toBlob(function(blob) {
               
                photo_file = blob // use the Blob or File API
                taken = true;
                verifybutton.disabled = false;
              });

            } else {
            clearphoto();
            }
            }

            // Set up our event listener to run the startup process
            // once loading is complete.
            window.addEventListener('load', startup, false);


            // $("#personalID-modal").modal('show');
            $("#personalID-modal").on("hidden.bs.modal", function () {
                console.log("modal closing");
                if (window.stream)
                window.stream.getTracks().forEach(function(track) { track.stop(); })
                
            });
            // PID Button
            $("#personalID-btn").click(function () {
                $("#personalID-modal").modal('show');
                tab_pid.eq(1).hide();
                tab_pid.eq(0).show();

            });

            $("#pid-modal-close").click(function () {
                $("#personalID-modal").modal('hide');
                
            });

            $("#personalID-modal").on('shown.bs.modal', function () {
                $(document).keydown(function (e) {
                    if (e.keyCode == 9) {
                        e.preventDefault();
                    }
                });
            });

            $("#next-pid").click(function () {
                tab_pid.eq(0).hide();
                tab_pid.eq(1).show();
               

            navigator.getMedia = ( navigator.getUserMedia ||
                                navigator.webkitGetUserMedia ||
                                navigator.mozGetUserMedia ||
                                navigator.msGetUserMedia);

            navigator.getMedia(
            {
            video: true,
            audio: false
            },
            function(stream) {
            if (navigator.mozGetUserMedia) {
                window.stream = stream;
                video.mozSrcObject = stream;
            } else {
                var vendorURL = window.URL || window.webkitURL;
                video.srcObject = stream;
                window.stream = stream;
                // video.src = vendorURL.createObjectURL(stream);
            }
            video.play();
            },
            function(err) {
            console.log("An error occured! " + err);
            }
            );

            video.addEventListener('canplay', function(ev){
            if (!streaming) {
            height = video.videoHeight / (video.videoWidth/width);

            // Firefox currently has a bug where the height can't be read from
            // the video, so we will make assumptions if this happens.

            if (isNaN(height)) {
                height = width / (4/3);
            }

            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);
            streaming = true;
            }
            }, false);
            });

         })

         

        // End Personal ID Verification


        // Facebook Login

        // var provider = new firebase.auth.FacebookAuthProvider();
        // firebase.auth().currentUser.linkWithPopup(provider).then(function(result) {
        //     // Accounts successfully linked.
        //     var credential = result.credential;
        //     var user = result.user;
        //     // ...
        //   }).catch(function(error) {
        //     // Handle Errors here.
        //     // ...
        //   });

        // End Facebook Login

        
    
// Login Facebook

function linkFacebook(){


    var provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('user_birthday');
    provider.addScope('user_location');
    provider.addScope('user_age_range');



    // var user_ref = trigonic_DB.collection("users").doc(current_user.uid);


    if (/Mobi|Android/i.test(navigator.userAgent)) 
    {
        firebase.auth().currentUser.linkWithRedirect(provider);
    }
    else
    {
        firebase.auth().currentUser.linkWithPopup(provider).then(function(result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        console.log(result);
        var token = result.credential.accessToken;

        var user_ref = trigonic_DB.collection("users").doc(current_user.uid);

        let url = 'https://graph.facebook.com/me?fields=name&access_token=' + token;
              fetch(url)
                .then((resp) => resp.json())
                .then(function(data){
                    // console.log(data);
                    user_ref.update({
                        fb:token,
                    }).then(function(){
                    swal({
                        title: 'Successful!',
                        text: 'Your facebook account with the name: ' + data.name + ' is linked to your Trigonic account. ',
                        type: 'success',
                       
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok'
                      })

                      $("#fb-btn").html('<i class="la la-facebook-official"></i>' + "Verified");
                      $("#fb-btn").prop('disabled',true);
                    })
                })
                .catch(function(error) {
                // If there is any error you will catch them here
                });  
        
       
        }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        console.log(error);
        // ...
        });
    }
}


// Unlink Phone

function unlinkPhone(){
    var provider = new firebase.auth.PhoneAuthProvider();
    firebase.auth().currentUser.unlink(provider.providerId).then(function(){
        var user_ref = trigonic_DB.collection("users").doc(current_user.uid);

        user_ref.update({
            phone:'none',
        }).then(function(){
            swal(
                'Successful!',
                'Your phone number has been unlinked.',
                'success'
              )

                $("#phone-btn").val("Not Verified");
                // $("#phone-btn").prop('disabled', true);
                form.phone = null;
                $("#phone-btn").toggleClass("focused");
        })
        

        console.log("unlinking phone", current_user);
    }).catch(function(error)
    {

    });
    
}

// Unlink FB


function unlinkFacebook(){
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().currentUser.unlink(provider.providerId).then(function(){
        var user_ref = trigonic_DB.collection("users").doc(current_user.uid);

        user_ref.update({
            fb:'none',
        }).then(function(){
            // swal(
            //     'Successful!',
            //     'Your facebook has been unlinked.',
            //     'success'
            //   )

                // $("#fb-btn").val("Not Verified");
                $("#fb-btn").prop('disabled', false);
                $("#fb-btn").html('<i class="la la-facebook-official"></i>' + "Not Verified");
                form.fb = null;
                // $("#fb-btn").toggleClass("focused");
        })
        

        // console.log("unlinking phone", current_user);
    }).catch(function(error)
    {

    });
    
}

// Submit Form

function submitForm() {
    var name = document.getElementById('full_name').value;
    var add_street = document.getElementById('add_street').value;
    var add_ward = document.getElementById('add_ward').value;
    var add_district = document.getElementById('add_district').value;
    var add_city = document.getElementById('add_city').value;

    form.name = name;
    console.log("Submit Form");
    if (form.photoFile) 
    {
        var storageRef = firebase.storage().ref();
        var user_profile_ref  =  storageRef.child("users/" + current_user.uid + "/profile.jpg");
        user_profile_ref.put(form.photoFile)
        .then(function(snapshot){
            snapshot.ref.getDownloadURL().then(function(downloadURL) {
                console.log('File available at', downloadURL);
                form.photoURL = downloadURL;
                    // Start uploading form data to current user doc
                    var user_ref = trigonic_DB.collection("users").doc(current_user.uid);

                        user_ref.update({
                            name: form.name || 'none',
                            DoB: form.DoB || 'none',
                            photoURL: form.photoURL || 'none',
                            add_street: add_street || 'none',
                            add_ward: add_ward || 'none',
                            add_district: add_district || 'none',
                            add_city: add_city || 'none',
                        }).then(function(){
                            swal(
                                'Successful!',
                                'You have updated your account!',
                                'success'
                              )

                              firebase.auth().signOut().then(function() {
                                // Sign-out successful.
                              }).catch(function(error) {
                                // An error happened.
                              });
                        })
                        

                        // console.log("unlinking phone", current_user);
                   


              });
        })

    }
    else
    {
        var user_ref = trigonic_DB.collection("users").doc(current_user.uid);

                        user_ref.update({
                            name: name || 'none',
                            DoB: form.DoB || 'none',
                            add_street: add_street || 'none',
                            add_ward: add_ward || 'none',
                            add_district: add_district || 'none',
                            add_city: add_city || 'none',
                        }).then(function(){
                            swal(
                                'Successful!',
                                'You have updated your account!',
                                'success'
                              )

                              firebase.auth().signOut().then(function() {
                                // Sign-out successful.
                              }).catch(function(error) {
                                // An error happened.
                              });
                        })
    }


    
    
}


// Format date 

function getFormattedDate(date) {
    var year = date.getFullYear();
  
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
  
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    
    return day + '/' + month + '/' + year;
  }

// Image uploader with Preview

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
            form.photoFile = input.files[0];
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
 