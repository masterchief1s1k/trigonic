
	var conversation_list = new Array();
	var current_conversation = null;
	var current_user;
	var previous_time = 0;
	var current_conversation_snapshot = null;
	


window.onload = function () {


	$('.conversation-list').on('click','li', function(){
		// console.log($(this).attr("data-id"));
		var clicked_id = $(this).attr("data-id");
		console.log((conversation_list.filter(function(e) { return e.id === clicked_id; }).length > 0));
		if (current_conversation != clicked_id && 
			(conversation_list.filter(function(e) { return e.id === clicked_id; }).length > 0))
		{
		
		$(this).addClass('active').siblings().removeClass('active');
		if (current_conversation_snapshot != null)
		{
			current_conversation_snapshot();
		}
		loadConversation(clicked_id);
		current_conversation = clicked_id;
		}
	 });
    // Catch Login Status
    firebase.auth().onAuthStateChanged(function(user) {
       if (user) {
		   current_user = user;
			$("#user-nav").empty();
        	$("#user-nav").append("<a class=\"signout\" href=\"#\"\><i class=\"fa fa-user\" style=\"font-size: 20px;\"></i></a> ");
			console.log(user);
			$('.signout').click(signout);
			fetchConversationList(user);
		   
       } else {
			$("#user-nav").empty();
			$("#user-nav").append("<a href=\"login.html\"\>Login/Register</a> ");
			alert("Please login kappa :v");
			window.location = '/../login.html';
       }
       });
	
	   	
		$('.menu > ul > li:has( > ul)').addClass('menu-dropdown-icon');
		//Checks if li has sub (ul) and adds class for toggle icon - just an UI


		$('.menu > ul > li > ul:not(:has(ul))').addClass('normal-sub');
		//Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

		$(".menu > ul").before("<a href=\"#\" class=\"menu-mobile\"></a>");

		//Adds menu-mobile class (for mobile toggle menu) before the normal menu
		//Mobile menu is hidden if width is more then 1199px, but normal menu is displayed
		//Normal menu is hidden if width is below 1199px, and jquery adds mobile menu

		$(".menu > ul > li").hover(function (e) {
			if ($(window).width() > 1183) {
				$(this).children("ul").stop(true, false).fadeToggle(300);
				e.preventDefault();
			}
		});
		//If width is more than 943px dropdowns are displayed on hover

		$(".menu > ul > li").click(function () {
			if ($(window).width() <= 1183) {
				$(this).children("ul").fadeToggle(300);
			}
		});
		//If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

		$(".menu-mobile").click(function (e) {
			$(".menu > ul").toggleClass('show-on-mobile');
			e.preventDefault();
		});
		//when clicked on mobile-menu, normal menu is shown as a list, classic rwd menu story (thanks mwl from stackoverflow)


		// Chat Handler





		$(".message").mCustomScrollbar().mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
	


		$(".btn-send").click(function(){
			if (document.getElementById("t1").value.replace(/\s/g, "").length > 0)
			{
				var msg_batch = trigonic_DB.batch();
				var msg_ref = trigonic_DB.collection('conversations/' + current_conversation + '/messages/').doc();
				var con_ref = trigonic_DB.collection('conversations').doc(current_conversation);
				
				var content = document.getElementById("t1").value;
				var now = new Date();
				var timestamp = firebase.firestore.Timestamp.fromDate(now);
				
				// console.log(split("DFzoJQoXAxWyRPDmN3J4BFHFdzy2=1535198661619=again== =s asd =asd =haha",/=/g,2));
				// var display_time = CalculateTime(timestamp);
				msg_batch.set(msg_ref, {
					content:content,
					date: timestamp,
					sender_id: current_user.uid
				});
				msg_batch.update(con_ref, {
					last_msg: current_user.uid + "=" + timestamp.toMillis() + "=" + content,
					read: false
				})
				msg_batch.commit().then(function(){
					// console.log("YEAH");
				})

				// if((now - previous_time) > 8640000)
				// {
				// 	renderTimes(CalculateTime(display_time));
				// }
				// previous_time = now;
				// renderMessages("right",
				// document.getElementById("t1").value,current_user.photoURL, ("0" + now.getHours()).substr(-2) + ":" + ("0" + now.getMinutes()).substr(-2))
			}
		});

		$("#t1").on('keydown', function (e){
			if (e.keyCode == 13)
			{
				e.preventDefault();
			}
		})

		$("#t1").on('keyup', function (e) {

			var length = document.getElementById("t1").value.replace(/\s/g, "").length;
			if (e.keyCode == 13 ) {
				if(e.shiftKey)
				{
					console.log("SHIFT");
					document.getElementById("t1").value = document.getElementById("t1").value + "\n";
				}
				else
				{
					
						$( ".btn-send" ).trigger( "click" );
					
				}
				// Do something
			}
		});
	
}

function split(str, sep, n) {
    var out = [];

    while(n--) out.push(str.slice(sep.lastIndex, sep.exec(str).index));

    out.push(str.slice(sep.lastIndex));
    return out;
}



async function fetchConversationList(user){
	

	var conversations = trigonic_DB.collection('users/' + user.uid + '/conversations');
	conversations.onSnapshot(function(snapshot) {
		snapshot.docChanges().forEach(function(change) {
			var doc = change.doc;
				if (change.type == "added")
				{
					console.log("snapshot conversation");
						// doc.data() is never undefined for query doc snapshots
						// console.log(doc.id, " => ", doc.data());
						var id;
						var name;
						var message;
						var avatar;
						var date;
						var read;
						var timestamp;
						var last_msg;

						doc.data().ref.onSnapshot(function(doc){

							
								
								// REF TO A CONVERSATION
								read = doc.data().read;
								id = doc.id;
								last_msg = split(doc.data().last_msg,/=/g,2);
								message = last_msg[2];
								timestamp = firebase.firestore.Timestamp.fromMillis(parseInt(last_msg[1]));
								date = CalculateTime(timestamp);

								if (user.uid = last_msg[0])
									{
										message = "You: " + message;
									}
									// else
									// {
									// 	message = message;
									// }
								
								var result = doc.data().members.find(function(element){
									return element.id != user.uid;
								});
								name = result.name;
								avatar = (result.photoURL === "none" ) ? null : result.photoURL;


								// var filter = conversation_list.filter(function(list_item) { return list_item.id === id; });

								var filter = conversation_list.findIndex(function(list_item){ return list_item.id === id;});
								if (filter >= 0)
								{
									console.log(filter);
									conversation_list[filter] = {id:id, name:name, message: message, avatar: avatar, date: date, read: read, timestamp: timestamp};
									updateConversations(id,message,date,read,timestamp);
									// Exist conversation in conversation_list, update conversation

								}
								else
								{
									// console.log("none");
									// Non-exist conversation in conversation_list, create a new conversation
									conversation_list.push({id:id, name:name, message: message, avatar: avatar, date: date, read: read, timestamp: timestamp});	
									renderConversations(id,name,message,avatar,date,read,timestamp);
									
								}
								
						})
				}
				
		});
	})

	// conversations.onSnapshot(function(snapshot){
	// 	snapshot.docChanges().forEach(function(change) {
    //         if (change.type === "added") {
	// 			console.log("added");
	// 		}
	// 	})
	// 	})

}

function loadConversation(conversation_id){
	console.log("loading conversation ID num: " + conversation_id);

	var conversation = conversation_list.find(function(e) { return e.id === conversation_id; });
	var left_profile = conversation.avatar;
	var right_profile = current_user.photoURL;


	// console.log(conversation);
	// Clear current conversation
	var msgList = document.getElementsByClassName("messages-list")[0];
	while (msgList.firstChild) {
		msgList.removeChild(msgList.firstChild);
	}
	var header = document.getElementsByClassName("headRight-sub")[0];
	while (header.firstChild) {
		header.removeChild(header.firstChild);
	}
	var name = document.createElement("H3");
	name.textContent = conversation.name;

	header.appendChild(name);

	// Render all available messages

	

	var messages = trigonic_DB.collection('conversations/' + conversation.id + '/messages/');
	current_conversation_snapshot = messages.orderBy('date').onSnapshot(function(snapshot){
		snapshot.docChanges().forEach(function(change) {
            if (change.type === "added") {

				console.log("New message: ", change.doc.data());
				var current_time = new Date(change.doc.data().date.seconds*1000);
				var time_diff = Math.abs(current_time - previous_time);
				
				var date = CalculateTime(change.doc.data().date);
				var msg_time = ("0" + current_time.getHours()).substr(-2) + ":" + ("0" + current_time.getMinutes()).substr(-2);
				if(time_diff > 8640000)
				{
					
					renderTimes(date);
				}
				
				if (change.doc.data().sender_id != current_user.uid)
				{
					renderMessages("left",change.doc.data().content,left_profile,msg_time);
				}
				else
				{
					renderMessages("right",change.doc.data().content,right_profile,msg_time);
				}

				previous_time = current_time;
				// doc.data().sender_id
				// renderMessages(side,content,image,date);
				// renderTimes(date);
				
            							}
		})
	})
	
	// Old method 
	// current_conversation_snapshot = messages.orderBy('date').get().then(function(querySnapshot) {
	// 	querySnapshot.forEach(function(doc){
	// 		console.log(doc.data());
	// 		var current_time = new Date(doc.data().date.seconds*1000);
	// 		var time_diff = Math.abs(current_time - previous_time);
			
	// 		var date = CalculateTime(doc.data().date);
	// 		console.log(doc.data().date);
	// 		var msg_time = ("0" + current_time.getHours()).substr(-2) + ":" + ("0" + current_time.getMinutes()).substr(-2);
	// 		if(time_diff > 8640000)
	// 		{
				
	// 			renderTimes(date);
	// 		}
			
	// 		if (doc.data().sender_id != current_user.uid)
	// 		{
	// 			renderMessages("left",doc.data().content,left_profile,msg_time);
	// 		}
	// 		else
	// 		{
	// 			renderMessages("right",doc.data().content,right_profile,msg_time);
	// 		}

	// 		previous_time = current_time;
	// 		// doc.data().sender_id
	// 		// renderMessages(side,content,image,date);
	// 		// renderTimes(date);
	// 	})
	// });





}

function CalculateTime(timestamp){
	
	var now = new Date();
	var added_date = new Date(timestamp.seconds*1000);
	var time_diff = Math.abs(now - added_date);
	if(time_diff < 8640000)
	{
		
		return ("0" + added_date.getHours()).substr(-2) + ":" + ("0" + added_date.getMinutes()).substr(-2);
	}
	else if (time_diff < 6*24*360*1000)
	{
		var weekday = new Array(7);
		weekday[0] =  "Sun";
		weekday[1] = "Mon";
		weekday[2] = "Tues";
		weekday[3] = "Wed";
		weekday[4] = "Thu";
		weekday[5] = "Fri";
		weekday[6] = "Sat";
		return weekday[added_date.getDay()];
	}
	else if (time_diff < 366*24*360*1000)
	{
		var month = new Array();
		month[0] = "January";
		month[1] = "February";
		month[2] = "March";
		month[3] = "April";
		month[4] = "May";
		month[5] = "June";
		month[6] = "July";
		month[7] = "August";
		month[8] = "September";
		month[9] = "October";
		month[10] = "November";
		month[11] = "December";
		return added_date.getDate() + " " + month[added_date.getMonth()];
	}
	else
	{
		return added_date.getDate() + "/" +(added_date.getMonth()+1) + "/" +added_date.getFullYear();
 	}
	
}

function updateConversations(id,message,date,read,timestamp){
	var conList = document.getElementsByClassName("conversation-list")[0];
	var updateListItem = conList.querySelector("[data-id = "+id+"]");
	updateListItem.setAttribute("timestamp", timestamp.seconds);
	var timeDiv = updateListItem.querySelector(".time");
	var noti = updateListItem.querySelector(".fa");
	var textDiv = updateListItem.querySelector(".mess");
	textDiv.textContent = message;
	timeDiv.textContent = date;

	// if (!read)
	// {
	// 	noti.style.display = "block";
	// }
	// else
	// {
	// 	noti.style.display = "none";
	// }

	sortList(conList);

}

function renderConversations(id,name,message,avatar,date,read,timestamp){
	var conList = document.getElementsByClassName("conversation-list")[0];
	var listItem = document.createElement("LI");
	listItem.setAttribute("data-id", id);
	listItem.setAttribute("timestamp", timestamp.seconds);
	var itemContent = document.createElement("DIV");
	itemContent.className = "chatList";
	var contentImage = document.createElement("DIV");
	contentImage.className = "img";
	var profileImage = document.createElement("IMG");
	var noti = document.createElement("I");
		noti.className = "fa fa-circle";
		contentImage.appendChild(noti);

	// if (!read)
	// {
	// 	noti.style.display = "block";
	// }
	

	if (avatar !== null)
	{
		profileImage.src = avatar;
	}

	else
	{
		profileImage.src = 'https://ui-avatars.com/api/?name=' + name;
	}

	contentImage.appendChild(profileImage);


	var contentMessage = document.createElement("DIV");
	contentMessage.className = "desc";

	var timeDiv = document.createElement("SMALL");
	timeDiv.className = "time";
	timeDiv.textContent = date;

	var nameDiv = document.createElement("H5");
	nameDiv.className = "name";
	nameDiv.textContent = name;
	
	var textDiv = document.createElement("SMALL");
	textDiv.className = "mess";
	textDiv.textContent = message;

	
	// Render elements to DOM


	contentMessage.appendChild(timeDiv);
	contentMessage.appendChild(nameDiv);
	contentMessage.appendChild(textDiv);

	itemContent.appendChild(contentImage);
	itemContent.appendChild(contentMessage);

	listItem.appendChild(itemContent);

	conList.insertBefore(listItem, conList.firstChild);
	sortList(conList);

}

function sortList(ul){
	var new_ul = ul.cloneNode(false);
	// ['onclick', 'onmouseover'].forEach(function(method) {
	// 	new_ul[method] = ul[method];
	// });

    // Add all lis to an array
    var lis = [];
    for(var i = ul.childNodes.length; i--;){
        if(ul.childNodes[i].nodeName === 'LI')
            lis.push(ul.childNodes[i]);
    }

    // Sort the lis in descending order
    lis.sort(function(a, b){
       return b.getAttribute("timestamp") - a.getAttribute("timestamp");
    });

    // Add them into the ul in order
    for(var i = 0; i < lis.length; i++)
        new_ul.appendChild(lis[i]);
	ul.parentNode.replaceChild(new_ul, ul);
	
	$('.conversation-list').on('click','li', function(){
		// console.log($(this).attr("data-id"));
		var clicked_id = $(this).attr("data-id");
		console.log((conversation_list.filter(function(e) { return e.id === clicked_id; }).length > 0));
		if (current_conversation != clicked_id && 
			(conversation_list.filter(function(e) { return e.id === clicked_id; }).length > 0))
		{
		$(this).addClass('active').siblings().removeClass('active');
		if (current_conversation_snapshot != null)
		{
			current_conversation_snapshot();
		}
		loadConversation(clicked_id);
		current_conversation = clicked_id;
		}
	 });
}


function renderTimes(date){
	var msgList = document.getElementsByClassName("messages-list")[0];
	var item = document.createElement("LI");
	item.className = "msg-day";
	var container = document.createElement("SMALL");
	container.textContent = date;
	item.appendChild(container);
	msgList.appendChild(item);
}

function renderMessages(side,content,image,date){
	// var msgList = document.getElementsByClassName("msg-list");
	var outerclass;
	var innerclass;
	if (side == "right")
		{
			outerclass = "msg-right";
			innerclass = "msg-right-sub";
		}
	else
		{
			outerclass = "msg-left";
			innerclass = "msg-left-sub";
		}
	var outer = document.createElement("LI");
	outer.className = outerclass;
	var sub = document.createElement("DIV");
	sub.className = innerclass;
	var img = document.createElement("IMG");
	img.src = image;
	var msg = document.createElement("DIV");
	msg.className = "msg-desc";
	msg.textContent = content;
	var timestamp = document.createElement("SMALL");
	timestamp.textContent = date;
	outer.appendChild(sub);
	sub.appendChild(img);
	sub.appendChild(msg);
	sub.appendChild(timestamp);


	$(".messages-list").append(outer);

	// Remove textarea and scroll to bottom to show new messages

	// $(".message").mCustomScrollbar("scrollTo","bottom");
	$(".message").mCustomScrollbar().mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
	document.getElementById("t1").value = "";
	check();
	// <li class="msg-right">
	// 						<div class="msg-left-sub">
	// 							<img src="../images/man04.png">
	// 							<div class="msg-desc">
	// 								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	// 								tempor incididunt ut labore et dolore magna aliqua.
	// 							</div>
	// 							<small>05:25 am</small>
	// 						</div>
	// 					</li>
}

function signout(){
	console.log("start sign out");
	firebase.auth().signOut().then(function() {
		console.log("sign out");
	  }).catch(function(error) {
		// An error happened.
		console.log(error);
	  });
}