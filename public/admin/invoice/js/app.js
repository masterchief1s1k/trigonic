function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  
    // we'll store the parameters here
    var obj = {};
  
    // if query string exists
    if (queryString) {
  
      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];
  
      // split our query string into its component parts
      var arr = queryString.split('&');
  
      for (var i = 0; i < arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');
  
        // set parameter name and value (use 'true' if empty)
        var paramName = a[0];
        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
  
        // // (optional) keep case consistent
        // paramName = paramName.toLowerCase();
        // if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();
  
        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
        if (paramName.match(/\[(\d+)?\]$/)) {
  
          // create key if it doesn't exist
          var key = paramName.replace(/\[(\d+)?\]/, '');
          if (!obj[key]) obj[key] = [];
  
          // if it's an indexed array e.g. colors[2]
          if (paramName.match(/\[\d+\]$/)) {
            // get the index value and add the entry at the appropriate position
            var index = /\[(\d+)\]/.exec(paramName)[1];
            obj[key][index] = paramValue;
          } else {
            // otherwise add the value to the end of the array
            obj[key].push(paramValue);
          }
        } else {
          // we're dealing with a string
          if (!obj[paramName]) {
            // if it doesn't exist, create property
            obj[paramName] = paramValue;
          } else if (obj[paramName] && typeof obj[paramName] === 'string'){
            // if property does exist and it's a string, convert it to an array
            obj[paramName] = [obj[paramName]];
            obj[paramName].push(paramValue);
          } else {
            // otherwise add the property
            obj[paramName].push(paramValue);
          }
        }
      }
    }
  
    return obj;
  }
  BS.prototype.onAuthStateChanged = function(user){


  }


function BS(){
  this.checkSetup();
  
  //shortcut to DOM elements
  this.initFirebase();
}


// Sets up shortcuts to Firebase features and initiate firebase auth.
BS.prototype.initFirebase = function(){
  //shortcut to firebase SDK features
  this.auth = firebase.auth();
  this.database = firebase.database();   
  this.firestore = firebase.firestore();
  // Initiates Firebase auth and listen to auth state changes.
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};


// Checks that the Firebase SDK has been correctly setup and configured.
BS.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
       window.alert('Wrong configuration, Please contact admin.');
  }
};


// Triggers when the auth state change for instance when the user signs-in or signs-out.
BS.prototype.onAuthStateChanged = function(user){
  if (user) { // User is signed in!
      
      firebase.auth().currentUser.getIdTokenResult(true).then((idTokenResult) => {
      if (idTokenResult.claims.admin) { // 3
          // window.location = 'index.html';
      } else {
          console.log("Not an admin");
          setTimeout(function(){ window.location = '../login.html'; }, 3000);
          Swal.fire(
              'Access Denied',
              'Chưa login ai cho xài? Bấm OK để Login',
              'error'
            ).then(function () {
                window.location = '../login.html';
          });
          }
      })
      .catch((error) => {
              console.log("Token error: ",error);
          });                 
  } else { // User is signed out!
    setTimeout(function(){ window.location = '../login.html'; }, 3000);
    Swal.fire(
        'Access Denied',
        'Chưa login ai cho xài? Bấm OK để Login',
        'error'
      ).then(function () {
          window.location = '../login.html';
    });
  }
};



window.onload = function() {
  window.BS = new BS();    
    
};

  (function ($) {


$(function () {

    
    var types = ["Sneaker", "giày da", "dép & sandal", "high heels"]
    var clean_num = [0, 0, 0, 0];
    var clean_cost = [119000,169000,69000,99000]
    var repaint_list = [];
    var repaint_dict_price = {
        "de": 299000,
        "than":399000,
        "full":599000
      };
    var repaint_dict_name = {
        "de": "Repaint Đế Giày",
        "than":"Repaint Thân Giày",
        "full":"Repaint Full"
    };
    console.log(getAllUrlParams().orderid);
    var orderid = getAllUrlParams().orderid;
    var customerid;
    bluesicht_DB.collection("orders").doc(orderid).get().then(function(doc){
        
        if (doc.exists) {
            $("#invoice-id").text(doc.id);
            $("#date").text(moment(doc.data().timestamp.toDate()).format("h:mm, DD, MMM, YYYY"));
            console.log("Document data:", doc.data());
            customerid = doc.data().customid;
            bluesicht_DB.collection("users").where('customid', "==", customerid).get().then(function(querySnapshot){
                querySnapshot.forEach(function(doc) {
                    // doc.data() is never undefined for query doc snapshots
                    console.log(doc.id, " => ", doc.data());
                    let data = doc.data();
                    $("#name").text(data.name);
                    $("#phone").text(data.phone);
                    $("#email").text(data.email);
                    

                });
            }).then(function(){
                bluesicht_DB.collection("shoes").where("order_id", "==", orderid).get().then(function(querySnapshot){
                    querySnapshot.forEach(function(doc) {
                        // doc.data() is never undefined for query doc snapshots
                        console.log(doc.id, " => ", doc.data());
                        let data = doc.data();
                        var type_int = parseInt(data.type,  10)
                        if (data.clean)
                        {
                            clean_num[type_int-1] = clean_num[type_int-1] + 1;
                            // $(".item-row:last").after('<tr class="item-row"><td class="item-name"><span>Chăm sóc Sneaker</span><div class="delete-wpr"></div></td><td class="description"><span>Chăm sóc '+ types[type_int-1] +'</span></td><td><span class="cost"> Vệ sinh ' + types[type_int-1] + ' </span></td><td><span class="qty"></span></td><td><span class="price"></span></td></tr>');
                
                        }
                        if (data.repaint != 'none')
                        {
                            var shoe_repaint = {type:type_int, repaint:data.repaint }
                        repaint_list.push(shoe_repaint)
                        }
                        
            
                       
                        
            
                    });
                }).then(function(){
                    console.log(clean_num)
                    console.log(repaint_list)
                        var type_iter = 0;
                        clean_num.forEach(function(e){
                            if (e > 0) { 
                                $(".item-row:last").after('<tr class="item-row"><td class="item-name"><span>Chăm sóc '+ types[type_iter] +'</span><div class="delete-wpr"></div></td><td class="description"><span class="cost"> Vệ sinh ' + types[type_iter] + ' </span></td><td><span class="cost">' + clean_cost[type_iter] + ' </span></td><td><span class="qty"> '  +  e + '</span></td><td><span class="price">' + clean_cost[type_iter]*e + '</span></td></tr>');
                              }
                            type_iter++;
                        })
                        repaint_list.forEach(function(r){
                            $(".item-row:last").after('<tr class="item-row"><td class="item-name"><span>Chăm sóc '+ types[r.type] +'</span><div class="delete-wpr"></div></td><td class="description"><span class="cost"> ' + repaint_dict_name[r.repaint] + ' </span></td><td><span class="cost">' + repaint_dict_price[r.repaint] + ' </span></td><td><span class="qty"> '  +  1 + '</span></td><td><span class="price"> ' +  repaint_dict_price[r.repaint] + '</span></td></tr>');
                              
                        })
            
                        update_total();
                }).catch(function(error) {
                    console.log("Error getting documents: ", error);
                });
            }).catch(function(error) {
                console.log("Error getting documents: ", error);
            });
        }
    })
    
});

$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');


    calculateDiscount();
})

$("#discount").blur(function(){
    console.log("Bluring");
    console.log();
    
    calculateDiscount();
})


$('textarea').keypress(function(e) {
    var a = [];
    var k = e.which;

    for (i = 48; i < 58; i++)
        a.push(i);

    if (!(a.indexOf(k)>=0))
        e.preventDefault();
});

function calculateDiscount() {
    var total = 0;

    $('.price').each(function(i){
        price = $(this).html().replace("vnd","");
        if (!isNaN(price)) total += Number(price);
      });

      console.log(total);

      discount = $('#discount').val();
      discount_type = $('.active').attr("data-title");

      if (discount_type == "cash")
      {
        console.log(discount)
        new_total = total - parseInt(discount);
        $("#total").text(new_total + "vnd" );
      }
      else
      {
        new_total = total - total*parseInt(discount)/100;
        $("#total").text(Math.max(0,new_total) + "vnd");
      }

      

}
function update_total() {
    var total = 0;
    $('.price').each(function(i){
      price = $(this).html().replace("vnd","");
      if (!isNaN(price)) total += Number(price);
      console.log(price)
    });
  
    // total = roundNumber(total,3);
  
    $('#subtotal').html(total + "vnd");
    $('#total').html(total + "vnd");

    // window.print();
    
    // update_balance();
  }
  
  function update_balance() {
    var due = $("#total").html().replace("vnd","") - $("#paid").val().replace("vnd","");
    due = roundNumber(due,2);
    
    $('.due').html (due + "vnd");
  }

})(jQuery);