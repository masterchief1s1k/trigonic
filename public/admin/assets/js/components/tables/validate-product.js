function getAllUrlParams(url) {

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
  
    // we'll store the parameters here
    var obj = {};
  
    // if query string exists
    if (queryString) {
  
      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];
  
      // split our query string into its component parts
      var arr = queryString.split('&');
  
      for (var i = 0; i < arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');
  
        // set parameter name and value (use 'true' if empty)
        var paramName = a[0];
        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];
  
        // // (optional) keep case consistent
        // paramName = paramName.toLowerCase();
        // if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();
  
        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
        if (paramName.match(/\[(\d+)?\]$/)) {
  
          // create key if it doesn't exist
          var key = paramName.replace(/\[(\d+)?\]/, '');
          if (!obj[key]) obj[key] = [];
  
          // if it's an indexed array e.g. colors[2]
          if (paramName.match(/\[\d+\]$/)) {
            // get the index value and add the entry at the appropriate position
            var index = /\[(\d+)\]/.exec(paramName)[1];
            obj[key][index] = paramValue;
          } else {
            // otherwise add the value to the end of the array
            obj[key].push(paramValue);
          }
        } else {
          // we're dealing with a string
          if (!obj[paramName]) {
            // if it doesn't exist, create property
            obj[paramName] = paramValue;
          } else if (obj[paramName] && typeof obj[paramName] === 'string'){
            // if property does exist and it's a string, convert it to an array
            obj[paramName] = [obj[paramName]];
            obj[paramName].push(paramValue);
          } else {
            // otherwise add the property
            obj[paramName].push(paramValue);
          }
        }
      }
    }
  
    return obj;
  }

(function ($) {

	'use strict';

$(function () {

	var userid = getAllUrlParams().userid;
	function format ( d,url ) {

		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			'<tr>'+
				'<td>Order ID:</td>'+
				'<td>'+d.orderID+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Customer ID:</td>'+
				'<td>'+d.customerID+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Delivery:</td>'+
				'<td>'+d.Delivery+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Address:</td>'+
				'<td>'+d.Address+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>City:</td>'+
				'<td>'+d.City+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Note:</td>'+
				'<td>'+d.Note+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Validate this product:</td>'+
				'<td> <button class="btn btn-primary" ProductID="'+ d.orderID +'" id="confirm-btn"> Print </button> </td>'+
			'</tr>'+
			// '<tr>'+
			// 	'<td>Test button:</td>'+
			// 	'<td> <button id="fun-btn"> Test </button> </td>'+
			// '</tr>'+
			
		'</table>';
	}
	
	var table = $('#validate-table').DataTable({			
		rowId: 'orderID',
		columns : [
			// {
            //     "className":      'details-control',
            //     "orderable":      false,
            //     "data":           null,
            //     "defaultContent": ''
            // },
			{ data : 'orderID' },
			{ data : 'Time' },
			{ data : 'customerID' },
			{ data : 'customer Name' },
			{ data : 'Delivery' },
			{ data : 'Address' },
			{ data : 'City' },
			{ data : 'Note' },
			{ data : 'Action' },	
			
			
		],
		"order": [[ 2, "desc" ]],
		"columnDefs": [ {
			"targets": -1,
			"data": null,
			"defaultContent": "<button class='btn btn-primary' id='receipt' >Print Receipt</button> <button class='btn btn-secondary' id='orders' >Edit Orders</button> <button class='btn btn-info' id='shoes' >View Shoes</button>"
		} ]
	});

	$.fn.dataTable.ext.errMode = 'none';
	bluesicht_auth.onAuthStateChanged(function(user) {
		if (user) {
			console.log(user);
				bluesicht_auth.currentUser.getIdTokenResult()
				.then((idTokenResult) => {
						if (userid != null)
						{
							var userRef = bluesicht_DB.collection("users").doc(userid);
							userRef.get().then(function(doc) {
								if (doc.exists) {
									console.log("Document data:", doc.data());
									bluesicht_DB.collection('orders').where("customid", "==", doc.data().customid).onSnapshot(function(snapshot) {
																
										snapshot.docChanges().forEach(function(change){
											console.log(change.doc.data());
		
											var dataObject = {
												orderID: change.doc.id,
												Time: moment(change.doc.data().timestamp.toDate()).format("h:mm, DD, MMM, YYYY") ,
												customerID: change.doc.data().customid,
												'customer Name': change.doc.data().name,
												Delivery: change.doc.data().delivery,
												Address: change.doc.data().address,
												City: change.doc.data().city,
												Note: change.doc.data().note,
												// Action: change.doc.data().Pending,										
		
											};
												console.log(dataObject);
											if (table.rows('[id="'+dataObject.orderID+'"]').any())
											{
												table.rows('[id="'+dataObject.orderID+'"]').remove();
												table.row.add(dataObject).draw();	
												console.log("Already exist!");
											}
											else
											{
												// if (change.doc.data().Pending == true)
												table.row.add(dataObject).draw();
												
											}
											
										})
									});
								} else {
									// doc.data() will be undefined in this case
									console.log("No such document!");
								}
							}).catch(function(error) {
								console.log("Error getting document:", error);
							});
						}
						else
						{

						
							bluesicht_DB.collection('orders').onSnapshot(function(snapshot) {
																
								snapshot.docChanges().forEach(function(change){

									// var current_ref = change.doc.ref;
									// bluesicht_DB.collection('users').where("customid", "==", change.doc.data().customid ).get().then(function(userQuery){
										
									// 	userQuery.forEach(function(user) {

									// 		current_ref.update({
									// 			name:user.data().name
									// 		})

											var dataObject = {
												orderID: change.doc.id,
												Time: moment(change.doc.data().timestamp.toDate()).format("h:mm, DD, MMM, YYYY") ,
												customerID: change.doc.data().customid,
												'customer Name': change.doc.data().name,
												Delivery: change.doc.data().delivery,
												Address: change.doc.data().address,
												City: change.doc.data().city,
												Note: change.doc.data().note,
												// Action: change.doc.data().Pending,										
		
											};
												// console.log(dataObject);
											if (table.rows('[id="'+dataObject.orderID+'"]').any())
											{
												table.rows('[id="'+dataObject.orderID+'"]').remove();
												table.row.add(dataObject).draw();	
												// console.log("Already exist!");
											}
											else
											{
												// if (change.doc.data().Pending == true)
												table.row.add(dataObject).draw();
												
											}
										
										
									
									
									
								})
							});
						}
						
					})
				.catch((error) => {
					console.log(error);
				});

		} else {
			console.log("not a user");
			
			// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
		}
	});
	$('#validate-table tbody').on('click', 'td button#orders',async function () {
	});

	$('#validate-table tbody').on('click', 'td button#shoes',async function () {
	});

	$('#validate-table tbody').on('click', 'td button#receipt',async function () {
		var tr = $(this).closest('tr');
		console.log("pressing")
		console.log($(this).parents('tr') );
		var data = table.row( $(this).parents('tr') ).data();
		console.log(data);
        window.open("invoice?orderid=" +data.orderID );
		// var row = table.row( tr );		
		// if ( row.child.isShown() ) {
		// 	// This row is already open - close it
		// 	row.child.hide();
		// 	tr.removeClass('shown');			
		// }
		// else {			
		// 	var cid = row.data().ProductID;			
		// 	var url1,url2,url3;
		// 	await storageRef.child(`DatabaseImages/${cid}/1.jpg`).getDownloadURL().then(function(url){
		// 		url1 = url;
		// 	});			
		// 	await row.child( format(row.data(),url1) ).show();
		// 	tr.addClass('shown');			
		// 	// $("#tr td:confirm-btn").on( 'click', 'td.confirm-btn', function(){
		// 	$("#confirm-btn").click(function(){
		// 		var current_product_id = $(this).attr("ProductID");
		// 		// call function to validate (current_product_id);				
		// 		var validateProduct = firebase.functions().httpsCallable('validateProductCallable');		
		// 		validateProduct({uid:current_product_id}).catch(function(error){
		// 			console.log("Validating click error: ",error);
		// 		});
		// 		console.log("Validating Prduct");
				
		// 	});
			
		// 	$("#fun-btn").click(function(){				
		// 		console.log(url1);				
		// 	});
			
		// }
		
	} );	
	
});


})(jQuery);