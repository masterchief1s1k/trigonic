storageRef.child('images/stars.jpg').getDownloadURL().then(function(url) {
  // `url` is the download URL for 'images/stars.jpg'

  format(row.data(), url);

  // This can be downloaded directly:
  var xhr = new XMLHttpRequest();
  xhr.responseType = 'blob';
  xhr.onload = function(event) {
    var blob = xhr.response;
  };
  xhr.open('GET', url);
  xhr.send();

  // Or inserted into an <img> element:
  var img = document.getElementById('myimg');
  img.src = url;
}).catch(function(error) {
  // Handle any errors
});


$("#confirm-btn").click(function(){

    var current_user_id = $(this).attr("user-id");
    // call function to validate (current_user_id);
})

function format ( d ,url) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.extn+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extra info:</td>'+
            '<td>And any further details here (images etc)... <img src="' +url + '"/> </td>'+
        '</tr>'+
        '<tr>'+
            '<td>Validate this account:</td>'+
            '<td> <button user-id="'+ d.id +'" id="confirm-btn"> Confirm </button> </td>'+
        '</tr>'+
        
    '</table>';
}
