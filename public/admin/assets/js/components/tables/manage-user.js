(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	

	// $(function () {
	// 	$('#sorting-table').DataTable({
	// 		"lengthMenu": [
	// 			[10, 15, 20, -1],
	// 			[10, 15, 20, "All"]
	// 		],
	// 		"order": [
	// 			[3, "desc"]
	// 		]
	// 	});
	// });

	$(function () {	

		var table = $('#export-table').DataTable({
			dom: 'Bfrtip',
			rowId: 'UserID',
			"scrollX": true,
			columns : [
				{ data : 'ServerID' },
				{ data : 'UserID' },
				{ data : 'Name' },
				{ data : 'Email' },
				{ data : 'Phone' },
				{ data : 'Type' }
			],

			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});
		// $('#export-table').on( 'click', 'tbody td:not(:first-child)', function (e) {
		$('#export-table').on( 'click', 'tbody td', function (e) {
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var data = row.data();
			$('#input-1').val(data.ServerID);
			$("input-1").prop('disabled', true);
			$('#input-2').val(data.Name);
			$('#input-3').val(data.Email);
			$('#input-4').val(data.Phone);


			$('#modal-centered').modal('show');

			var updatebutton = document.getElementById("submit_btn");
			var orderbutton = document.getElementById("order_btn");
			var deletebutton = document.getElementById("delete_btn");
			var updateUser = bluesicht_functions.httpsCallable('updateUserCallable');
			var deleteUser = bluesicht_functions.httpsCallable('deleteUserCallable');	

			orderbutton.addEventListener('click', function(){
				var UID = document.getElementById("input-1").value;
				window.location.href = "orders.html?userid=" + UID;
			})

			deletebutton.addEventListener('click', function(){

				Swal.fire({
					title: 'Are you sure?',
					text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				  }).then((result) => {
					if (result.value) {

						var UID = document.getElementById("input-1").value;
				var newName = document.getElementById("input-2").value;
				deleteUser({
					uid: UID,
					name:newName
				}).then(function(){
					$('#modal-centered').modal('hide');
				}).catch(function(error){

					$('#modal-centered').modal('hide');
					console.log("Updating user error: ",error);
				});		
					  Swal.fire(
						'Deleted!',
						'Your file has been deleted.',
						'success'
					  )
					}
				  })
				
				


			})
			
			updatebutton.addEventListener('click', function() {
				var UID = document.getElementById("input-1").value;
				var newName = document.getElementById("input-2").value;
				var newEmail = document.getElementById("input-3").value;
				var newPhone = document.getElementById("input-4").value;
								
				updateUser({
					uid:UID,
					name:newName,
					email:newEmail,
					phone:newPhone
				}).catch(function(error){
					console.log("Updating user error: ",error);
				});				
				console.log("Updating User");
				$('#modal-centered').modal('hide');
			});			
		} );	
		
		$.fn.dataTable.ext.errMode = 'none';
		bluesicht_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					bluesicht_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
								bluesicht_DB.collection('users').onSnapshot(function(snapshot) {									
									
									snapshot.docChanges().forEach(function(change){
										console.log(change.doc.data());
										var dataObject = {
											ServerID: change.doc.id,
											UserID: change.doc.data().customid,
											Name: change.doc.data().name,
											Email: change.doc.data().email,
											Phone: change.doc.data().phone,
											Type: capitalize(change.doc.data().user_type)

										};

										if (change.doc.data().userCountID == null)
										{

										

										if (change.type === "added") {
											console.log("New customer: ", change.doc.data());
											table.row.add(dataObject).draw();	
										}
										if (change.type === "modified") {
											console.log("Modified customer: ", change.doc.data());
											table.rows('[id="'+dataObject.UserID+'"]').remove();
											table.row.add(dataObject).draw();	
										}
										if (change.type === "removed") {
											console.log("Removed customer: ", change.doc.data());
											table.rows('[id="'+dataObject.UserID+'"]').remove().draw();
										}
										}

										
										// 	console.log(dataObject);
										// if (table.rows('[UserID="'+dataObject.UserID+'"]').any())
										// {
										// 	table.rows('[UserID="'+dataObject.UserID+'"]').remove();
										// 	table.row.add(dataObject).draw();	
										// 	console.log("Already exist!");
										// }
										// else
										// {
																					
										// }
										
									})
								});
							// } else {
								// console.log("not an admin");
							// }
						})
					.catch((error) => {
						console.log(error);
					});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

	const capitalize = (s) => {
		if (typeof s !== 'string') return ''
		return s.charAt(0).toUpperCase() + s.slice(1)
	  }
})(jQuery);