"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase = require("firebase");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const serviceAccount = require('../bluesichtshoecare-firebase-adminsdk-3rhhy-889164ca0e.json');
const cors = require('cors')({ origin: true });
const config = {
    credential: admin.credential.cert(serviceAccount),
    apiKey: "AIzaSyD7sWq9OyLlgbf-REijWZUwDt2q2pD-rM0",
    authDomain: "bluesichtshoecare.firebaseapp.com",
    databaseURL: "https://bluesichtshoecare.firebaseio.com",
    projectId: "bluesichtshoecare",
    storageBucket: "bluesichtshoecare.appspot.com",
    messagingSenderId: "497538613208",
    keyFilename: ('../bluesichtshoecare-firebase-adminsdk-3rhhy-889164ca0e.json')
};
admin.initializeApp(config);
firebase.initializeApp(config);
const db = admin.firestore();
const settings = { timestampsInSnapshots: true };
db.settings(settings);
function writeUser(user) {
    return __awaiter(this, void 0, void 0, function* () {
        var id;
        yield db.collection('users').doc('statistics').get().then(function (doc) {
            return __awaiter(this, void 0, void 0, function* () {
                if (doc.exists) {
                    id = doc.data().userCountID;
                }
            });
        });
        const userCountRef = db.collection('users').doc('statistics');
        db.runTransaction((t) => __awaiter(this, void 0, void 0, function* () {
            return yield t.get(userCountRef)
                .then(doc => {
                var newUserCountID = doc.data().userCountID + 1;
                t.update(userCountRef, { userCountID: newUserCountID });
            }).then(() => {
                console.log('Transaction success: ', id);
            }).catch(e => {
                console.log('Transaction error: ', e);
            });
        }));
        yield db.collection('users').add({
            customid: id,
            name: user.name || 'none',
            dob: user.dob || 'none',
            email: user.email || 'none',
            address: user.address || 'none',
            city: user.city || 'none',
            user_type: user.order_type || 'none',
            phone: user.phone || 'none',
            timestamp: user.timestamp
        }).catch(function (error) {
            console.error("Error adding info: ", error);
        })
            .then((data) => __awaiter(this, void 0, void 0, function* () {
            console.log("User created: " + JSON.stringify(user) + "with UID: " + data.id);
            // await db.collection('users').doc(data.id).update({uid:data.id})
            return { user_id: id };
        }));
    });
}
exports.writeUserCallable = functions.region('asia-east2').https.onCall((user) => __awaiter(this, void 0, void 0, function* () {
    console.log(`Writing ${user.name}`);
    var id;
    yield db.collection('users').doc('statistics').get().then(function (doc) {
        return __awaiter(this, void 0, void 0, function* () {
            if (doc.exists) {
                id = doc.data().userCountID;
            }
        });
    });
    const userCountRef = db.collection('users').doc('statistics');
    db.runTransaction((t) => __awaiter(this, void 0, void 0, function* () {
        t.get(userCountRef)
            .then(doc => {
            var newUserCountID = doc.data().userCountID + 1;
            t.update(userCountRef, { userCountID: newUserCountID });
        }).then(() => {
            console.log('Transaction success: ', id);
            return db.collection('users').add({
                customid: id,
                name: user.name || 'none',
                dob: user.dob || 'none',
                email: user.email || 'none',
                address: user.address || 'none',
                city: user.city || 'none',
                user_type: user.order_type || 'none',
                phone: user.phone || 'none',
                timestamp: user.timestamp
            }).catch(function (error) {
                console.error("Error adding info: ", error);
            })
                .then(data => {
                console.log("User created: " + JSON.stringify(user) + "with id: " + id);
                // await db.collection('users').doc(data.id).update({uid:data.id})
                return { user_id: id };
            });
        }).catch(e => {
            console.log('Transaction error: ', e);
        });
    }));
    // return writeUser(user).then(() =>{
    //   console.log(`Successfully added ${user.name}`);
    // });
}));
exports.deleteUserData = functions.region('asia-east2').auth.user().onDelete((user) => __awaiter(this, void 0, void 0, function* () {
    db.collection('users').doc(user.uid).delete().catch(function (error) {
        console.error("Error delete user: ", error);
    });
}));
// request.auth.uid == resource.data.uid
function grantAdmin(email) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield admin.auth().getUserByEmail(email); // 1
        if (user.customClaims && user.customClaims.admin === true) {
            console.log("Already an admin.");
            return;
        } // 2
        return admin.auth().setCustomUserClaims(user.uid, {
            admin: true
        }); // 3
    });
}
function writeService(order) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('shoes').add({
            // uid: order.uid || 'none',
            owner_id: order.id,
            owner_email: order.email || 'none',
            owner_phone: order.phone || 'none',
            shoe_type: order.shoe_type,
            clean: true || false,
            repaint: order.repaint || 'none',
            status: 'unfinished',
            delivery: order.delivery || 'none',
            total: order.total,
            timestamp: order.timestamp
        }).catch(function (error) {
            console.error("Error adding info: ", error);
        })
            .then((data) => __awaiter(this, void 0, void 0, function* () {
            console.log("Service created: " + JSON.stringify(order) + "with UID: " + data.id);
            // await db.collection('services').doc(data.id).update({uid:data.id})
        }));
    });
}
exports.writeServiceCallable = functions.region('asia-east2').https.onCall((order) => __awaiter(this, void 0, void 0, function* () {
    console.log(`Adding ${order.shoe_type} ${order.email} ${order.phone}`);
    return writeService(order).then(() => {
        console.log(`${order.shoe_type} ${order.email} ${order.phone} creating done.`);
    });
}));
exports.writeAdmin = functions.region('asia-east2').https.onCall((data, context) => {
    // if (context.auth.token.admin !== true) {   
    //   console.log("Request not authorized. User must be a moderator.");
    // };
    const email = data.email;
    return grantAdmin(email).then(() => {
        console.log(`${email} is now an admin.`);
    });
});
function validateUser(uid) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('users').doc(uid).update({
            pending: false
        }).catch(function (error) {
            console.log("Error validating: ", error);
        });
    });
}
;
exports.validateUserCallable = functions.region('asia-east2').https.onCall(data => {
    console.log(`Validating user ${data.uid}`);
    return validateUser(data.uid).then(() => {
        console.log(`${data.uid} validated.`);
    });
});
function updateUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('users').doc(data.uid).update({
            name: data.name,
            email: data.email,
            phone: data.phone
        }).catch(function (error) {
            console.log("Error updating user: ", error);
        });
    });
}
;
exports.updateUserCallable = functions.region('asia-east2').https.onCall(data => {
    console.log(`Updating user ${data.name}`);
    return updateUser(data).then(() => {
        console.log(`User ${data.uid} - ${data.name} updated.`);
    });
});
function deleteUser(data) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('users').doc(data.uid).delete().catch(function (error) {
            console.log("Error updating user: ", error);
        });
    });
}
;
exports.deleteUserCallable = functions.region('asia-east2').https.onCall(data => {
    console.log(`Deleting user ${data.name}`);
    return deleteUser(data).then(() => {
        console.log(`User ${data.uid} - ${data.name} deleted.`);
    });
});
function validateProduct(uid) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('Products').doc(uid).update({
            Pending: false
        }).catch(function (error) {
            console.log("Error validating: ", error);
        });
    });
}
;
exports.validateProductCallable = functions.region('asia-east2').https.onCall(data => {
    console.log(`Validating product ${data.Name}`);
    return validateProduct(data.uid).then(() => {
        console.log(`${data.uid} - ${data.Name} validated.`);
    });
});
function updateProduct(data) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('Products').doc(data.uid).update({
            Name: data.Name,
            Brand: data.Brand,
            Condition: data.Condition,
            Size: data.Size,
            Price: data.Price
        }).catch(function (error) {
            console.log("Error updating product: ", error);
        });
    });
}
;
exports.updateProductCallable = functions.region('asia-east2').https.onCall(data => {
    console.log(`Updating product ${data.uid} - ${data.Name}`);
    return updateProduct(data).then(() => {
        console.log(`Product ${data.uid} - ${data.Name} updated.`);
    });
});
function updateService(data) {
    return __awaiter(this, void 0, void 0, function* () {
        yield db.collection('Services').doc(data.uid).update({
            Name: data.Name,
            Status: data.Status,
        }).catch(function (error) {
            console.log("Error updating product: ", error);
        });
    });
}
;
exports.updateServiceCallable = functions.region('asia-east2').https.onCall(data => {
    console.log(`Updating service ${data.uid} - ${data.Name}`);
    return updateService(data).then(() => {
        console.log(`Service ${data.uid} - ${data.Name} updated.`);
    });
});
function getID(uid) {
    return __awaiter(this, void 0, void 0, function* () {
        let query;
        // const user = await admin.auth().getUser(uid); //get using uid
        const userRef = yield db.collection("users").doc(uid);
        yield userRef.get().then(doc => {
            if (!doc.exists) {
                console.log('No such document!');
            }
            else {
                console.log('Document data:', doc.data());
                query = doc.data();
            }
        })
            .catch(err => {
            console.log('Error getting document', err);
        });
        ;
        return query;
    });
}
exports.userProfile = functions.region('asia-east2').https.onRequest((req, res) => __awaiter(this, void 0, void 0, function* () {
    const data = yield getID(req.query.uid);
    console.log(data.name);
    res.status(200).send(`<!doctype html>
    <head>
      <title>FBID</title>      
    </head>
    <body>      
      Name: <input type="text" id="name" placeholder="${data.name}"><BR>
      UID is ${req.query.uid}
    </body>
  </html>`);
}));
//# sourceMappingURL=index.js.map