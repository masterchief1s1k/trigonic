(function($){
            
        window.onload = function() {

            // console.log("Tell me smt");
            // console.log('window - onload'); // 4th

        // Global Variable

        var form;

        var name2 = document.getElementById("name2");
        var email2 = document.getElementById("email2");
        var phone2 = document.getElementById("phone2");
        var dob2 = document.getElementById("dob2");
        var address2 = document.getElementById("address2");
        var country2 = document.getElementById("country2");
        var city2 = document.getElementById("city2");
        var shoesArray = [];
        var i;
        var cleanbox = false;
        var repainttype = 'none';

        // Form Part 1 - User Info
        var name = document.getElementById("name");
        var email = document.getElementById("email");
        var phone = document.getElementById("phone");
        var dob = document.getElementById("dob");
        var address = document.getElementById("address");
        // var country = document.getElementById("country");
        var city = document.getElementById("city");
        var regbox = document.getElementById("check-1");
        
        

        // Inject Event to Function passvalue
        name.onkeyup =  
        email.onkeyup =
        phone.onkeyup = 
        dob.onkeyup =
        name.onkeyup = 
        address.onkeyup = 
        city.onkeyup =  function() {passvalue()};

        // Date Picker pick Birthday


        $('#dob').daterangepicker({
            locale: {
                format: "DD/MM/YYYY"
            },
            singleDatePicker: true,
            // autoApply: true,
            autoUpdateInput: false,
            showDropdowns: true,
            minYear: 1970,
            maxYear: 2010},
            function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
            $('#dob').data('daterangepicker').setStartDate(start.format('DD/MM/YYYY'));
            // // console.log(start);
            // var date =  new firebase.firestore.Timestamp.fromDate(start._d);
            dob2.textContent = convertDate(start._d);

            console.log(dob2);
        });


        // Form Part 2 - Service Detail
        
        $(".repaint").toggle(); 

        $("#repaint_shoes").change(() => {
            $(".repaint").fadeToggle();

        })

        $("#delshoes").hide();


        // Form Part 3 - Confirmation and Shipping


        // Chỗ viết Function
        function passvalue(){
            //Get the input element
            
            //Get the value

            console.log("PASSING DATA TO PART 3");
            
            name2.textContent = name.value;
            email2.textContent = email.value;
            phone2.textContent = phone.value;
            dob2.textContent = dob.value;
            address2.textContent = address.value;
            // country2.textContent = country.value;
            city2.textContent = city.value;
            regbox2 = regbox; 
        }

        function convertDate(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
        }
        
        // function testUser(){
        //     console.log(name.value)
        //     if (regbox.checked == true){
        //         console.log(email.value);
        //     }
        // }

        function testShoes(){
            console.log()
        }

        //if regbox.checked == true
        function addUser(){            
            var addNewUser = firebase.functions().httpsCallable('writeUserCallable');		
            addNewUser({
                name:name.value,
                email:email.value,
                phone:phone.value,
                dob:dob.value,
                address:address.value,
                city:city.value
            }).catch(function(error){
                console.log("Adding user error: ",error);
            });
        }

        function addService(){
            i = 0;
            var addNewService = firebase.functions().httpsCallable('writeServiceCallable');
            for (i;i < shoesCounter; i++){
                addNewService({                
                    owner:'none',
                    type:loaigiay.value,                
                    service:service.value,                
                }).catch(function(error){
                    console.log("Adding service error: ",error);
                });
        }

        function addService(){
            shoesArray = [];
            i = 0;            
            var shoes = document.getElementsByClassName("selectgiay");
            var clean = document.getElementsByClassName("cleangiay");
            var repaint = document.getElementsByClassName("repaintgiay");
            var repaintde = document.getElementsByClassName("repaintde");
            var repaintthan = document.getElementsByClassName("repaintthan");
            var repaintfull = document.getElementsByClassName("repaintfull");            
            // console.log(clean[0].checked);
            // console.log(repaint[0].checked);
            for (i; i < shoesCounter; i++){
                cleanbox = false;
                repainttype = 'none';
                if (clean[i].checked == false && repaint[i].checked == false)
                {
                    console.log("Not chosen")
                    continue;
                }
                if (clean[i].checked == true)
                {
                    cleanbox = true;
                }
                if (repaint[i].checked == true)
                {
                    if (repaintde[i].checked == true){
                        repainttype = "de";
                    }
                    else if (repaintthan[i].checked == true){
                        repainttype = "than";
                    }                      
                    else if (repaintfull[i].checked == true){
                        repainttype = "full";
                    }
                    else {
                        console.log("Not selected")
                        continue;
                    }
                }
                shoesArray.push({
                    "type":shoes[i].selectedOptions[0].text,
                    "clean":cleanbox,
                    "repaint":repainttype
                })
                console.log(shoesArray);
            }
        }

        $("#test").on('click',function(){
            addService();
        })

        $('#finishadd').on('click', function() {            
            // addService();
        });        
    }
    
    //define counter
    var shoesIncrement = 1;
    var shoesCounter = 1;
    
    $(function(){
    // $(document).ready(function(){
        $("#addshoes").on('click',function(){
            $("#delshoes").show();
            //define shoes' clone    
            var shoesclone = $('.tabgiay:first').clone(false);
            // shoesclone.copyCSS('#tabgiay');
            // var typeShoes = shoesclone.find("option").each(function() {
            //     $(this).prop({
            //         "id":$(this).attr('id')+shoesIncrement,
            //     }) 
            //  });            
            var cleanShoes = shoesclone.find("input.custom-control-input").each(function() {
               $(this).prop({
                "name":$(this).attr('name')+shoesIncrement,              
                "id":$(this).attr('id')+shoesIncrement,
                "checked":false
               }) 
            });
            var labelShoes = shoesclone.find("label.custom-control-descfeedback").each(function() {
                $(this).prop({
                 "for":$(this).attr('for')+shoesIncrement
                }) 
             });
            var divShoes = shoesclone.find(".repaint").each(function() {
                $(this).prop({
                 "class":$(this).attr('class')+shoesIncrement
                }) 
             });       
            
            let newclass = ".repaint" + shoesIncrement;
            
            
            shoesclone.appendTo(".tabgiayall")
            .on('click', 'button.remove', remove);            

            $(newclass).hide(); 
            
            $("#repaint_shoes" + shoesIncrement).change(() => {
                console.log("changing");
                
                $(newclass).fadeToggle();

            })
            
            shoesIncrement++;
            shoesCounter++;
            document.getElementById("shoesNum").innerHTML = `Số đôi giày của bạn: ${shoesCounter}`;
            return false;
        });

        function remove() {        
            $(this).parents(".tabgiay").remove();
            shoesCounter--;
            document.getElementById("shoesNum").innerHTML = `Số đôi giày của bạn: ${shoesCounter}`;
            if ($(".tabgiay").length == 1) {
                $('#delshoes').hide();
            } else {
                $('#delshoes').show();
            }
        }        
        
        // service.addEventListener('change', changeVal);

        function changeVal(){
            $('.tabgiayall').find('.tabgiay').each(function(){                
                var shoes = document.getElementsByClassName("selectgiay");
                var test = shoes.options[shoes.selectedIndex].text;                
                console.log(shoes);
            });
        }

        
        
    });
// })

})(jQuery);


        