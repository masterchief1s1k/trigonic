(function($){        
        window.onload = function() {

            // console.log("Tell me smt");
            // console.log('window - onload'); // 4th

        // Global Variable

        var form;

        var name2 = document.getElementById("name2");
        var email2 = document.getElementById("email2");
        var phone2 = document.getElementById("phone2");
        var dob2 = document.getElementById("dob2");
        var address2 = document.getElementById("address2");
        var country2 = document.getElementById("country2");
        var city2 = document.getElementById("city2");

        // Form Part 1 - User Info
        var name = document.getElementById("name");
        var email = document.getElementById("email");
        var phone = document.getElementById("phone");
        var dob = document.getElementById("dob");
        var address = document.getElementById("address");
        // var country = document.getElementById("country");
        var city = document.getElementById("city");
        
        

        // Inject Event to Function passvalue
        name.onkeyup =  
        email.onkeyup =
        phone.onkeyup = 
        dob.onkeyup =
        name.onkeyup = 
        address.onkeyup = 
        city.onkeyup =  function() {passvalue()};

        // Date Picker pick Birthday


        $('#dob').daterangepicker({
            locale: {
                format: "DD/MM/YYYY"
            },
            singleDatePicker: true,
            // autoApply: true,
            autoUpdateInput: false,
            showDropdowns: true,
            minYear: 1975,
            maxYear: 2010},
            function(start, end, label) {
            // console.log("A new date selection was made: " + start.format('DD/MM/YYYY') + ' to ' + end.format('DD/MM/YYYY'));
            $('#dob').data('daterangepicker').setStartDate(start.format('DD/MM/YYYY'));
            // // console.log(start);
            // var date =  new firebase.firestore.Timestamp.fromDate(start._d);
            dob2.textContent = convertDate(start._d);

            console.log(dob2);
        });


        // Form Part 2 - Service Detail
        
        $(".repaint").toggle(); 

        $("#repaint_shoes").change(() => {
            $(".repaint").fadeToggle();

        })


        // Form Part 3 - Confirmation and Shipping


        // Chỗ viết Function
        function passvalue(){
            //Get the input element
            
            //Get the value

            console.log("PASSING DATA TO PART 3");
            
            name2.textContent = name.value;
            email2.textContent = email.value;
            phone2.textContent = phone.value;
            dob2.textContent = dob.value;
            address2.textContent = address.value;
            // country2.textContent = country.value;
            city2.textContent = city.value; 
        }

        function convertDate(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
            var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
        }
        
        function addUser(){
            var name = document.getElementById('name').value;
            var email = document.getElementById('email').value;
            var email = document.getElementById('phone').value;
            var addNewUser = firebase.functions().httpsCallable('writeUserData');		
            addNewUser({
                name:,

            }).catch(function(error){
                console.log("Adding user error: ",error);
            });
        }
    }
    
    //define counter
    var shoesCount = 1;    
    
    $(function(){
        $("#addshoes").on('click',function(){
            
            //define shoes' clone    
            var shoesclone = $('.tabgiay:first').clone(false);
            // shoesclone.copyCSS('#tabgiay');            
            var inputShoes = shoesclone.find("input.custom-control-input").each(function() {
               $(this).prop({
                "name":$(this).attr('name')+shoesCount,              
                "id":$(this).attr('id')+shoesCount,
                "checked":false
               }) 
            });
            var labelShoes = shoesclone.find("label.custom-control-descfeedback").each(function() {
                $(this).prop({
                 "for":$(this).attr('for')+shoesCount
                }) 
             });
            var divShoes = shoesclone.find(".repaint").each(function() {
                $(this).prop({
                 "class":$(this).attr('class')+shoesCount
                }) 
             });

            var delShoes = shoesclone.find("")
        
            //inject new section
            let newclass = ".repaint" + shoesCount;
            console.log(typeof newclass);
            console.log(newclass);
            
            
           
            shoesclone.appendTo(".tabgiayall");
            $(newclass).hide(); 

            $("#repaint_shoes" + shoesCount).change(() => {
                console.log("changing");
                
                $(newclass).fadeToggle();

            })
            shoesCount++;
            return false;
        });

        //xóa giày
        $('#delshoes').on('click', function() {            
            //fade out
            $(this).parent().fadeOut(300, function(){
                //remove parent element
                $(this).parent().parent().empty();                
                return false;
            });
            shoesCount--;
            return false;
        });
    });
    

})(jQuery);


        