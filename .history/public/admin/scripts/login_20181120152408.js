'use strict';
function SCDC(){
    this.checkSetup();
    
    //shortcut to DOM elements
    this.googleSignInButton = document.getElementById('googleSignInButton');
    this.signInButton = document.getElementById('signIn');
    //handle event
    this.googleSignInButton.addEventListener('click',this.signIn.bind(this));
    this.signInButton.addEventListener('click',this.signInWithUsername.bind(this));
    this.initFirebase();
}

//Sign in with Username and password
SCDC.prototype.signInWithUsername = function(){
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    // alert(password);
    this.auth.signInWithEmailAndPassword(username,password).catch(function(error){
        var errorCode = error.code;
        var errorMessage = error.message;
        window.alert("Error: "+errorMessage);
    });
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    this.database = firebase.database();   
    this.firestore = firebase.firestore();
    // Initiates Firebase auth and listen to auth state changes.
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Signs-in Friendly Chat.
SCDC.prototype.signIn = function() {
    // Sign in Firebase using popup auth and Google as the identity provider.
    var provider = new firebase.auth.GoogleAuthProvider();
    this.auth.signInWithPopup(provider);
  };

// Triggers when the auth state change for instance when the user signs-in or signs-out.
SCDC.prototype.onAuthStateChanged = function(user){
    if (user) { // User is signed in!
        var userID = user.uid;
        this.firestore.collection("users").doc(userID).set({
            admin: true
        })        
        window.location = 'index.html';
    } else { // User is signed out!
        // window.location = 'login.html';
    }
};

// Checks that the Firebase SDK has been correctly setup and configured.
SCDC.prototype.checkSetup = function() {
    if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
         window.alert('You have not configured and imported the Firebase SDK. ' +
          'Make sure you go through the codelab setup instructions and make ' +
          'sure you are running the codelab using `firebase serve`');
    }
};

window.onload = function() {
    window.SCDC = new SCDC();
};