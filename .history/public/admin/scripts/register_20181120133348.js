'use strict';
function SCDC(){
    this.checkSetup();
    
    //shortcut to DOM elements
    this.googleSignInButton = document.getElementById('googleSignInButton');
    this.signInButton = document.getElementById('signIn');
    //handle event
    this.googleSignInButton.addEventListener('click',this.signIn.bind(this));
    this.signInButton.addEventListener('click',this.signInWithUsername.bind(this));
    this.initFirebase();
}

//Sign in with Username and password
SCDC.prototype.signUp = function(){
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    // alert(password);
    this.auth.createUserWithEmailAndPassword(username,password).catch(function(error,user){
        var errorCode = error.code;
        var errorMessage = error.message;
        window.alert("Error: "+errorMessage);
    });
    window.alert("Sign up successfully!");
    window.location = 'login.html';
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    this.database = firebase.database();
    // this.storage = firebase.storage();

    // Initiates Firebase auth and listen to auth state changes.
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
SCDC.prototype.onAuthStateChanged = function(user){
    if (user) { // User is signed in!
        window.location = 'index.html';
    } else { // User is signed out!
        // window.location = 'login.html';
    }
};

window.onload = function() {
    window.SCDC = new SCDC();
};