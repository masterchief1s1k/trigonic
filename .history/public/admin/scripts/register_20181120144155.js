'use strict';
function SCDC(){
    this.checkSetup();    
    this.initFirebase();
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
}

//Sign in with Username and password
SCDC.prototype.signUp = function(){
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;    
    // alert(password);
    this.auth.createUserWithEmailAndPassword(username,password).catch(function(error){
        var errorCode = error.code;
        var errorMessage = error.message;        
        window.alert("Error: "+error.code+" "+errorMessage);
    });
    this.auth.signInWithEmailAndPassword(username,password).catch(function(error){
        var errorCode = error.code;
        var errorMessage = error.message;
        window.alert("Error: "+errorMessage);
    });
    window.alert("Sign up successfully!");
    window.location = 'index.html';
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    this.database = firebase.database();
    // this.storage = firebase.storage(); 
    var writeAdmin = firebase.functions().httpsCallable('writeAdmin');
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
SCDC.prototype.onAuthStateChanged = function(user){    
    if (user) {
    var userId = user.uid;
    database.collection('users').doc(userId).set({
        userType: "admin",
        })
    }   
};

window.onload = function() {
    window.SCDC = new SCDC();
};