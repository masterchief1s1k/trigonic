'use strict';
function SCDC(){           
    this.initFirebase();
    // this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
    //shortcut to DOM elements    
    this.btnSignUp = document.getElementById('btnSignUp');
    //handle event
    this.btnSignUp.addEventListener('click',e=>{
        e.preventDefault();
        this.signIn.bind(this);
    });    
}

//Sign in with Username and password
SCDC.prototype.signUp = function(){    
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;    
    // alert(password);
    this.auth.createUserWithEmailAndPassword(username,password).catch(function(error){        
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log("Info: ",error);        
        // window.alert("Error: "+errorCode+" "+errorMessage);
    });    
    window.alert("Sign up successfully!");    
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));   
    
};
SCDC.prototype.onAuthStateChanged = function(user){
    if (user) { // User is signed in!
        var userID = user.uid;
        this.firestore.collection("users").doc(userID).set({
            admin: 'true'
        })        
        window.location = 'index.html';
    } else { // User is signed out!
        // window.location = 'login.html';
    }
};

window.onload = function() {
    window.SCDC = new SCDC();
};