
'use strict';

function SCDC(){
    this.checkSetup();
    // Shortcuts to DOM Elements.
    this.userName = document.getElementById('user-name');
    this.userPic = document.getElementById('user-image');
    this.userName2 = document.getElementById('user-name-2');
    this.userPic2 = document.getElementById('user-image-2');
    this.userName3 = document.getElementById('user-name-3');
    this.userPic3 = document.getElementById('user-image-3');
    // this.creationDate = document.getElementById('creation_date');
    this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    // this.database = firebase.database();
    // this.storage = firebase.storage();
    this.firestore = firebase.firestore();

    // Initiates Firebase auth and listen to auth state changes.
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

// Signs-in Friendly Chat.
SCDC.prototype.signIn = function() {
    // Sign in Firebase using popup auth and Google as the identity provider.
    var provider = new firebase.auth.GoogleAuthProvider();
    this.auth.signInWithRedirect(provider);
};

// Returns true if user is signed-in. Otherwise false and displays a message.
SCDC.prototype.checkSignedInWithMessage = function() {
    // Return true if the user is signed in Firebase
    if (this.auth.currentUser) {
      return true;
    }
    return false;
};
  

// Signs-out of Friendly Chat.
SCDC.prototype.signOut = function() {
    // Sign out of Firebase.
    this.auth.signOut();
};

// Triggers when the auth state change for instance when the user signs-in or signs-out.
SCDC.prototype.onAuthStateChanged = function(user){
    if (user) { // User is signed in!

        var profilePicUrl = user.photoURL;
        var userID = user.uid;
        var userName = user.displayName;
        // var creationDate = user.metadata.timeCreated;

        this.userName.textContent = userName;
        this.userPic.src = profilePicUrl || 'dist/img/profile_placeholder.png';
        this.userName2.textContent = userName;
        this.userPic2.src = profilePicUrl || 'dist/img/profile_placeholder.png';
        this.userName3.textContent = userName;
        this.userPic3.src = profilePicUrl || 'dist/img/profile_placeholder.png';
        var writeAdmin = firebase.functions().httpsCallable('writeAdmin');
        writeAdmin(user);
        // Write database
        this.firestore.collection("users").doc(userID).set({
            name: userName,
            gender: "random"
        })

    } else { // User is signed out!
        window.alert("Please sign in!");
        window.location = 'login.html';
    }
};

// Checks that the Firebase SDK has been correctly setup and configured.
SCDC.prototype.checkSetup = function() {
    if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
         window.alert('You have not configured and imported the Firebase SDK. ' +
          'Make sure you go through the codelab setup instructions and make ' +
          'sure you are running the codelab using `firebase serve`');
    }
};

window.onload = function() {
    window.SCDC = new SCDC();
    var rootRef = firestore.ref().child("users/");
    rootRef.on("child_added", snap => {
        var dataSet = [
            snap.child("name").val(),
            snap.child("email").val(),
            snap.child("userId").val(),
            snap.child("phone").val()
        ];
    })
    var table = $('#user').DataTable();
    table.rows.add([dataSet]).draw();
    // $('#user').DataTable( {
    //     data: dataSet,
    //     columns: [
    //         { title: "Name" },
    //         { title: "Email" },
    //         { title: "UID" },
    //         { title: "Phone" },            
    //     ]
    // } );
};

// $(document).ready(function() {
    
// } );