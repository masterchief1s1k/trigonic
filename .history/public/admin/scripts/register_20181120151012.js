'use strict';
function SCDC(){       
    this.initFirebase();
    // this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
}

//Sign in with Username and password
SCDC.prototype.signUp = function(){
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;    
    // alert(password);
    this.auth.createUserWithEmailAndPassword(username,password).catch(function(error){
        var errorCode = error.code;
        var errorMessage = error.message;        
        window.alert("Error: "+errorCode+" "+errorMessage);
    });    
    window.alert("Sign up successfully!");
    window.location = 'login.html';
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    this.database = firebase.database();   
    
};

window.onload = function() {
    window.SCDC = new SCDC();
};