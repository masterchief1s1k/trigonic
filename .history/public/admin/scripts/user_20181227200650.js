// Triggers when the auth state change for instance when the user signs-in or signs-out.
SCDC.prototype.onAuthStateChanged = function(user){
    if (user) { // User is signed in!

        var profilePicUrl = user.photoURL;
        var userID = user.uid;
        var userName = user.displayName;
        // var creationDate = user.metadata.timeCreated;

        this.userName.textContent = userName;
        this.userPic.src = profilePicUrl || 'dist/img/profile_placeholder.png';
        this.userName2.textContent = userName;
        this.userPic2.src = profilePicUrl || 'dist/img/profile_placeholder.png';
        this.userName3.textContent = userName;
        this.userPic3.src = profilePicUrl || 'dist/img/profile_placeholder.png';
        var writeAdmin = firebase.functions().httpsCallable('writeAdmin');
        writeAdmin(user);
        // Write database
        this.firestore.collection("users").doc(userID).set({
            name: userName,
            gender: "random"
        })

    } else { // User is signed out!
        window.alert("Please sign in!");
        window.location = 'login.html';
    }
};


window.onload = function() {
    window.SCDC = new SCDC();    
      
};
// request.auth.uid == resource.data.uid
$(document).ready(function() {
    var table = $('#user').DataTable({
        "columnDefs":[{
            "targets":-1,
            "data":null,            
        }]
    });
    
    firebase.firestore().collection('users').onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change){
            console.log(change.doc.data());
            var dataSet = [
                change.doc.data().name,
                change.doc.data().email,
                change.doc.data().userId,
                change.doc.data().phone];
            table.rows.add([dataSet]).draw();
        })
    });
    $('#user tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        alert( data[0] +"'s email is: "+ data[ 1 ] );
    } );    
    // $('#user').DataTable( {        
    //     columns: [
    //         { title: "Name" },
    //         { title: "Email" },
    //         { title: "UID" },
    //         { title: "Phone" },            
    //     ]
    // } );   
} );