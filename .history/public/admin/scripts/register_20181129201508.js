'use strict';
function SCDC(){           
    this.initFirebase();          
}

//Sign in with Username and password
SCDC.prototype.signUp = function(){    
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;    
    // alert(password);
    this.auth.createUserWithEmailAndPassword(username,password).catch(function(error){        
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log("Info: ",error);      
    });    
    window.alert("Sign up successfully!");
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log("UID: ",user.uid);
            console.log(user);
            var writeAdmin = firebase.functions().httpsCallable('writeAdmin');
            writeAdmin({uid: user.uid,email:user.email}).catch(function(error){
                console.log("Write error: ",error);
            });  
        } else {
          // No user is signed in.
        }
      });    
}

SCDC.prototype.grantAdmin = function(){    
    var existing = document.getElementById('existing').value;    
    var writeAdmin = firebase.functions().httpsCallable('writeAdmin');
    writeAdmin({email:existing}).catch(function(error){
        console.log("Write error: ",error);
    });  
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
SCDC.prototype.initFirebase = function(){
    //shortcut to firebase SDK features
    this.auth = firebase.auth();
    this.database = firebase.database();
    this.firestore = firebase.firestore();
    this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));   
    
};

function listAllUsers(nextPageToken) {
    // List batch of users, 1000 at a time.
    admin.auth().listUsers(1000, nextPageToken)
      .then(function(listUsersResult) {
        listUsersResult.users.forEach(function(userRecord) {
          console.log("user", userRecord.toJSON());
        });
        if (listUsersResult.pageToken) {
          // List next batch of users.
          listAllUsers(listUsersResult.pageToken)
        }
      })
      .catch(function(error) {
        console.log("Error listing users:", error);
      });
  }

SCDC.prototype.onAuthStateChanged = function(user){
    if (user) { // User is signed in!
        firebase.auth().currentUser.getIdTokenResult(true).then((idTokenResult) => {
            if (idTokenResult.claims.admin) { // 3
                return;
            } else {
                window.alert ('Bye bye');
                window.location = 'login.html';
                }
            })
            .catch((error) => {
                    console.log("Token error: ",error);
                });      
        // window.location = 'index.html';
    } else { // User is signed out!
        window.location = 'login.html';
    }

};

window.onload = function() {
    if (!user)
    {
        window.location = 'login.html';
    }
    window.SCDC = new SCDC();
};