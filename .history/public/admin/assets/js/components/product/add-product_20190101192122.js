window.onload = function() {
    document.getElementById("submit").onclick = function storeData() {
        alert("hello");
        f1();
        //validation code to see State field is mandatory.  
    }
}

(function ($) {

	var db = firebase.firestore();
	var brandInput = null;
	var ref = db.collection("Products").doc();
	var ref_id = ref.id;
	var total;
	var id_img;
	
	$("#confirm-btn").click(function(){
		
	})

	//Modified the price of the product
	function MoneyFunction(){
		var x = document.getElementById("userprice");
		var y = document.getElementById("tax-price");
		var z = document.getElementById("price_field");

		var sad = x.value;
		total = sad * 110/100;
		y.textContent = sad *10/100;
		z.textContent = sad * 110/100;
	}
		//Adding Data Store Data function 
		console.log(brandInput);
		function storeData(){
			brandInput = document.getElementById("brand_field").value;
			var brandText = brandInput;
			var inputText = document.getElementById("text_field").value;
			var typeText = document.getElementById("type_field").value;
			var subtypeText = document.getElementById("subtype_field").value;
			var condText = document.getElementById("cond_field").value;
			var sizeText = document.getElementById("size_field").value;
			var typeDocmentRef = null;
			// Add a new document in collection "Products"
			ref.set({
				Name: inputText,
				Brand: brandText,
				Condition: condText,
				Size: sizeText,
				Price: total,
				Type: [typeText,subtypeText]
			})
			db.collection("Type").doc(typeText).collection("ref").doc(ref_id).set({
					typeDocmentRef : ref        
			})
			.then(function() {
			console.log(ref.id);
			})
			.catch(function(error) {
			console.error("Error writing document: ", error);
			});
		}
		//Listing Data in Database (Realtime)
		const list_div = document.getElementById("list_div");
		db.collection("Products").onSnapshot(function(querySnapshot) {
				querySnapshot.docChanges().forEach(function(change) {
					if (change.type === "added") {
						list_div.innerHTML += "<div class = 'list-item'><h3>Brand: " + change.doc.data().Brand + "</h3><h3>Type: "+ change.doc.data().Type + "</h3><h4>Name: " + change.doc.data().Name + "</h4><p>Condition: " + change.doc.data().Condition + "</p><p>Price: " + change.doc.data().Price + "</p><p>Size: " + change.doc.data().Size + "</p></div>"
					}
				})
		});
		//Handle multiple images upload
		//Listen for file selection
		var SelectedFile = document.getElementById("file");
		var submit = document.getElementById("submit-btn");
		submit.addEventListener('click', function(e){ 
		//Get files
			for (var i = 0; i < SelectedFile.files.length; i++) {
				var imageFile = SelectedFile.files[i];
				if (3<= SelectedFile.files.length && SelectedFile.files.length <= 9){
					UploadFile(imageFile);
					console.log("Upload files proceeded");
					}
				else {
						console.log("Error");
				}
			}
		});
		//Upload photo function
		function UploadFile(imageFile){
		var filename = imageFile.name;
		return new Promise(function (resolve, reject){
			var storageRef = firebase.storage().ref('/DatabaseImages/' +'/'+ ref_id + '/' + filename);
			//Upload file
			var task = storageRef.put(imageFile);
			//Upload progess bar
			task.on('state_changed',function progess (snapshot){
					var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
					console.log('Upload is ' + progress + '% done');
			},
			function (error){

			},
			function(){
					task.snapshot.ref.getDownloadURL().then(function(downloadURL){
							console.log(downloadURL);
					});
				}
			);
		});
		}
		// Preview photo before uploading function (Internet code)
		
		
		$(function () {
		$(":file").change(function () {
				$('#product-img').children().remove();
				id_img = 0;
				if (this.files) 
				{
					for(var i = 0, len = this.files.length; i<len ; i++) 
					{       
						new ImageCompressor(this.files[i], {
						quality: .9,
						maxWidth: 2800,
						maxHeight: 3200,
						minHeight: 100,
						convertSize: 0,
						success(file) {
								console.log(file);
								var reader = new FileReader();
								reader.onload = imageIsLoaded;
								reader.readAsDataURL(file);									
						},
						error(e) {
							console.log(e.message);
						},
						});
							
					}
				}
				});
		});

		function imageIsLoaded(e) {
			console.log(e.target);
			let container =  $('#product-img');
			let img = $('<img />');
			img.attr("id",id_img);
			img.attr('src', e.target.result);
			let btn_left = $('<input type="button" id="left"  value="left" />');
			let btn_right = $('<input type="button" id="right"  value="right" />');

			container.append(img);
			container.append(btn_left);
			container.append(btn_right);
			btn_left.on('click', function () {
				// $('#'+id_img+'').rotate(0);
				let current_angle = img.getRotateAngle()[0] || 0;
				img.rotate(current_angle - 90);
			});
			btn_right.on('click', function () {
				// $('#'+id_img+'').rotate(0);
				let current_angle = img.getRotateAngle()[0] || 0;
				img.rotate(current_angle + 90);
			});
			id_img++;
		}


})(jQuery);