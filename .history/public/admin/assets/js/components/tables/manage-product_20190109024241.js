(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	

	// $(function () {
	// 	$('#sorting-table').DataTable({
	// 		"lengthMenu": [
	// 			[10, 15, 20, -1],
	// 			[10, 15, 20, "All"]
	// 		],
	// 		"order": [
	// 			[3, "desc"]
	// 		]
	// 	});
	// });

	$(function () {	
		var brandList = [];
		trigonic_DB.collection('Brands').onSnapshot(async function(snapshot) {	
								
			snapshot.docChanges().forEach(function(change){
				console.log(change.doc.data());
				// Chi them khi change la added thoi, phong truong hop bi overlap
				if (change.type === "added") {
				let brandname = change.doc.data().brand_name;
				brandList.push(brandname);

				$('#input-3').append($("<option />").val(brandname).text(brandname));
				}
				
			})
			await console.log(brandList);
		})			

		var table = $('#export-table').DataTable({
			dom: 'Bfrtip',
			rowId: 'ProductID',
			columns : [				
				{ data : 'ProductID' },
				{ data : 'Name' },
				{ data : 'Brand' },
				{ data : 'Condition' },
				{ data : 'Size' },
				{ data : 'Price' }
			],

			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});

		// $('#export-table').on( 'click', 'tbody td:not(:first-child)', function (e) {
		$('#export-table').on( 'click', 'tbody td', function (e) {
			var tr = $(this).closest('tr');
			var row = table.row(tr);			
			let data = row.data();
			$('#input-1').val(data.ProductID);
			$('#input-2').val(data.Name);
			// Cái data.Brand này phải có trong list Brand nó mới auto-select nhé, nếu là brand lạ thì em add vô database trước mới được
			$('#input-3').val(data.Brand);
			$('#input-4').val(data.Condition);
			$('#input-5').val(data.Size);
			$('#input-6').val(data.Price);
			
			$('#modal-centered').modal('show');

			// $('modal-centered').modal('hide'); Cai nay de tat modal when function complete			

			var updatebutton = document.getElementById("submit_btn");
			var updateProduct = firebase.functions().httpsCallable('updateProductCallable');	
			
			updatebutton.addEventListener('click', function() {
				var newName = document.getElementById("input-2").value;
				var newBrand = document.getElementById("input-3").value;
				var newCondition = document.getElementById("input-4").value;
				var newSize = document.getElementById("input-5").value;
				var newPrice = document.getElementById("input-6").value;
								
				updateProduct({
					uid:data.ProductID,
					Name:newName,
					Brand:newBrand,
					Condition:newCondition,
					Size:newSize,
					Price:newPrice
				}).catch(function(error){
					console.log("Updating Product error: ",error);
				});
				console.log("Updating Product");
			});
			
			
			// document.getElementsByClassName('small button').click(function(){
			// 	console.log(row.data().ProductID);
			// })			
			// editor2.Draw(this);
			console.log(row.data());
		} );
		
		
		$.fn.dataTable.ext.errMode = 'none';
		trigonic_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					trigonic_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
						trigonic_DB.collection('Products').onSnapshot(function(snapshot) {							
							
							snapshot.docChanges().forEach(function(change){
								console.log(change.doc.data());

								var dataObject = {
									ProductID: change.doc.id,
									Name: change.doc.data().Name,
									Brand: change.doc.data().Brand,
									Condition: change.doc.data().Condition,
									Size: change.doc.data().Size,
									Price: change.doc.data().Price
								};
									console.log(dataObject);
								if (table.rows('[id="'+dataObject.ProductID+'"]').any())
								{
									table.rows('[id="'+dataObject.ProductID+'"]').remove();
									table.row.add(dataObject).draw();	
									console.log("Already exist!");
								}
								else
								{
									table.row.add(dataObject).draw();											
								}
								
							})
						});
					// } else {
						// console.log("not an admin");
					// }
				})
			.catch((error) => {
				console.log(error);
			});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

})(jQuery);