(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	

	// $(function () {
	// 	$('#sorting-table').DataTable({
	// 		"lengthMenu": [
	// 			[10, 15, 20, -1],
	// 			[10, 15, 20, "All"]
	// 		],
	// 		"order": [
	// 			[3, "desc"]
	// 		]
	// 	});
	// });

	$(function () {

		
		
		var userList = [];

		var table = $('#export-table').DataTable({
			dom: 'Bfrtip',
			rowId: 'UserID',
			columns : [
				{ data : 'UserID' },
				{ data : 'Name' },
				{ data : 'Email' },
				{ data : 'Phone' },
				{ data : 'Validation' },
				{ data : 'Facebook' }
			],

			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});

		$('#export-table').Tabledit({
			editButton: true,
			deleteButton: false,
			columns: {
				identifier: [0, 'UserID'],
				editable: [[1, 'Name'], [2, 'Email'], [3, 'Phone'], [4, 'Validation']]
			},
			buttons: {
				edit: {
					class: 'btn btn-sm btn-primary td-actions',
					html: '<a href="#"><i class="la la-edit p-1 mr-0 text-white"></i></a>',
					action: 'edit'
				},
				delete: {
					class: 'btn btn-sm btn-danger td-actions',
					html: '<a href="#"><i class="la la-close p-1 mr-0 text-white"></i></a>',
					action: 'delete'
				},
				save: {
					class: 'btn btn-success',
					html: 'Save'
				},
				restore: {
					class: 'btn btn-sm btn-warning',
					html: 'Restore',
					action: 'Restore'
				},
				confirm: {
					class: 'btn btn-primary',
					html: 'Confirm'
				}
			}
		});

		$.fn.dataTable.ext.errMode = 'none';
		trigonic_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					trigonic_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
								trigonic_DB.collection('users').onSnapshot(function(snapshot) {
									userList = [];
									
									snapshot.docChanges().forEach(function(change){
										console.log(change.doc.data());

										var dataObject = {
											UserID: change.doc.id,
											Name: change.doc.data().name,
											Email: change.doc.data().email,
											Phone: change.doc.data().phone,
											Validation: change.doc.data().pending,
											Facebook: change.doc.data().fb

										};
											console.log(dataObject);
										if (table.rows('[id="'+dataObject.UserID+'"]').any())
										{
											table.rows('[id="'+dataObject.UserID+'"]').remove();
											table.row.add(dataObject).draw();	
											console.log("Already exist!");
										}
										else
										{
											table.row.add(dataObject).draw();
											
										}
										
									})
								});
							// } else {
								// console.log("not an admin");
							// }
						})
					.catch((error) => {
						console.log(error);
					});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

})(jQuery);