(function ($) {

	'use strict';

$(function () {
	var statusList = ["Processing","Removed","Finished","Delivering","Delivered"]
	statusList.forEach(element => {
		$('#input-3').append($("<option />").val(element).text(element));
	});
	function format ( d ) {

		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			'<tr>'+
				'<td>Product name:</td>'+
				'<td>'+d.Name+'</td>'+
			'</tr>'+			
			'<tr>'+
				'<td>Status:</td>'+
				'<td>'+d.Status+'</td>'+
			'</tr>'+
			// '<tr>'+
			//     '<td>Image:</td>'+
			//     '<td> <img src="' +url + '"/> </td>'+
			// '</tr>'+			
			'<tr>'+
			'<tr>'+
				'<td>Notes:</td>'+
				'<td>'+d.Notes+'</td>'+
			'</tr>'+
			// '<tr>'+
			// 	'<td>Test button:</td>'+
			// 	'<td> <button id="fun-btn"> Test </button> </td>'+
			// '</tr>'+
			
		'</table>';
	}
	
	var table = $('#validate-table').DataTable({			
		rowId: 'ServiceID',
		columns : [
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
			{ data : 'ServiceID' },
			{ data : 'Name' },			
			{ data : 'Status' },			
			{ data : 'Notes' },			
		],

		buttons: {
			buttons: [{
				extend: 'copy',
				text: 'Copy',
				title: $('h1').text(),
				exportOptions: {
					columns: ':not(.no-print)'
				},
				footer: true
			},{
				extend: 'excel',
				text: 'Excel',
				title: $('h1').text(),
				exportOptions: {
					columns: ':not(.no-print)'
				},
				footer: true
			},{
				extend: 'csv',
				text: 'Csv',
				title: $('h1').text(),
				exportOptions: {
					columns: ':not(.no-print)'
				},
				footer: true
			},{
				extend: 'pdf',
				text: 'Pdf',
				title: $('h1').text(),
				exportOptions: {
					columns: ':not(.no-print)'
				},
				footer: true
			},{
				extend: 'print',
				text: 'Print',
				title: $('h1').text(),
				exportOptions: {
					columns: ':not(.no-print)'
				},
				footer: true,
				autoPrint: true
			}],
			dom: {
				container: {
					className: 'dt-buttons'
				},
				button: {
					className: 'btn btn-primary'
				}
			}
		}
	});

	$('#export-table').on( 'click', 'tbody td', function (e) {
		var tr = $(this).closest('tr');
		var row = table.row(tr);			
		let data = row.data();
		$('#input-1').val(data.ServiceID);
		$('#input-2').val(data.Name);		
		$('#input-3').val(data.Status);
		$('#input-4').val(data.Notes);		
		
		$('#modal-centered').modal('show');					

		var updatebutton = document.getElementById("submit_btn");
		var updateService = firebase.functions().httpsCallable('updateServiceCallable');	
		
		updatebutton.addEventListener('click', function() {
			var SID = document.getElementById("input-1").value;
			var newName = document.getElementById("input-2").value;
			var newStatus = document.getElementById("input-3").value;
			var newNotes = document.getElementById("input-4").value;
							
			updateService({
				uid:SID,
				Name:newName,				
				Status:newStatus,
				Notes:newNotes
			}).catch(function(error){
				console.log("Updating Product error: ",error);
			});
			console.log("Updating Product");
			$('#modal-centered').modal('hide');
		});			
	} );

	$.fn.dataTable.ext.errMode = 'none';
	trigonic_auth.onAuthStateChanged(function(user) {
		if (user) {
			console.log(user);
				trigonic_auth.currentUser.getIdTokenResult()
				.then((idTokenResult) => {
						// Confirm the user is an Admin.
						// if (!!idTokenResult.claims.admin) {
						trigonic_DB.collection('Services').onSnapshot(function(snapshot) {
															
							snapshot.docChanges().forEach(function(change){
								console.log(change.doc.data());

								var dataObject = {
									ServiceID: change.doc.id,
									Name: change.doc.data().Name,										
									Status: change.doc.data().Status,										
									Notes: change.doc.data().Notes,										

								};
									console.log(dataObject);
								if (table.rows('[id="'+dataObject.ServiceID+'"]').any())
								{
									table.rows('[id="'+dataObject.ServiceID+'"]').remove();
									table.row.add(dataObject).draw();	
									console.log("Already exist!");
								}
								else
								{
									if (change.doc.data().Status == true)
									table.row.add(dataObject).draw();
									
								}
								
							})
						});
						
					})
				.catch((error) => {
					console.log(error);
				});

		} else {
			console.log("not a user");
			
			// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
		}
	});

	$('#validate-table tbody').on('click', 'td.details-control',async function () {
		var tr = $(this).closest('tr');
		var row = table.row( tr );		
		if ( row.child.isShown() ) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');			
		}
		else {			
			var cid = row.data().ServiceID;			
			// var url1;
			// await storageRef.child(`DatabaseImages/${cid}/1.jpg`).getDownloadURL().then(function(url){
			// 	url1 = url;
			// });			
			await row.child( format( row.data() ) ).show();
			tr.addClass('shown');
			
		}
		
	} );	
	
});


})(jQuery);