(function ($) {

	'use strict';

$(function () {
	function format ( d,url ) {

		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			'<tr>'+
				'<td>Product name:</td>'+
				'<td>'+d.Name+'</td>'+
			'</tr>'+			
			'<tr>'+
				'<td>Condition:</td>'+
				'<td>'+d.Status+'</td>'+
			'</tr>'+
			// '<tr>'+
			//     '<td>Image:</td>'+
			//     '<td> <img src="' +url + '"/> </td>'+
			// '</tr>'+			
			'<tr>'+
			'<tr>'+
				'<td>Notes:</td>'+
				'<td>'+d.Notes+'</td>'+
			'</tr>'+
			// '<tr>'+
			// 	'<td>Test button:</td>'+
			// 	'<td> <button id="fun-btn"> Test </button> </td>'+
			// '</tr>'+
			
		'</table>';
	}
	
	var table = $('#validate-table').DataTable({			
		rowId: 'ServiceID',
		columns : [
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
			{ data : 'ServiceID' },
			{ data : 'Name' },			
			{ data : 'Status' },			
			{ data : 'Notes' },			
		],
	});

	$.fn.dataTable.ext.errMode = 'none';
	trigonic_auth.onAuthStateChanged(function(user) {
		if (user) {
			console.log(user);
				trigonic_auth.currentUser.getIdTokenResult()
				.then((idTokenResult) => {
						// Confirm the user is an Admin.
						// if (!!idTokenResult.claims.admin) {
							trigonic_DB.collection('Products').onSnapshot(function(snapshot) {
																
								snapshot.docChanges().forEach(function(change){
									console.log(change.doc.data());

									var dataObject = {
										ServiceID: change.doc.id,
										Name: change.doc.data().Name,										
										Status: change.doc.data().Status,
										Size: change.doc.data().Size,
										Price: change.doc.data().Price,
										Status: change.doc.data().Status,										

									};
										console.log(dataObject);
									if (table.rows('[id="'+dataObject.ServiceID+'"]').any())
									{
										table.rows('[id="'+dataObject.ServiceID+'"]').remove();
										table.row.add(dataObject).draw();	
										console.log("Already exist!");
									}
									else
									{
										if (change.doc.data().Status == true)
										table.row.add(dataObject).draw();
										
									}
									
								})
							});
						
					})
				.catch((error) => {
					console.log(error);
				});

		} else {
			console.log("not a user");
			
			// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
		}
	});

	$('#validate-table tbody').on('click', 'td.details-control',async function () {
		var tr = $(this).closest('tr');
		var row = table.row( tr );		
		if ( row.child.isShown() ) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');			
		}
		else {			
			var cid = row.data().ServiceID;			
			var url1,url2,url3;
			await storageRef.child(`DatabaseImages/${cid}/1.jpg`).getDownloadURL().then(function(url){
				url1 = url;
			});			
			await row.child( format(row.data(),url1) ).show();
			tr.addClass('shown');			
			// $("#tr td:confirm-btn").on( 'click', 'td.confirm-btn', function(){
			$("#confirm-btn").click(function(){
				var current_product_id = $(this).attr("ServiceID");
				// call function to validate (current_product_id);				
				var validateProduct = firebase.functions().httpsCallable('validateProductCallable');		
				validateProduct({uid:current_product_id}).catch(function(error){
					console.log("Validating click error: ",error);
				});
				console.log("Validating Prduct");
				
			});
			$("#fun-btn").click(function(){				
				console.log(url1);
				
			});
			
		}
		
	} );	
	
});


})(jQuery);