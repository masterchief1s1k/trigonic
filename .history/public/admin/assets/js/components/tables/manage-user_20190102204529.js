(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	

	// $(function () {
	// 	$('#sorting-table').DataTable({
	// 		"lengthMenu": [
	// 			[10, 15, 20, -1],
	// 			[10, 15, 20, "All"]
	// 		],
	// 		"order": [
	// 			[3, "desc"]
	// 		]
	// 	});
	// });

	$(function () {		
		
		var userList = [];		

		var table = $('#export-table').DataTable({
			dom: 'Bfrtip',
			rowId: 'UserID',
			columns : [
				{ data : 'UserID' },
				{ data : 'Name' },
				{ data : 'Email' },
				{ data : 'Phone' },
				{ data : 'Validation' },
				{ data : 'Facebook' }
			],

			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});		

		
		// var editor = $('#export-table').Tabledit({
		// 	url: window.location.href,
		// 	// eventType: 'click',
		// 	editButton: false,
		// 	deleteButton: false,
		// 	hideIdentifier: false,
		// 	columns: {
		// 		identifier: [0, 'UserID'],
		// 		editable: [[1, 'Name'], [2, 'Email'], [3, 'Phone'], [4, 'Validation']]
		// 	},		
		// 	onDraw: function() {
		// 		console.log('onDraw()');
		// 	},
		// 	onAjax: function(action, serialize) {
		// 		console.log('onAjax(action, serialize)');
		// 		console.log(action);
		// 		console.log(serialize);
		// 	}
		// });

		var editor = new $.fn.dataTable.Editor( {			
			table: "#export-table",
			fields: [ {
					label: "User ID:",
					name: "UserID"
				}, {
					label: "Name:",
					name: "Name"
				}, {
					label: "Email:",
					name: "Email"
				}, {
					label: "Office:",
					name: "Phone"
				}, {
					label: "Extension:",
					name: "Validation"
				}, {
					label: "Start date:",
					name: "Facebook",
					// type: "datetime"
				}
			]
		} );

		$('#export-table').on( 'click', 'tbody td:not(:first-child)', function (e) {
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			editor.cell( this ).edit();
			console.log(row.data());
		} );
		
		
		$.fn.dataTable.ext.errMode = 'none';
		trigonic_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					trigonic_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
								trigonic_DB.collection('users').onSnapshot(function(snapshot) {
									userList = [];
									
									snapshot.docChanges().forEach(function(change){
										console.log(change.doc.data());

										var dataObject = {
											UserID: change.doc.id,
											Name: change.doc.data().name,
											Email: change.doc.data().email,
											Phone: change.doc.data().phone,
											Validation: change.doc.data().pending,
											Facebook: change.doc.data().fb

										};
											console.log(dataObject);
										if (table.rows('[id="'+dataObject.UserID+'"]').any())
										{
											table.rows('[id="'+dataObject.UserID+'"]').remove();
											table.row.add(dataObject).draw();	
											console.log("Already exist!");
										}
										else
										{
											table.row.add(dataObject).draw();											
										}
										
									})
								});
							// } else {
								// console.log("not an admin");
							// }
						})
					.catch((error) => {
						console.log(error);
					});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

})(jQuery);