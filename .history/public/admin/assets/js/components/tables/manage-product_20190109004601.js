(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	

	// $(function () {
	// 	$('#sorting-table').DataTable({
	// 		"lengthMenu": [
	// 			[10, 15, 20, -1],
	// 			[10, 15, 20, "All"]
	// 		],
	// 		"order": [
	// 			[3, "desc"]
	// 		]
	// 	});
	// });

	$(function () {	
		var brandList = [];
		trigonic_DB.collection('Brands').onSnapshot(async function(snapshot) {						
			snapshot.docChanges().forEach(function(change){
				console.log(change.doc.data());
				var brandObject = {
					label: change.doc.data().brand_name,
					value: change.doc.data().brand_name,					
				};
				brandList.push(brandObject);
			})
			await console.log(brandList);
		})			

		var table = $('#export-table').DataTable({
			dom: 'Bfrtip',
			rowId: 'ProductID',
			columns : [				
				{ data : 'ProductID' },
				{ data : 'Name' },
				{ data : 'Brand' },
				{ data : 'Condition' },
				{ data : 'Size' },
				{ data : 'Price' }
			],

			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});		

		
		var editor = new $.fn.dataTable.Editor( {			
			table: "#export-table",
			idSrc:  'ProductID',
			fields: [ {
					label: "Product ID:",
					name: "ProductID"
				}, {
					label: "Name:",
					name: "Name"
				}, {
					label: "Brand:",
					name: "Brand",
					type: "select",
					options: brandList
				}, {
					label: "Condition:",
					name: "Condition"
				}, {
					label: "Size:",
					name: "Size"
				}, {
					label: "Price:",
					name: "Price",
				}
			]
		} );
		

		// $('#export-table').on( 'click', 'tbody td:not(:first-child)', function (e) {
		$('#export-table').on( 'click', 'tbody td', function (e) {
			// editor.fields[2].options = brandList;
			var tr = $(this).closest('tr');
			var row = table.row(tr);			
			editor.bubble(this,['Name','Brand','Condition','Size','Price']);
			var updatebutton = document.getElementsByClassName('small button');
			var updateProduct = firebase.functions().httpsCallable('updateProductCallable');	
			
			updatebutton[0].addEventListener('click', function() {
				var newdata = row.data();
				updateProduct({
					uid:newdata.ProductID,
					Name:newdata.Name,
					Brand:newdata.Brand,
					Condition:newdata.Condition,
					Size:newdata.Size,
					Price:newdata.Price
				}).catch(function(error){
					console.log("Updating Product error: ",error);
				});
				console.log("Updating Product");
			});
			
			
			// document.getElementsByClassName('small button').click(function(){
			// 	console.log(row.data().ProductID);
			// })			
			// editor2.Draw(this);
			console.log(row.data());
		} );
		
		
		$.fn.dataTable.ext.errMode = 'none';
		trigonic_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					trigonic_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
						trigonic_DB.collection('Products').onSnapshot(function(snapshot) {							
							
							snapshot.docChanges().forEach(function(change){
								console.log(change.doc.data());

								var dataObject = {
									ProductID: change.doc.id,
									Name: change.doc.data().Name,
									Brand: change.doc.data().Brand,
									Condition: change.doc.data().Condition,
									Size: change.doc.data().Size,
									Price: change.doc.data().Price
								};
									console.log(dataObject);
								if (table.rows('[id="'+dataObject.ProductID+'"]').any())
								{
									table.rows('[id="'+dataObject.ProductID+'"]').remove();
									table.row.add(dataObject).draw();	
									console.log("Already exist!");
								}
								else
								{
									table.row.add(dataObject).draw();											
								}
								
							})
						});
					// } else {
						// console.log("not an admin");
					// }
				})
			.catch((error) => {
				console.log(error);
			});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

})(jQuery);