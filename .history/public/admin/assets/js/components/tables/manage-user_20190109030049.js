(function ($) {

	'use strict';

	// ------------------------------------------------------- //
	// Auto Hide
	// ------------------------------------------------------ //	

	// $(function () {
	// 	$('#sorting-table').DataTable({
	// 		"lengthMenu": [
	// 			[10, 15, 20, -1],
	// 			[10, 15, 20, "All"]
	// 		],
	// 		"order": [
	// 			[3, "desc"]
	// 		]
	// 	});
	// });

	$(function () {	

		var table = $('#export-table').DataTable({
			dom: 'Bfrtip',
			rowId: 'UserID',
			columns : [				
				{ data : 'UserID' },
				{ data : 'Name' },
				{ data : 'Email' },
				{ data : 'Phone' },
				{ data : 'Validation' },
				{ data : 'Facebook' }
			],

			buttons: {
				buttons: [{
					extend: 'copy',
					text: 'Copy',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'excel',
					text: 'Excel',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'csv',
					text: 'Csv',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'pdf',
					text: 'Pdf',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true
				},{
					extend: 'print',
					text: 'Print',
					title: $('h1').text(),
					exportOptions: {
						columns: ':not(.no-print)'
					},
					footer: true,
					autoPrint: true
				}],
				dom: {
					container: {
						className: 'dt-buttons'
					},
					button: {
						className: 'btn btn-primary'
					}
				}
			}
		});
		// $('#export-table').on( 'click', 'tbody td:not(:first-child)', function (e) {
		$('#export-table').on( 'click', 'tbody td', function (e) {
			var tr = $(this).closest('tr');
			var row = table.row( tr );
			var data = row.data();
			$('#input-1').val(data.UserID);
			$('#input-2').val(data.Name);
			$('#input-3').val(data.Email);
			$('#input-4').val(data.Phone);

			var updatebutton = document.getElementById("submit_btn");
			var updateUser = firebase.functions().httpsCallable('updateUserCallable');	
			
			updatebutton.addEventListener('click', function() {
				var newName = document.getElementById("input-2").value;
				var newEmail = document.getElementById("input-3").value;
				var newPhone = document.getElementById("input-4").value;				
				updateUser({
					uid:data.UserID,
					name:newName,
					email:newEmail,
					phone:newPhone
				}).catch(function(error){
					console.log("Updating user error: ",error);
				});
				console.log("Updating User");
			});
			
			
			// document.getElementsByClassName('small button').click(function(){
			// 	console.log(row.data().UserID);
			// })			
			// editor2.Draw(this);
			console.log(row.data());
		} );
		
		
		$.fn.dataTable.ext.errMode = 'none';
		trigonic_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					trigonic_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
								trigonic_DB.collection('users').onSnapshot(function(snapshot) {									
									
									snapshot.docChanges().forEach(function(change){
										console.log(change.doc.data());

										var dataObject = {
											UserID: change.doc.id,
											Name: change.doc.data().name,
											Email: change.doc.data().email,
											Phone: change.doc.data().phone,
											Validation: change.doc.data().pending,
											Facebook: change.doc.data().fb

										};
											console.log(dataObject);
										if (table.rows('[id="'+dataObject.UserID+'"]').any())
										{
											table.rows('[id="'+dataObject.UserID+'"]').remove();
											table.row.add(dataObject).draw();	
											console.log("Already exist!");
										}
										else
										{
											table.row.add(dataObject).draw();											
										}
										
									})
								});
							// } else {
								// console.log("not an admin");
							// }
						})
					.catch((error) => {
						console.log(error);
					});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

})(jQuery);