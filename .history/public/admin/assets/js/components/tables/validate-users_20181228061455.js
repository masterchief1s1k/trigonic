(function ($) {

	'use strict';

	
$("#confirm-btn").click(function(){

    var current_user_id = $(this).attr("user-id");
    // call function to validate (current_user_id);
})

function format ( d ,url) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Full name:</td>'+
            '<td>'+d.name+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Extension number:</td>'+
            '<td>'+d.extn+'</td>'+
        '</tr>'+
        // '<tr>'+
        //     '<td>Extra info:</td>'+
        //     '<td>And any further details here (images etc)... <img src="' +url + '"/> </td>'+
        // '</tr>'+
        '<tr>'+
            '<td>Validate this account:</td>'+
            '<td> <button user-id="'+ d.id +'" id="confirm-btn"> Confirm </button> </td>'+
        '</tr>'+
        
    '</table>';
}

	$(function () {

		
		
		var userList = [];

		var table = $('#validate-table').DataTable({			
			rowId: 'UserID',
			columns : [
				{ data : 'UserID' },
				{ data : 'Name' },
				{ data : 'Email' },
				{ data : 'Phone' },
				{ data : 'Validation' },
				{ data : 'Facebook' }
			],
		});

		$.fn.dataTable.ext.errMode = 'none';
		trigonic_auth.onAuthStateChanged(function(user) {
			if (user) {
				console.log(user);
					trigonic_auth.currentUser.getIdTokenResult()
					.then((idTokenResult) => {
							// Confirm the user is an Admin.
							// if (!!idTokenResult.claims.admin) {
								trigonic_DB.collection('users').onSnapshot(function(snapshot) {
									userList = [];
									
									snapshot.docChanges().forEach(function(change){
										console.log(change.doc.data());

										var dataObject = {
											UserID: change.doc.id,
											Name: change.doc.data().name,
											Email: change.doc.data().email,
											Phone: change.doc.data().phone,
											Validation: change.doc.data().pending,
											Facebook: change.doc.data().fb

										};
											console.log(dataObject);
										if (table.rows('[id="'+dataObject.UserID+'"]').any())
										{
											table.rows('[id="'+dataObject.UserID+'"]').remove();
											table.row.add(dataObject).draw();	
											console.log("Already exist!");
										}
										else
										{
											table.row.add(dataObject).draw();
											
										}
										
									})
								});
							// } else {
								// console.log("not an admin");
							// }
						})
					.catch((error) => {
						console.log(error);
					});

			} else {
				console.log("not a user");
				
				// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
			}
		});



	});

})(jQuery);