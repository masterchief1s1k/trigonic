(function ($) {

	'use strict';

$(function () {
	function format ( d ) {
		// `d` is the original data object for the row
		return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
			'<tr>'+
				'<td>Full name:</td>'+
				'<td>'+d.Name+'</td>'+
			'</tr>'+
			'<tr>'+
				'<td>Extension number:</td>'+
				'<td>'+d.phone+'</td>'+
			'</tr>'+
			// '<tr>'+
			//     '<td>Extra info:</td>'+
			//     '<td>And any further details here (images etc)... <img src="' +url + '"/> </td>'+
			// '</tr>'+
			'<tr>'+
				'<td>Validate this account:</td>'+
				'<td> <button user-id="'+ d.id +'" id="confirm-btn"> Confirm </button> </td>'+
			'</tr>'+
			
		'</table>';
	}

	$("#confirm-btn").click(function(){

		var current_user_id = $(this).attr("user-id");
		validateUser({user-id}).catch(function(error){
			console.log("Validating click error: ",error);
		});;
		// call function to validate (current_user_id);
	})
	
	var userList = [];

	var table = $('#validate-table').DataTable({			
		rowId: 'UserID',
		columns : [
			{
                "className":      'details-control',
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
			{ data : 'UserID' },
			{ data : 'Name' },
			{ data : 'Email' },
			{ data : 'Phone' },
			{ data : 'Validation' },			
		],
	});

	$.fn.dataTable.ext.errMode = 'none';
	trigonic_auth.onAuthStateChanged(function(user) {
		if (user) {
			console.log(user);
				trigonic_auth.currentUser.getIdTokenResult()
				.then((idTokenResult) => {
						// Confirm the user is an Admin.
						// if (!!idTokenResult.claims.admin) {
							trigonic_DB.collection('users').onSnapshot(function(snapshot) {
								userList = [];
								
								snapshot.docChanges().forEach(function(change){
									console.log(change.doc.data());

									var dataObject = {
										UserID: change.doc.id,
										Name: change.doc.data().name,
										Email: change.doc.data().email,
										Phone: change.doc.data().phone,
										Validation: change.doc.data().pending,										

									};
										console.log(dataObject);
									if (table.rows('[id="'+dataObject.UserID+'"]').any())
									{
										table.rows('[id="'+dataObject.UserID+'"]').remove();
										table.row.add(dataObject).draw();	
										console.log("Already exist!");
									}
									else
									{
										table.row.add(dataObject).draw();
										
									}
									
								})
							});
						
					})
				.catch((error) => {
					console.log(error);
				});

		} else {
			console.log("not a user");
			
			// $("#user-nav").append("<a href=\"login/index.html\"\>Login/Register</a> ");
		}
	});
	$('#validate-table tbody').on('click', 'td.details-control', function () {
		var tr = $(this).closest('tr');
		var row = table.row( tr );
	
		if ( row.child.isShown() ) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			// Open this row
			row.child( format(row.data()) ).show();
			tr.addClass('shown');
			console.log(row.data());
		}
	} );
	
});



})(jQuery);