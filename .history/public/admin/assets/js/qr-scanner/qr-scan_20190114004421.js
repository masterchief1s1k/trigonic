(function ($) {	
    import QrScanner from "qr-scanner.min.js";
    
    const debugCheckbox = document.getElementById('debug-checkbox');
    const debugCanvas = document.getElementById('debug-canvas');
    const debugCanvasContext = debugCanvas.getContext('2d');    
    const fileSelector = document.getElementById('file-selector');
    const fileQrResult = document.getElementById('file-qr-result');

    function setResult(label, result) {
        label.textContent = result;
        label.style.color = 'teal';
        clearTimeout(label.highlightTimeout);
        label.highlightTimeout = setTimeout(() => label.style.color = 'inherit', 100);
    }    

    // ####### File Scanning #######

    fileSelector.addEventListener('change', event => {
        const file = fileSelector.files[0];
        if (!file) {
            return;
        }
        QrScanner.scanImage(file)
            .then(result => setResult(fileQrResult, result))
            .catch(e => setResult(fileQrResult, e || 'No QR code found.'));
    });


    // ####### debug mode related code #######

    debugCheckbox.addEventListener('change', () => setDebugMode(debugCheckbox.checked));

    function setDebugMode(isDebug) {
        const worker = scanner._qrWorker;
        worker.postMessage({
            type: 'setDebug',
            data: isDebug
        });
        if (isDebug) {
            debugCanvas.style.display = 'block';
            worker.addEventListener('message', handleDebugImage);
        } else {
            debugCanvas.style.display = 'none';
            worker.removeEventListener('message', handleDebugImage);
        }
    }

    function handleDebugImage(event) {
        const type = event.data.type;
        if (type === 'debugImage') {
            const imageData = event.data.data;
            if (debugCanvas.width !== imageData.width || debugCanvas.height !== imageData.height) {
                debugCanvas.width = imageData.width;
                debugCanvas.height = imageData.height;
            }
            debugCanvasContext.putImageData(imageData, 0, 0);
        }
    }
    

})(jQuery);