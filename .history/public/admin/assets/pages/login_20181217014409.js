window.onload = function () {    

    var btnSignIn = document.getElementById("signin-btn");
    btnSignIn.onclick = signInWithUsername;

    firebase.auth().onAuthStateChanged(function (firebaseUser) {
    if (firebaseUser) {
        console.log(`logged in as ${firebaseUser.email}`,firebaseUser);                
    } else {
        console.log('not logged in');                
        } 
    });  

}

function signInWithUsername(){
    var username = document.getElementById('username').value;
    var password = document.getElementById('password').value;
    // alert(password);
    firebase.auth().signInWithEmailAndPassword(username,password).catch(function(error){
        var errorCode = error.code;
        var errorMessage = error.message;
        window.alert("Error: "+errorMessage);
    });
}
