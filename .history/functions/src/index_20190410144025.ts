import * as firebase from 'firebase';
import { write } from 'fs';
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const serviceAccount = require('../bluesichtshoecare-firebase-adminsdk-3rhhy-889164ca0e.json');

const config = {
  credential: admin.credential.cert(serviceAccount),
  apiKey: "AIzaSyD7sWq9OyLlgbf-REijWZUwDt2q2pD-rM0",
  authDomain: "bluesichtshoecare.firebaseapp.com",
  databaseURL: "https://bluesichtshoecare.firebaseio.com",
  projectId: "bluesichtshoecare",
  storageBucket: "bluesichtshoecare.appspot.com",
  messagingSenderId: "497538613208",
  keyFilename: ('../bluesichtshoecare-firebase-adminsdk-3rhhy-889164ca0e.json')
};

admin.initializeApp(config);
firebase.initializeApp(config);
const db = admin.firestore();
const settings = { timestampsInSnapshots: true};
db.settings(settings);


async function writeUser(user){
  var id;
  await db.collection('users').doc('statistics').get().then(async function (doc) {
    if (doc.exists) {          
      id = doc.data().userCountID;
    }
  });

  const userCountRef = db.collection('users').doc('statistics');
  db.runTransaction(async t => {
    return await t.get(userCountRef)
    .then(doc => {
      var newUserCountID = doc.data().userCountID + 1;
      t.update(userCountRef, {userCountID: newUserCountID});
    }).then(result => {
      console.log('Transaction success: ',result);
    }).catch (e => {
      console.log('Transaction error: ',e);
    })
  })

  await db.collection('users').add({
    usertype: 'none',
    uid: user.uid || 'none',
    customid: id,
    name: user.displayName || 'none' || user.name,
    dob: user.dob || 'none',
    email: user.email || 'none',
    // photourl: user.photoURL + '?type=large' || 'https://ui-avatars.com/api/?name=' + name ,
    // gender : user.gender || 'none',
    // fb: user.fbLink || 'none',
    phone: user.phone || 'none',
    personalid: 'none'
  }).catch(function(error){
    console.error("Error adding info: ", error);
    })
  .then (async () => {
    console.log("User created: " + JSON.stringify(user));    
    await db.collection('users').doc(user.uid).update({uid:user.uid})
    })
    return user;
}

exports.writeUserCallable = functions.https.onCall(async user => {
  console.log(`Writing ${user.name}`);
  return writeUser(user).then(()=>{
    console.log(`Successfully added ${user.name}`);
  });
});

exports.deleteUserData = functions.auth.user().onDelete(async user => {   
 
  db.collection('users').doc(user.uid).delete().catch(function(error){
    console.error("Error delete user: ", error);
    })  
});
// request.auth.uid == resource.data.uid
async function grantAdmin(email) {
  const user = await admin.auth().getUserByEmail(email); // 1
  if (user.customClaims && user.customClaims.admin === true) {
      console.log("Already an admin.")
      return;
  } // 2
  return admin.auth().setCustomUserClaims(user.uid, {
      admin: true
  }); // 3
}

async function writeService(order){
  await db.collection('services').add({
    uid: order.uid || 'none',
    owner: order.owner || 'none',
    type: order.type,
    clean: order.service,
  }).catch(function(error){
    console.error("Error adding info: ", error);
    })
  .then (async () => {
    console.log("User created: " + JSON.stringify(order));    
    await db.collection('users').doc(order.uid).update({uid:order.uid})
    })
}

exports.writeServiceCallable = functions.https.onCall(async order => {
  console.log(`Adding ${order.name}`);
  return writeService(order).then(()=>{
    console.log(`${order.name} validated.`);
  })
  
});

exports.writeAdmin = functions.https.onCall((data, context) => {   
  if (context.auth.token.admin !== true) {   
    console.log("Request not authorized. User must be a moderator.");
  };
  const email = data.email; 
  return grantAdmin(email).then(() => {
      console.log(`${email} is now an admin.`);
  }); 
});

async function validateUser(uid){
  await db.collection('users').doc(uid).update({
    pending: false
  }).catch(function(error) {
    console.log("Error validating: ", error);
})};

exports.validateUserCallable = functions.https.onCall(data => { 
  console.log(`Validating user ${data.uid}`);

  return validateUser(data.uid).then(() =>{
    console.log(`${data.uid} validated.`);
  })
});

async function updateUser(data){
  await db.collection('users').doc(data.uid).update({
    name:data.name,
		email:data.email,
		phone:data.phone
  }).catch(function(error) {
    console.log("Error updating user: ", error);
})};

exports.updateUserCallable = functions.https.onCall(data => { 
  console.log(`Updating user ${data.name}`);
  return updateUser(data).then(() =>{
    console.log(`User ${data.uid} - ${data.name} updated.`);
  })
});

async function validateProduct(uid){
  await db.collection('Products').doc(uid).update({
    Pending: false
  }).catch(function(error) {
    console.log("Error validating: ", error);
})};

exports.validateProductCallable = functions.https.onCall(data => { 
  console.log(`Validating product ${data.Name}`);

  return validateProduct(data.uid).then(() =>{
    console.log(`${data.uid} - ${data.Name} validated.`);
  })
});

async function updateProduct(data){
  await db.collection('Products').doc(data.uid).update({    
    Name:data.Name,
    Brand:data.Brand,
    Condition:data.Condition,
    Size:data.Size,
    Price:data.Price
  }).catch(function(error) {
    console.log("Error updating product: ", error);
})};

exports.updateProductCallable = functions.https.onCall(data => { 
  console.log(`Updating product ${data.uid} - ${data.Name}`);
  return updateProduct(data).then(() =>{
    console.log(`Product ${data.uid} - ${data.Name} updated.`);
  })
});

async function updateService(data){
  await db.collection('Services').doc(data.uid).update({    
    Name:data.Name,
    Status:data.Status,
    // Notes:data.Notes,    
  }).catch(function(error) {
    console.log("Error updating product: ", error);
})};

exports.updateServiceCallable = functions.https.onCall(data => { 
  console.log(`Updating service ${data.uid} - ${data.Name}`);
  return updateService(data).then(() =>{
    console.log(`Service ${data.uid} - ${data.Name} updated.`);
  })
});



async function getID(uid){
  let query;  
  // const user = await admin.auth().getUser(uid); //get using uid
  const userRef = await db.collection("users").doc(uid);
  await userRef.get().then(doc => {
    if (!doc.exists) {
      console.log('No such document!');
    } else {
      console.log('Document data:', doc.data());      
      query = doc.data();
    }
  })
  .catch(err => {
    console.log('Error getting document', err);
  });;
  return query;
}

exports.userProfile = functions.https.onRequest(async (req, res) => {  
  const data = await getID(req.query.uid);
  console.log(data.name);
  res.status(200).send(`<!doctype html>
    <head>
      <title>FBID</title>      
    </head>
    <body>      
      Name: <input type="text" id="name" placeholder="${data.name}"><BR>
      UID is ${req.query.uid}
    </body>
  </html>`);
});