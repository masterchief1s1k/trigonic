import * as firebase from 'firebase';
const admin = require('firebase-admin');
const functions = require('firebase-functions');

const serviceAccount = require('../trigonic-a4354-firebase-adminsdk-fiaob-e3805bc5af.json');

const config = {
    credential: admin.credential.cert(serviceAccount),
    apiKey: "AIzaSyA3uKbxPyeGSxpDOUH6bkS_L8wFgdEqrLw",
    authDomain: "trigonic-a4354.firebaseapp.com",
    databaseURL: "https://trigonic-a4354.firebaseio.com",
    projectId: "trigonic-a4354",
    storageBucket: "trigonic-a4354.appspot.com",
    messagingSenderId: "403029019752",
    keyFilename: ('../trigonic-a4354-firebase-adminsdk-fiaob-e3805bc5af.json')
  };

admin.initializeApp(config);
firebase.initializeApp(config);
const db = admin.firestore();


// exports.writeUserData = functions.auth.user().onCall(async user => {   
 
//   await db.collection('users').doc(user.uid).set({
//     userType: 'none',
//     userId: user.uid,
//     name: user.displayName || 'none',
//     DoB: user.DoB || 'none',
//     email: user.email || 'none',
//     photoURL: user.photoURL + '?type=large' || 'https://ui-avatars.com/api/?name=' + name ,
//     gender : user.gender || 'none',
//     fb: user.fbLink || 'none',
//     phone: user.phone || 'none',
//     personalID: 'none'
//   }).catch(function(error){
//     console.error("Error adding info: ", error);
//     })
//   .then (async () => {
//     console.log("User created: " + JSON.stringify(user));})
// });

exports.deleteUserData = functions.auth.user().onDelete(async user => {   
 
  db.collection('users').doc(user.uid).delete().catch(function(error){
    console.error("Error delete user: ", error);
    })  
});
// request.auth.uid == resource.data.uid
async function grantAdmin(email) {
  const user = await admin.auth().getUserByEmail(email); // 1
  if (user.customClaims && user.customClaims.admin === true) {
      console.log("Already an admin.")
      return;
  } // 2
  return admin.auth().setCustomUserClaims(user.uid, {
      admin: true
  }); // 3
}

exports.writeAdmin = functions.https.onCall((data, context) => {   
  if (context.auth.token.admin !== true) {   
    console.log("Request not authorized. User must be a moderator.");
  };
  const email = data.email; 
  return grantAdmin(email).then(() => {
      console.log(`${email} is now an admin.`);
  }); 
});

async function validateUser(uid){
  await db.collection('users').doc(uid).update({
    pending: false
  }).catch(function(error) {
    console.log("Error validating: ", error);
})};

exports.validateUserCallable = functions.https.onCall(data => { 
  console.log(`Validating user ${data.uid}`);

  return validateUser(data.uid).then(() =>{
    console.log(`${data.uid} validated.`);
  })
});

async function updateUser(data){
  await db.collection('users').doc(data.uid).update({
    name:data.name,
		email:data.email,
		phone:data.phone
  }).catch(function(error) {
    console.log("Error updating user: ", error);
})};

exports.updateUserCallable = functions.https.onCall(data => { 
  console.log(`Updating user ${data.name}`);
  return updateUser(data).then(() =>{
    console.log(`User ${data.uid} - ${data.name} updated.`);
  })
});

async function validateProduct(uid){
  await db.collection('Products').doc(uid).update({
    Pending: false
  }).catch(function(error) {
    console.log("Error validating: ", error);
})};

exports.validateProductCallable = functions.https.onCall(data => { 
  console.log(`Validating product ${data.Name}`);

  return validateProduct(data.uid).then(() =>{
    console.log(`${data.uid} - ${data.Name} validated.`);
  })
});

async function updateProduct(data){
  await db.collection('Products').doc(data.uid).update({    
    Name:data.Name,
    Brand:data.Brand,
    Condition:data.Condition,
    Size:data.Size,
    Price:data.Price
  }).catch(function(error) {
    console.log("Error updating product: ", error);
})};

exports.updateProductCallable = functions.https.onCall(data => { 
  console.log(`Updating product ${data.uid} - ${data.Name}`);
  return updateProduct(data).then(() =>{
    console.log(`Product ${data.uid} - ${data.Name} updated.`);
  })
});

async function updateService(data){
  await db.collection('Services').doc(data.uid).update({    
    Name:data.Name,
    Status:data.Status,
    // Notes:data.Notes,    
  }).catch(function(error) {
    console.log("Error updating product: ", error);
})};

exports.updateServiceCallable = functions.https.onCall(data => { 
  console.log(`Updating service ${data.uid} - ${data.Name}`);
  return updateService(data).then(() =>{
    console.log(`Service ${data.uid} - ${data.Name} updated.`);
  })
});



async function getID(QRCode){
  let user = await admin.auth().getUser(QRCode); //get using uid
  return user.facebookID;
}

// exports.getFBIDCallable = functions.https.onCall(data  => {
//   var x = getID(data);
//   //trả x về javascript xử lý bật trang user fb của người dùng?
// })

exports.generateDynamicCallable = functions.https.onCall(data  => {
  const info = getID(data);  
  data.status(200).send(`<!doctype html>
    <head>
      <title>FBID</title>
    </head>
    <body>
      ${info}
    </body>
  </html>`);
});