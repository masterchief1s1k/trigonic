// var path = require('path');
// var os = require('os');
// var fs = require('fs');
// var sharp = require('sharp');
// var express = require('express');
// var bucket = admin.storage().bucket('trigonic-a4354.appspot.com');

// exports.generateThumbnail = functions.region('asia-northeast1').storage.object().onFinalize(async object => {
//   var fileBucket = object.bucket;
//   var filePath = object.name;
//   var contentType = object.contentType;  
//   var fileName = path.basename(filePath);
//   var tempPath = path.join(os.tmpdir(), fileName);
//   var thumbName = `thumb_${fileName}`;
//   var thumbPath = path.join(os.tmpdir(), thumbName);

//   // Exit if this is triggered on a file that is not an image.
//   if (!contentType.startsWith('image/')) {
//     console.log('This is not an image.');
//     return null;
//   }  
//   // Exit if the image is already a thumbnail.
//   if (fileName.startsWith('thumb_')) {
//     console.log('Already a Thumbnail.');
//     return null;
//   }

//   //Download image
//   await bucket.file(filePath).download( {
//     destination: tempPath,
//   })
//   .then(() => {
//     console.log(`${filePath} downloaded to ${tempPath}`);
//   })
//   //Resize image to a thumbnail
//   .then(async () =>{    
//     await sharp(tempPath)
//     .resize(200)
//     .toBuffer()
//     .then(async data => {      
//       await fs.writeFileSync(thumbPath,data);      
//     })
//     .catch( err => {
//       console.log('Sharp ERROR: ',err);
//     })
//   })  
//   .then(async () => {
//     console.log('Thumbnail created at', thumbPath);        
//     //Upload the thumbnail
//     // var fileName = filePath.split('/').pop();
//     // var thumbName = `thumb_${fileName}`;
//     await bucket.upload(thumbPath, {
//       destination: thumbName,      
//   })
//   })
//   .then(async() => {
//     console.log(`${thumbName} uploaded to ${bucket.name}.`);
//     await fs.unlinkSync(thumbPath);
//   })  
// });