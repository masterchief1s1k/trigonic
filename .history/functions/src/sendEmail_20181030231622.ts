import * as firebase from 'firebase';
var admin = require('firebase-admin');
var functions = require('firebase-functions');

var serviceAccount = require('./trigonic-a4354-firebase-adminsdk-fiaob-0a7be100d0.json');

var config = {
    credential: admin.credential.cert(serviceAccount),
    apiKey: "AIzaSyA3uKbxPyeGSxpDOUH6bkS_L8wFgdEqrLw",
    authDomain: "trigonic-a4354.firebaseapp.com",
    databaseURL: "https://trigonic-a4354.firebaseio.com",
    projectId: "trigonic-a4354",
    storageBucket: "trigonic-a4354.appspot.com",
    messagingSenderId: "403029019752",
    keyFilename: ('./trigonic-a4354-firebase-adminsdk-fiaob-0a7be100d0.json')
  };

admin.initializeApp(config);
firebase.initializeApp(config);

var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');

var authMailgun = {
  auth: {
    api_key: 'a28bd21bc4de76e97556679d57314caf-4836d8f5-efde817c',
    //Domain riêng
    domain: 'sandboxb24f25a418424145ba198892358d42fe.mailgun.org'
  }  
}

var user = firebase.auth().currentUser;
var email;
var uid = 'lFH0JmeNGBYt0s8DjlyfoUAojwy2';

var userRef, emailRef;

admin.auth().getUser(uid)
  .then(function(userRecord) {
    emailRef = userRecord.email;
    // See the UserRecord reference doc for the contents of userRecord.
    console.log("Successfully fetched user data:", userRecord.email);
  })
  .catch(function(error) {
    console.log("Error fetching user data:", error);
  });

 
var nodemailerMailgun = nodemailer.createTransport(mg(auth));

exports.sendEmail = functions.https.onRequest(async (req, res) => 
{  
  await nodemailerMailgun.sendMail({
    from: 'no-reply@trigonic.com',
    to: email,     
    subject: 'Testing request!',
    'h:Reply-To': 'no-reply@sandboxc77c3a9be90a494081dad1628d554337.mailgun.org',
    //You can use "html:" to send HTML email content.
    html: '<b>Its working!</b>',
    //"text:" to send plain-text content.!
    text: 'Mailgun rocks, pow pow!'
  }, function (err, info) {
    if (err) {
      console.log('Error: ' + err);
    }
    else {
      console.log('Response: ' + info);
    }
  });
})